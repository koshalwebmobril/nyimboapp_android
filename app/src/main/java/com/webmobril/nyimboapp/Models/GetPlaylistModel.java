package com.webmobril.nyimboapp.Models;

import com.google.gson.annotations.SerializedName;

public class GetPlaylistModel {

	@SerializedName("updated_at")
	private String updatedAt;

	@SerializedName("user_id")
	private int userId;

	@SerializedName("name")
	private String name;

	@SerializedName("created_at")
	private String createdAt;

	@SerializedName("id")
	private int id;

	@SerializedName("song_playlist_count")
	private int songPlaylistCount;

	@SerializedName("status")
	private int status;

	public String getUpdatedAt(){
		return updatedAt;
	}

	public int getUserId(){
		return userId;
	}

	public String getName(){
		return name;
	}

	public String getCreatedAt(){
		return createdAt;
	}

	public int getId(){
		return id;
	}

	public int getSongPlaylistCount(){
		return songPlaylistCount;
	}

	public int getStatus(){
		return status;
	}
}