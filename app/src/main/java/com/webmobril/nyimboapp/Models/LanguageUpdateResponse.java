package com.webmobril.nyimboapp.Models;

import com.google.gson.annotations.SerializedName;

public class LanguageUpdateResponse {

	@SerializedName("result")
	private LanguageUpdateModel languageUpdateModel;

	@SerializedName("status")
	private String status;

	public LanguageUpdateModel getLanguageUpdateModel(){
		return languageUpdateModel;
	}

	public String getStatus(){
		return status;
	}
}