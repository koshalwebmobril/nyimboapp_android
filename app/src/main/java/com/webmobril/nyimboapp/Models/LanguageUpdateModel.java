package com.webmobril.nyimboapp.Models;

import com.google.gson.annotations.SerializedName;

public class LanguageUpdateModel {

	@SerializedName("langauge_id")
	private String langaugeId;

	@SerializedName("updated_at")
	private String updatedAt;

	@SerializedName("user_id")
	private String userId;

	@SerializedName("created_at")
	private String createdAt;

	@SerializedName("id")
	private int id;

	@SerializedName("status")
	private int status;

	public String getLangaugeId(){
		return langaugeId;
	}

	public String getUpdatedAt(){
		return updatedAt;
	}

	public String getUserId(){
		return userId;
	}

	public String getCreatedAt(){
		return createdAt;
	}

	public int getId(){
		return id;
	}

	public int getStatus(){
		return status;
	}
}