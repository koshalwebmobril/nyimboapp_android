package com.webmobril.nyimboapp.Models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class TrandingSongsModel implements Parcelable {

	@SerializedName("image")
	private String image;

	@SerializedName("paid_status")
	private int paidStatus;

	@SerializedName("year")
	private Object year;

	@SerializedName("name")
	private String name;

	@SerializedName("description")
	private String description;

	@SerializedName("id")
	private int id;


	@SerializedName("upload_song")
	private String  upload_song;


	protected TrandingSongsModel(Parcel in) {
		image = in.readString();
		paidStatus = in.readInt();
		name = in.readString();
		description = in.readString();
		id = in.readInt();
		upload_song = in.readString();
	}

	public static final Creator<TrandingSongsModel> CREATOR = new Creator<TrandingSongsModel>() {
		@Override
		public TrandingSongsModel createFromParcel(Parcel in) {
			return new TrandingSongsModel(in);
		}

		@Override
		public TrandingSongsModel[] newArray(int size) {
			return new TrandingSongsModel[size];
		}
	};

	public String getImage(){
		return image;
	}

	public int getPaidStatus(){
		return paidStatus;
	}

	public Object getYear(){
		return year;
	}

	public String getName(){
		return name;
	}

	public String getDescription(){
		return description;
	}

	public int getId(){
		return id;
	}


	public String getUploadSong(){
		return upload_song;
	}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeString(image);
		dest.writeInt(paidStatus);
		dest.writeString(name);
		dest.writeString(description);
		dest.writeInt(id);
		dest.writeString(upload_song);
	}
}