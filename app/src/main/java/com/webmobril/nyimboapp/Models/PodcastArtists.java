package com.webmobril.nyimboapp.Models;

import com.google.gson.annotations.SerializedName;

public class PodcastArtists{

	@SerializedName("name")
	private String name;

	@SerializedName("id")
	private int id;

	public String getName(){
		return name;
	}

	public int getId(){
		return id;
	}
}