package com.webmobril.nyimboapp.Models;

import com.google.gson.annotations.SerializedName;

public class GenreListModel1 {

	@SerializedName("image")
	private String image;

	@SerializedName("name")
	private String name;

	@SerializedName("id")
	private int id;

	public String getImage(){
		return image;
	}

	public String getName(){
		return name;
	}

	public int getId(){
		return id;
	}
}