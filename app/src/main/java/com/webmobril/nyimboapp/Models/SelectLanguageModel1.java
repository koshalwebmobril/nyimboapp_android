package com.webmobril.nyimboapp.Models;

import com.google.gson.annotations.SerializedName;

public class SelectLanguageModel1 {

	@SerializedName("updated_at")
	private String updatedAt;

	@SerializedName("created_at")
	private String createdAt;

	@SerializedName("language")
	private String language;

	@SerializedName("color")
	private String color;

	@SerializedName("id")
	private int id;

	@SerializedName("status")
	private int status;

	private boolean isSelected = false;

	public boolean isSelected() {
		return isSelected;
	}

	public void setSelected(boolean selected) {
		isSelected = selected;
	}

	public String getUpdatedAt(){
		return updatedAt;
	}

	public String getCreatedAt(){
		return createdAt;
	}

	public String getLanguage(){
		return language;
	}

	public String getColor(){
		return color;
	}

	public int getId(){
		return id;
	}

	public int getStatus(){
		return status;
	}
}