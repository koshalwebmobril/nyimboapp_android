package com.webmobril.nyimboapp.Models;

import com.google.gson.annotations.SerializedName;

public class GetProfileModel {

	@SerializedName("confirmation_code")
	private Object confirmationCode;

	@SerializedName("gender")
	private Object gender;

	@SerializedName("login_type")
	private int loginType;

	@SerializedName("created_at")
	private String createdAt;

	@SerializedName("otp_verify_status")
	private int otpVerifyStatus;

	@SerializedName("device_type")
	private int deviceType;

	@SerializedName("confirmed")
	private int confirmed;

	@SerializedName("password")
	private Object password;

	@SerializedName("profile_image")
	private Object profileImage;

	@SerializedName("social_type")
	private int socialType;

	@SerializedName("updated_at")
	private String updatedAt;

	@SerializedName("id")
	private int id;

	@SerializedName("remember_token")
	private Object rememberToken;

	@SerializedName("first_name")
	private String firstName;

	@SerializedName("email")
	private String email;

	@SerializedName("lat")
	private Object lat;

	@SerializedName("pincode")
	private Object pincode;

	@SerializedName("image")
	private Object image;

	@SerializedName("otp_time")
	private String otpTime;

	@SerializedName("lng")
	private Object lng;

	@SerializedName("approval")
	private int approval;

	@SerializedName("mobile")
	private Object mobile;

	@SerializedName("last_name")
	private Object lastName;

	@SerializedName("otp")
	private Object otp;

	@SerializedName("apple_id")
	private String appleId;

	@SerializedName("is_term_accept")
	private int isTermAccept;

	@SerializedName("created_by")
	private Object createdBy;

	@SerializedName("deleted_at")
	private Object deletedAt;

	@SerializedName("fb_id")
	private Object fbId;

	@SerializedName("dob")
	private Object dob;

	@SerializedName("device_token")
	private String deviceToken;

	@SerializedName("updated_by")
	private Object updatedBy;

	@SerializedName("username")
	private String username;

	@SerializedName("status")
	private int status;

	public Object getConfirmationCode(){
		return confirmationCode;
	}

	public Object getGender(){
		return gender;
	}

	public int getLoginType(){
		return loginType;
	}

	public String getCreatedAt(){
		return createdAt;
	}

	public int getOtpVerifyStatus(){
		return otpVerifyStatus;
	}

	public int getDeviceType(){
		return deviceType;
	}

	public int getConfirmed(){
		return confirmed;
	}

	public Object getPassword(){
		return password;
	}

	public Object getProfileImage(){
		return profileImage;
	}

	public int getSocialType(){
		return socialType;
	}

	public String getUpdatedAt(){
		return updatedAt;
	}

	public int getId(){
		return id;
	}

	public Object getRememberToken(){
		return rememberToken;
	}

	public String getFirstName(){
		return firstName;
	}

	public String getEmail(){
		return email;
	}

	public Object getLat(){
		return lat;
	}

	public Object getPincode(){
		return pincode;
	}

	public Object getImage(){
		return image;
	}

	public String getOtpTime(){
		return otpTime;
	}

	public Object getLng(){
		return lng;
	}

	public int getApproval(){
		return approval;
	}

	public Object getMobile(){
		return mobile;
	}

	public Object getLastName(){
		return lastName;
	}

	public Object getOtp(){
		return otp;
	}

	public String getAppleId(){
		return appleId;
	}

	public int getIsTermAccept(){
		return isTermAccept;
	}

	public Object getCreatedBy(){
		return createdBy;
	}

	public Object getDeletedAt(){
		return deletedAt;
	}

	public Object getFbId(){
		return fbId;
	}

	public Object getDob(){
		return dob;
	}

	public String getDeviceToken(){
		return deviceToken;
	}

	public Object getUpdatedBy(){
		return updatedBy;
	}

	public String getUsername(){
		return username;
	}

	public int getStatus(){
		return status;
	}
}