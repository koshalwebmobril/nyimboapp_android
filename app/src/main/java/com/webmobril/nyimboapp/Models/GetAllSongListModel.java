package com.webmobril.nyimboapp.Models;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;
import com.google.gson.annotations.SerializedName;
import com.webmobril.nyimboapp.Models.ArtistSongswithnameModel;

public class GetAllSongListModel implements Parcelable {

	@SerializedName("image")
	private String image;

	@SerializedName("paid_status")
	private int paidStatus;

	@SerializedName("year")
	private Object year;

	@SerializedName("description")
	private String description;

	@SerializedName("upload_song")
	private String uploadSong;

	@SerializedName("created_at")
	private String createdAt;

	@SerializedName("language_id")
	private int languageId;

	@SerializedName("type")
	private String type;

	@SerializedName("artist_id")
	private Object artistId;

	@SerializedName("duration")
	private String duration;

	@SerializedName("like_dislike_songs_count")
	private int likeDislikeSongsCount;

	@SerializedName("category_id")
	private int categoryId;

	@SerializedName("updated_at")
	private String updatedAt;

	@SerializedName("name")
	private String name;

	@SerializedName("artist_songs")
	private List<ArtistSongswithnameModel> artistSongs;

	@SerializedName("album_id")
	private int albumId;

	@SerializedName("id")
	private int id;

	@SerializedName("keyword")
	private String keyword;

	@SerializedName("plan_id")
	private int planId;

	@SerializedName("status")
	private int status;

	protected GetAllSongListModel(Parcel in) {
		image = in.readString();
		paidStatus = in.readInt();
		description = in.readString();
		uploadSong = in.readString();
		createdAt = in.readString();
		languageId = in.readInt();
		type = in.readString();
		duration = in.readString();
		likeDislikeSongsCount = in.readInt();
		categoryId = in.readInt();
		updatedAt = in.readString();
		name = in.readString();
		albumId = in.readInt();
		id = in.readInt();
		keyword = in.readString();
		planId = in.readInt();
		status = in.readInt();
	}

	public static final Creator<GetAllSongListModel> CREATOR = new Creator<GetAllSongListModel>() {
		@Override
		public GetAllSongListModel createFromParcel(Parcel in) {
			return new GetAllSongListModel(in);
		}

		@Override
		public GetAllSongListModel[] newArray(int size) {
			return new GetAllSongListModel[size];
		}
	};

	public String getImage(){
		return image;
	}

	public int getPaidStatus(){
		return paidStatus;
	}

	public Object getYear(){
		return year;
	}

	public String getDescription(){
		return description;
	}

	public String getUploadSong(){
		return uploadSong;
	}

	public String getCreatedAt(){
		return createdAt;
	}

	public int getLanguageId(){
		return languageId;
	}

	public String getType(){
		return type;
	}

	public Object getArtistId(){
		return artistId;
	}

	public String getDuration(){
		return duration;
	}

	public int getLikeDislikeSongsCount(){
		return likeDislikeSongsCount;
	}

	public int getCategoryId(){
		return categoryId;
	}

	public String getUpdatedAt(){
		return updatedAt;
	}

	public String getName(){
		return name;
	}

	public List<ArtistSongswithnameModel> getArtistSongs(){
		return artistSongs;
	}

	public int getAlbumId(){
		return albumId;
	}

	public int getId(){
		return id;
	}

	public String getKeyword(){
		return keyword;
	}

	public int getPlanId(){
		return planId;
	}

	public int getStatus(){
		return status;
	}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeString(image);
		dest.writeInt(paidStatus);
		dest.writeString(description);
		dest.writeString(uploadSong);
		dest.writeString(createdAt);
		dest.writeInt(languageId);
		dest.writeString(type);
		dest.writeString(duration);
		dest.writeInt(likeDislikeSongsCount);
		dest.writeInt(categoryId);
		dest.writeString(updatedAt);
		dest.writeString(name);
		dest.writeInt(albumId);
		dest.writeInt(id);
		dest.writeString(keyword);
		dest.writeInt(planId);
		dest.writeInt(status);
	}

	public void setLikeDislikeSongsCount(int likeDislikeSongsCount)
	{
		this.likeDislikeSongsCount = likeDislikeSongsCount;
	}

}