package com.webmobril.nyimboapp.Models;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class GetUserArtistResult {

	@SerializedName("artists")
	private List<GetUserArtistModel> artists;

	public List<GetUserArtistModel> getArtists(){
		return artists;
	}
}