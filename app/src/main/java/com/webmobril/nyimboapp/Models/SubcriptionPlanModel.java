package com.webmobril.nyimboapp.Models;

public class SubcriptionPlanModel
{
    private int id;

    public String getTitle_plan1() {
        return title_plan1;
    }

    public String getTitle_text1() {
        return title_text1;
    }

    public String getTitle_plan2() {
        return title_plan2;
    }

    public String getTitle_text2() {
        return title_text2;
    }

    public void setTitle_plan1(String title_plan1) {
        this.title_plan1 = title_plan1;
    }

    public void setTitle_text1(String title_text1) {
        this.title_text1 = title_text1;
    }

    public void setTitle_plan2(String title_plan2) {
        this.title_plan2 = title_plan2;
    }

    public void setTitle_text2(String title_text2) {
        this.title_text2 = title_text2;
    }

    private String title_plan1;
    private String title_text1;
    private String title_plan2;
    private String title_text2;

    public SubcriptionPlanModel(String title_plan1, String title_text1,String title_plan2, String title_text2)
    {
        this.title_plan1=title_plan1;
        this.title_text1=title_text1;
        this.title_plan2=title_plan2;
        this.title_text2=title_text2;

    }
    public int getId()
    {
        return id;
    }
    public void setId(int id)
    {
        this.id = id;
    }


}
