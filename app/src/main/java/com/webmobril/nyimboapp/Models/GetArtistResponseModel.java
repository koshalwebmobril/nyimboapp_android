package com.webmobril.nyimboapp.Models;

import com.google.gson.annotations.SerializedName;

public class GetArtistResponseModel {

	@SerializedName("image")
	private String image;

	@SerializedName("updated_at")
	private String updatedAt;

	@SerializedName("color")
	private String color;

	@SerializedName("name")
	private String name;

	@SerializedName("created_at")
	private String createdAt;

	@SerializedName("language")
	private String language;

	@SerializedName("id")
	private int id;

	@SerializedName("language_id")
	private int languageId;

	@SerializedName("artist_id")
	private int artistId;

	@SerializedName("info")
	private String info;

	@SerializedName("status")
	private int status;


	private boolean isSelected = false;

	public boolean isSelected() {
		return isSelected;
	}


	public void setSelectedArtist(boolean selected) {
		isSelected = selected;
	}

	public String getImage(){
		return image;
	}

	public String getUpdatedAt(){
		return updatedAt;
	}

	public String getColor(){
		return color;
	}

	public String getName()
	{
		return name;
	}

	public String getCreatedAt(){
		return createdAt;
	}

	public String getLanguage(){
		return language;
	}

	public int getId(){
		return id;
	}

	public int getLanguageId(){
		return languageId;
	}

	public int getArtistId(){
		return artistId;
	}

	public String getInfo(){
		return info;
	}

	public int getStatus(){
		return status;
	}



}