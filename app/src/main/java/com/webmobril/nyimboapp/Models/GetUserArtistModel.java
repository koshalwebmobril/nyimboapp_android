package com.webmobril.nyimboapp.Models;

import com.google.gson.annotations.SerializedName;

public class GetUserArtistModel {

	@SerializedName("image")
	private String image;

	@SerializedName("updated_at")
	private String updatedAt;

	@SerializedName("user_id")
	private int userId;

	@SerializedName("name")
	private String name;

	@SerializedName("created_at")
	private String createdAt;

	@SerializedName("id")
	private int id;

	@SerializedName("type")
	private String type;

	@SerializedName("artist_id")
	private int artistId;

	@SerializedName("info")
	private String info;

	@SerializedName("status")
	private int status;

	@SerializedName("song_artist_count")
	private int songArtistCount;

	public String getImage(){
		return image;
	}

	public String getUpdatedAt(){
		return updatedAt;
	}

	public int getUserId(){
		return userId;
	}

	public String getName(){
		return name;
	}

	public String getCreatedAt(){
		return createdAt;
	}

	public int getId(){
		return id;
	}

	public String getType(){
		return type;
	}

	public int getArtistId(){
		return artistId;
	}

	public String getInfo(){
		return info;
	}

	public int getStatus(){
		return status;
	}

	public int getSongArtistCount(){
		return songArtistCount;
	}
}