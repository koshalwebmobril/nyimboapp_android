package com.webmobril.nyimboapp.Models;

import com.google.gson.annotations.SerializedName;

public class PodcastEpisodeItemModel {

	@SerializedName("image")
	private String image;

	@SerializedName("year")
	private String year;

	@SerializedName("created_at")
	private String createdAt;

	@SerializedName("description")
	private String description;

	@SerializedName("upload_song")
	private String uploadSong;

	@SerializedName("episode")
	private int episode;

	@SerializedName("language_id")
	private int languageId;

	@SerializedName("artist_id")
	private int artistId;

	@SerializedName("genre_id")
	private Object genreId;

	@SerializedName("category_id")
	private int categoryId;

	@SerializedName("updated_at")
	private String updatedAt;

	@SerializedName("user_id")
	private Object userId;

	@SerializedName("name")
	private String name;

	@SerializedName("album_id")
	private int albumId;

	@SerializedName("id")
	private int id;

	@SerializedName("keyword")
	private String keyword;

	@SerializedName("status")
	private int status;

	public String getImage(){
		return image;
	}

	public String getYear(){
		return year;
	}

	public String getCreatedAt(){
		return createdAt;
	}

	public String getDescription(){
		return description;
	}

	public String getUploadSong(){
		return uploadSong;
	}

	public int getEpisode(){
		return episode;
	}

	public int getLanguageId(){
		return languageId;
	}

	public int getArtistId(){
		return artistId;
	}

	public Object getGenreId(){
		return genreId;
	}

	public int getCategoryId(){
		return categoryId;
	}

	public String getUpdatedAt(){
		return updatedAt;
	}

	public Object getUserId(){
		return userId;
	}

	public String getName(){
		return name;
	}

	public int getAlbumId(){
		return albumId;
	}

	public int getId(){
		return id;
	}

	public String getKeyword(){
		return keyword;
	}

	public int getStatus(){
		return status;
	}
}