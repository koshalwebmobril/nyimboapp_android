package com.webmobril.nyimboapp.Models;

import com.google.gson.annotations.SerializedName;

public class AuthenticationModel
{
        @SerializedName("token")
        String  token;

        @SerializedName("id")
        int id;

        @SerializedName("username")
        String username;

        @SerializedName("email")
        String email;

        @SerializedName("otp")
        String otp;

        @SerializedName("mobile")
        String mobile;

        @SerializedName("dob")
        String dob;

        @SerializedName("gender")
        String gender;

        @SerializedName("language_status")
        int language_status;

        @SerializedName("artist_status")
        int artist_status;

        public String getToken() {
                return token;
        }

        public void setToken(String token) {
                this.token = token;
        }

        public int getId() {
                return id;
        }
        public void setId(int id) {
                this.id = id;
        }

        public String getUsername() {
                return username;
        }

        public void setUsername(String username) {
                this.username = username;
        }

        public String getEmail() {
                return email;
        }

        public void setEmail(String email) {
                this.email = email;
        }

        public String getOtp() {
                return otp;
        }

        public void setOtp(String otp) {
                this.otp = otp;
        }

        public String getMobile() {
                return mobile;
        }

        public void setMobile(String mobile) {
                this.mobile = mobile;
        }

        public String getDob() {
                return dob;
        }

        public void setDob(String dob) {
                this.dob = dob;
        }

        public String getGender() {
                return gender;
        }
        public void setGender(String gender) {
                this.gender = gender;
        }


        public int getLanguage_status() {
                return language_status;
        }
        public void setLanguage_status(int language_status) {
                this.language_status =language_status;
        }

        public int getArtist_status() {
                return artist_status;
        }
        public void setArtist_status(int artist_status) {
                this.artist_status =artist_status;
        }
}
