package com.webmobril.nyimboapp.Models;

import com.google.gson.annotations.SerializedName;

public class PodcastCategoryModel {

	@SerializedName("cat_img")
	private String catImg;

	@SerializedName("color")
	private String color;

	@SerializedName("updated_at")
	private String updatedAt;

	@SerializedName("name")
	private String name;

	@SerializedName("created_at")
	private String createdAt;

	@SerializedName("id")
	private int id;

	@SerializedName("type")
	private int type;

	@SerializedName("status")
	private String status;

	public String getCatImg(){
		return catImg;
	}

	public String getColor(){
		return color;
	}

	public String getUpdatedAt(){
		return updatedAt;
	}

	public String getName(){
		return name;
	}

	public String getCreatedAt(){
		return createdAt;
	}

	public int getId(){
		return id;
	}

	public int getType(){
		return type;
	}

	public String getStatus(){
		return status;
	}
}