package com.webmobril.nyimboapp.Models;

import com.google.gson.annotations.SerializedName;

public class AlbumsListModel1 {

	@SerializedName("image")
	private String image;

	@SerializedName("paid_status")
	private int paidStatus;

	@SerializedName("name")
	private String name;

	@SerializedName("id")
	private int id;

	public String getImage(){
		return image;
	}

	public int getPaidStatus(){
		return paidStatus;
	}

	public String getName(){
		return name;
	}

	public int getId(){
		return id;
	}
}