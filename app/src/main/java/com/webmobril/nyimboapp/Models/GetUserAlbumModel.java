package com.webmobril.nyimboapp.Models;

import com.google.gson.annotations.SerializedName;

public class GetUserAlbumModel {

	@SerializedName("image")
	private String image;

	@SerializedName("album_name")
	private String albumName;

	@SerializedName("album_id")
	private int albumId;

	@SerializedName("songs_count")
	private int songsCount;

	public String getImage(){
		return image;
	}

	public String getAlbumName(){
		return albumName;
	}

	public int getAlbumId(){
		return albumId;
	}

	public int getSongsCount(){
		return songsCount;
	}
}