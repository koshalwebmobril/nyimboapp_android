package com.webmobril.nyimboapp.Models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class ArtistSongswithnameModel implements Parcelable {

	@SerializedName("image")
	private String image;

	@SerializedName("artist_name")
	private String artistName;

	protected ArtistSongswithnameModel(Parcel in) {
		image = in.readString();
		artistName = in.readString();
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeString(image);
		dest.writeString(artistName);
	}

	@Override
	public int describeContents() {
		return 0;
	}

	public static final Creator<ArtistSongswithnameModel> CREATOR = new Creator<ArtistSongswithnameModel>() {
		@Override
		public ArtistSongswithnameModel createFromParcel(Parcel in) {
			return new ArtistSongswithnameModel(in);
		}

		@Override
		public ArtistSongswithnameModel[] newArray(int size) {
			return new ArtistSongswithnameModel[size];
		}
	};

	public String getImage(){
		return image;
	}

	public String getArtistName(){
		return artistName;
	}
}