package com.webmobril.nyimboapp.Models;

public class MusicListModel
{
    private int id;
    private int image;
    private String name;


    public MusicListModel(int image, String name)
    {
        this.image=image;
        this.name=name;

    }
    public int getId()
    {
        return id;
    }
    public void setId(int id)
    {
        this.id = id;
    }

    public String getName()
    {
        return name;
    }
    public void setName(String name)
    {
        this.name =name;
    }



    public int getImage()
    {
        return image;
    }
    public void setImage(int image)
    {
        this.image =image;
    }
}
