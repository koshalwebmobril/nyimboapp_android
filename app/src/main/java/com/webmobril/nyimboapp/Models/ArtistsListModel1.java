package com.webmobril.nyimboapp.Models;

import com.google.gson.annotations.SerializedName;

public class ArtistsListModel1 {

	@SerializedName("image")
	private String image;

	@SerializedName("name")
	private String name;

	@SerializedName("artist_id")
	private int artistId;

	public String getImage(){
		return image;
	}

	public String getName(){
		return name;
	}

	public int getArtistId(){
		return artistId;
	}
}