package com.webmobril.nyimboapp.fragments;

import android.content.Context;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.webmobril.nyimboapp.Activities.MainActivity;
import com.webmobril.nyimboapp.Adapters.ArtistAdapter;
import com.webmobril.nyimboapp.Adapters.ParticularAdapter;
import com.webmobril.nyimboapp.Adapters.TopAlbumsAdapter;
import com.webmobril.nyimboapp.Adapters.TrendingSongAdapter;
import com.webmobril.nyimboapp.Models.AlbumsListModel1;

import com.webmobril.nyimboapp.Models.ArtistsListModel1;
import com.webmobril.nyimboapp.Models.GenreListModel1;
import com.webmobril.nyimboapp.Models.TrandingSongsModel;
import com.webmobril.nyimboapp.R;
import com.webmobril.nyimboapp.Response.HomePageResponse;
import com.webmobril.nyimboapp.Sharedpreference.LoginPreferences;
import com.webmobril.nyimboapp.Utils.ProgressD;
import com.webmobril.nyimboapp.Utils.UrlApi;
import com.webmobril.nyimboapp.databinding.FragmentHomeBinding;
import com.webmobril.nyimboapp.network.ApiInterface;


import java.util.ArrayList;
import java.util.List;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

public class HomeFragment extends Fragment implements View.OnClickListener
{
        View view;
        RecyclerView recyclerview_trandingsong;
        RecyclerView recyclerview_artist;
        RecyclerView recyclerview_topalbums;
        RecyclerView recyclerview_particular_song;

        TrendingSongAdapter trendingSongAdapter;
        ArtistAdapter artistadapter;
        TopAlbumsAdapter topAlbumsAdapter;
        ParticularAdapter particularsongadapter;
        FragmentHomeBinding binding;

        @Override
        public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            binding = DataBindingUtil.inflate(inflater, R.layout.fragment_home, container, false);
            view = binding.getRoot();
            ((MainActivity)requireActivity()).toolbarHome();
            init();
            HomeApi();
            return view;
        }
        @Override
        public void onAttach(@NonNull Context context)
        {
            super.onAttach(context);
        }

        @Override
        public void onCreate(Bundle savedInstanceState)
        {
            super.onCreate(savedInstanceState);
            setHasOptionsMenu(true);
        }

        public void init()
        {
            recyclerview_trandingsong=view.findViewById(R.id.recyclerview_trandingsong);
            recyclerview_artist=view.findViewById(R.id.recyclerview_artist);
            recyclerview_topalbums=view.findViewById(R.id.recyclerview_top_albums);
            recyclerview_particular_song=view.findViewById(R.id.recyclerview_particular_song);
        }
        @Override
        public void onClick(View v)
        {
            switch (v.getId())
            {
                case R.id.imgBack:
                  //  backpressed();
                    break;
            }
        }

        private void HomeApi()
       {
        final ProgressD progressDialog = ProgressD.show(getActivity(),getResources().getString(R.string.logging_in), true, false, null);
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();
        Gson gson = new GsonBuilder()
                .setLenient()
                .create();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(UrlApi.BASE_URL)
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(client)
                .build();
        ApiInterface service = retrofit.create(ApiInterface.class);

        Call<HomePageResponse> call = service.homePage(LoginPreferences.getActiveInstance(getContext()).getUser_id());
        call.enqueue(new Callback<HomePageResponse>()
        {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onResponse(Call<HomePageResponse> call, retrofit2.Response<HomePageResponse> response)
            {
                progressDialog.dismiss();
                try
                {
                    HomePageResponse resultFile = response.body();
                    if(resultFile.getStatus().equals("true"))
                    {
                        List<TrandingSongsModel> trandingsongmodel=resultFile.getHomePageResultResponse().getTrandingSongs();
                        List<ArtistsListModel1>  artistlistmodel=resultFile.getHomePageResultResponse().getArtists();
                        List<AlbumsListModel1>   topalubmslist=resultFile.getHomePageResultResponse().getAlbums();
                        List<GenreListModel1>    genralitem=resultFile.getHomePageResultResponse().getGenre();

                        recyclerview_trandingsong.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));
                        trendingSongAdapter = new TrendingSongAdapter(getActivity(), (ArrayList) trandingsongmodel);
                        recyclerview_trandingsong.setAdapter(trendingSongAdapter);

                        recyclerview_artist.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));
                        artistadapter = new ArtistAdapter(getActivity(), artistlistmodel);
                        recyclerview_artist.setAdapter(artistadapter);

                        recyclerview_topalbums.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));
                        topAlbumsAdapter = new TopAlbumsAdapter(getActivity(), (ArrayList) topalubmslist);
                        recyclerview_topalbums.setAdapter(topAlbumsAdapter);

                        recyclerview_particular_song.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));
                        particularsongadapter = new ParticularAdapter(getActivity(), (ArrayList) genralitem);
                        recyclerview_particular_song.setAdapter(particularsongadapter);

                        binding.trendingSongText.setText("Trending songs");
                        binding.artistText.setText("Artist");
                        binding.topAlbum.setText("Top Albums");
                        binding.particularCategory.setText("Particular Category of Songs");
                    }
                    else
                    { }
                }
                catch (Exception e)
                {
                    Log.e("Login Faild", e.toString());
                }
            }
            @Override
            public void onFailure(Call<HomePageResponse> call, Throwable t) { }
        });
    }
}