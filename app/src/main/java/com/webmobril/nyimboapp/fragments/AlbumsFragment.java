package com.webmobril.nyimboapp.fragments;

import android.content.Context;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.webmobril.nyimboapp.Adapters.AlbumsListAdapter;
import com.webmobril.nyimboapp.Models.GetUserAlbumModel;
import com.webmobril.nyimboapp.R;
import com.webmobril.nyimboapp.Response.GetUserAlbumResponse;
import com.webmobril.nyimboapp.Sharedpreference.LoginPreferences;
import com.webmobril.nyimboapp.Utils.ProgressD;
import com.webmobril.nyimboapp.Utils.UrlApi;
import com.webmobril.nyimboapp.network.ApiInterface;

import java.util.List;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

public class AlbumsFragment extends Fragment
{
    View view;
    RecyclerView recyclerview;
    AlbumsListAdapter albumsListAdapter;
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        view= inflater.inflate(R.layout.fragment_albums, container, false);
        init();
        hitUserAlbum();
        return view;
    }

    @Override
    public void onAttach(@NonNull Context context)
    {
        super.onAttach(context);
       /* if(context!=null)
            onSignupClick=(OnSignupClick) context;*/
    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    public void init()
    {
        recyclerview=view.findViewById(R.id.recyclerview);
    }
    private void hitUserAlbum()
    {
        final ProgressD progressDialog = ProgressD.show(getActivity(), getResources().getString(R.string.logging_in), true, false, null);
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();
        Gson gson = new GsonBuilder()
                .setLenient()
                .create();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(UrlApi.BASE_URL)
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(client)
                .build();
        ApiInterface service = retrofit.create(ApiInterface.class);
        Call<GetUserAlbumResponse> call = service.getuseralbum(LoginPreferences.getActiveInstance(getActivity()).getUser_id());
        call.enqueue(new Callback<GetUserAlbumResponse>()
        {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onResponse(Call<GetUserAlbumResponse> call, retrofit2.Response<GetUserAlbumResponse> response) {
                progressDialog.dismiss();
                try {
                    GetUserAlbumResponse resultFile = response.body();
                    if (resultFile.getStatus().equals("true"))
                    {
                        List<GetUserAlbumModel> getuseralbumlist =  resultFile.getResult();
                        LinearLayoutManager mLayoutManager = new LinearLayoutManager(getActivity(),RecyclerView.VERTICAL,false);
                        recyclerview.setLayoutManager(mLayoutManager);
                        albumsListAdapter = new AlbumsListAdapter(getActivity(),getuseralbumlist);
                        recyclerview.setAdapter(albumsListAdapter);
                    }
                    else
                    { }
                } catch (Exception e) {
                    Log.e("Login Faild", e.toString());
                }
            }

            @Override
            public void onFailure(Call<GetUserAlbumResponse> call, Throwable t) {
            }
        });
    }

}