package com.webmobril.nyimboapp.fragments;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.webmobril.nyimboapp.Activities.MainActivity;
import com.webmobril.nyimboapp.R;
import com.webmobril.nyimboapp.databinding.FragmentProfileBinding;


public class LibraryFragment extends Fragment
{

    FragmentProfileBinding binding;
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_library, container, false);
        View view = binding.getRoot();
        ((MainActivity)requireActivity()).toolbarOther("Library");
        init();
        return view;
    }
    private void init()
    {

    }
}