package com.webmobril.nyimboapp.fragments;

import android.content.Context;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.material.tabs.TabLayout;
import com.webmobril.nyimboapp.Adapters.PlaylistsAlubmsArtistsAdapter;
import com.webmobril.nyimboapp.R;

import java.util.ArrayList;


public class MusicFragment extends Fragment {

    View view;
    ViewPager simpleViewPager;
    TabLayout tabLayout;
    private ArrayList<Fragment> fragmentArrayList;
    private ArrayList<String> titleList;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState)
    {
        view= inflater.inflate(R.layout.fragment_music, container, false);
        init();

        fragmentArrayList.add(new PlayListsFragment());
        titleList.add("Playlists");

        fragmentArrayList.add(new ArtistsFragment());
        titleList.add("Artists");
        tabLayout.setupWithViewPager(simpleViewPager);
        simpleViewPager.setOffscreenPageLimit(3);

        fragmentArrayList.add(new AlbumsFragment());
        titleList.add("Albums");
        tabLayout.setupWithViewPager(simpleViewPager);
        simpleViewPager.setOffscreenPageLimit(3);

        PlaylistsAlubmsArtistsAdapter adapter = new PlaylistsAlubmsArtistsAdapter(getActivity().getSupportFragmentManager(), fragmentArrayList,titleList);
        simpleViewPager.setAdapter(adapter);
        // addOnPageChangeListener event change the tab on slide
        simpleViewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        return view;
    }
    @Override
    public void onAttach(@NonNull Context context)
    {
        super.onAttach(context);
       /* if(context!=null)
            onSignupClick=(OnSignupClick) context;*/
    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);

    }
    private void init()
    {
        simpleViewPager = (ViewPager) view.findViewById(R.id.simpleViewPager);
        tabLayout = (TabLayout) view.findViewById(R.id.simpleTabLayout);
        fragmentArrayList=new ArrayList<>();
        titleList=new ArrayList<>();
    }

}