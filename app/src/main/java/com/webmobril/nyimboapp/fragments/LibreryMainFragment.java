package com.webmobril.nyimboapp.fragments;

import android.annotation.SuppressLint;
import android.graphics.Color;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.webmobril.nyimboapp.Activities.MainActivity;
import com.webmobril.nyimboapp.R;
import com.webmobril.nyimboapp.databinding.FragmentLibreryMainBinding;


public class LibreryMainFragment extends Fragment  implements View.OnClickListener
{
       FragmentLibreryMainBinding binding;
       View view;
       TextView music,Podcasts;
        @Override
        public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            binding = DataBindingUtil.inflate(inflater, R.layout.fragment_librery_main, container, false);
             view = binding.getRoot();
            ((MainActivity)requireActivity()).toolbarHome();
            init();
            return view;
        }
        @SuppressLint("ResourceAsColor")
        private void init()
        {
            music=view.findViewById(R.id.music);
            Podcasts=view.findViewById(R.id.Podcasts);
            binding.music.setOnClickListener(this);
            binding.Podcasts.setOnClickListener(this);

            FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
            Fragment fragmentMusic = fragmentManager.findFragmentByTag("music");
            fragmentMusic = new MusicFragment();
            FragmentTransaction transaction = fragmentManager.beginTransaction();
            transaction.replace(R.id.music_podcasts_framelayout, fragmentMusic, "music");
            transaction.commit();

        }


    @SuppressLint("ResourceAsColor")
    @Override
    public void onClick(View v)
    {
        if (v.getId() == R.id.music)
        {
            FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
            Fragment fragmentMusic = fragmentManager.findFragmentByTag("music");
            if (fragmentMusic == null)
            {
                fragmentMusic = new MusicFragment();
            }
            FragmentTransaction transaction = fragmentManager.beginTransaction();
            transaction.replace(R.id.music_podcasts_framelayout, fragmentMusic, "music");
            transaction.commit();


            Podcasts.setTextColor(Color.parseColor("#5D726F"));            //first time color
            music.setTextColor(Color.parseColor("#FFFFFF"));

        }

        if (v.getId() == R.id.Podcasts)
        {
            FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
            Fragment fragmentPodcasts = fragmentManager.findFragmentByTag("podcasts");
            if ( fragmentPodcasts == null)
            {
                fragmentPodcasts = new PodcastsFragment();
            }
            FragmentTransaction transaction = fragmentManager.beginTransaction();
            transaction.replace(R.id.music_podcasts_framelayout,  fragmentPodcasts, "podcasts");
            transaction.commit();

            music.setTextColor(Color.parseColor("#5D726F"));
            Podcasts.setTextColor(Color.parseColor("#FFFFFF"));
        }
    }
}