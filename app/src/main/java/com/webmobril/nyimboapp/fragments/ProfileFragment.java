package com.webmobril.nyimboapp.fragments;

import android.os.Build;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.webmobril.nyimboapp.Activities.MainActivity;
import com.webmobril.nyimboapp.Adapters.ArtistAdapter;
import com.webmobril.nyimboapp.Adapters.ParticularAdapter;
import com.webmobril.nyimboapp.Adapters.TopAlbumsAdapter;
import com.webmobril.nyimboapp.Adapters.TrendingSongAdapter;
import com.webmobril.nyimboapp.Models.AlbumsListModel1;
import com.webmobril.nyimboapp.Models.ArtistsListModel1;
import com.webmobril.nyimboapp.Models.GenreListModel1;
import com.webmobril.nyimboapp.Models.GetProfileModel;
import com.webmobril.nyimboapp.Models.TrandingSongsModel;
import com.webmobril.nyimboapp.R;
import com.webmobril.nyimboapp.Response.GetProfileApiResponse;
import com.webmobril.nyimboapp.Response.HomePageResponse;
import com.webmobril.nyimboapp.Sharedpreference.LoginPreferences;
import com.webmobril.nyimboapp.Utils.ProgressD;
import com.webmobril.nyimboapp.Utils.UrlApi;
import com.webmobril.nyimboapp.databinding.FragmentProfileBinding;
import com.webmobril.nyimboapp.network.ApiInterface;

import java.util.ArrayList;
import java.util.List;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;


public class ProfileFragment extends Fragment
{
       FragmentProfileBinding binding;
       @Override
        public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
      {
            binding = DataBindingUtil.inflate(inflater, R.layout.fragment_profile, container, false);
            View view = binding.getRoot();
            ((MainActivity)requireActivity()).toolbarOther("Profile");
            init();
            hitProfileApi();
            return view;
        }
        private void init()
        {

        }


    private void hitProfileApi()
    {
        final ProgressD progressDialog = ProgressD.show(getActivity(),getResources().getString(R.string.logging_in), true, false, null);
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();
        Gson gson = new GsonBuilder()
                .setLenient()
                .create();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(UrlApi.BASE_URL)
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(client)
                .build();
        ApiInterface service = retrofit.create(ApiInterface.class);

        Call<GetProfileApiResponse> call = service.getProfile(LoginPreferences.getActiveInstance(getContext()).getUser_id());
        call.enqueue(new Callback<GetProfileApiResponse>()
        {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onResponse(Call<GetProfileApiResponse> call, retrofit2.Response<GetProfileApiResponse> response)
            {
                progressDialog.dismiss();
                try
                {
                    GetProfileApiResponse resultFile = response.body();
                    if(resultFile.getStatus().equals("true"))
                    {
                        GetProfileModel getProfileModel=resultFile.getGetProfileModel();
                        binding.textName.setText(getProfileModel.getUsername());
                        if(!getProfileModel.getEmail().equals(""))
                        {
                            binding.textEmail.setText(getProfileModel.getEmail());
                        }
                        binding.textMobileNo.setText(getProfileModel.getMobile().toString());
                    }
                }
                catch (Exception e)
                {
                    Log.e("Login Faild", e.toString());
                }
            }
            @Override
            public void onFailure(Call<GetProfileApiResponse> call, Throwable t) { }
        });
    }

}