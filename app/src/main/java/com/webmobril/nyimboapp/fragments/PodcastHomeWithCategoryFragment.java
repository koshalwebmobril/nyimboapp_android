package com.webmobril.nyimboapp.fragments;

import android.content.Context;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.webmobril.nyimboapp.Adapters.FeaturedPodcastsAdapter;
import com.webmobril.nyimboapp.Adapters.PodcastCategoryAdapter;
import com.webmobril.nyimboapp.Adapters.PopularPodcastsAdapter;
import com.webmobril.nyimboapp.Adapters.TopSeriesAdapter;
import com.webmobril.nyimboapp.Models.PodcastCategoryModel;
import com.webmobril.nyimboapp.Models.PodcastFeaturedModel;
import com.webmobril.nyimboapp.Models.PopularPodcastsModel1;
import com.webmobril.nyimboapp.Models.TopSeriesModel;
import com.webmobril.nyimboapp.R;
import com.webmobril.nyimboapp.Response.GetPodcastAllDataResponse;
import com.webmobril.nyimboapp.Response.GetPodcastCategoryResponse;
import com.webmobril.nyimboapp.Sharedpreference.LoginPreferences;
import com.webmobril.nyimboapp.Utils.ProgressD;
import com.webmobril.nyimboapp.Utils.UrlApi;
import com.webmobril.nyimboapp.databinding.FragmentCategoryPodcastBinding;
import com.webmobril.nyimboapp.network.ApiInterface;

import java.util.ArrayList;
import java.util.List;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;


public class PodcastHomeWithCategoryFragment extends Fragment
{
        View view;
        PopularPodcastsAdapter popularPodcastsAdapter;
        RecyclerView recyclerview_popular_podcasts;
        RecyclerView recyclerview_top_series;
        RecyclerView recyclerview_feature_podcast;
        RelativeLayout relative_feature_podcast;
        TextView populatpodcast_textview,top_series;
        View view_popular_podcast,view_topseries;
        LinearLayout linear_featured_podcast;

     TopSeriesAdapter topseriesadapter;
     FeaturedPodcastsAdapter featurepodcastAdapter;
     FragmentCategoryPodcastBinding binding;
     String category_id;

        @Override
        public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            binding = DataBindingUtil.inflate(inflater, R.layout.fragment_category_podcast, container, false);
            view = binding.getRoot();
            init();
            hitgetpodcastalbumApi();
            return view;
        }
        @Override
        public void onAttach(@NonNull Context context)
        {
            super.onAttach(context);
        }

        @Override
        public void onCreate(Bundle savedInstanceState)
        {
            super.onCreate(savedInstanceState);
            setHasOptionsMenu(true);
        }
        public void init()
        {
            category_id = getArguments().getString("category_id");
            populatpodcast_textview=view.findViewById(R.id.populatpodcast_textview);
            top_series=view.findViewById(R.id.top_series);
            relative_feature_podcast=view.findViewById(R.id.relative_feature_podcast);
            recyclerview_popular_podcasts=view.findViewById(R.id.recyclerview_popular_podcasts);
            recyclerview_top_series=view.findViewById(R.id.recyclerview_top_series);
            recyclerview_feature_podcast=view.findViewById(R.id.recyclerview_feature_podcast);

            view_popular_podcast=view.findViewById(R.id.view_popular_podcast);
            view_topseries=view.findViewById(R.id.view_topseries);
            linear_featured_podcast=view.findViewById(R.id.linear_featured_podcast);
        }

    private void hitgetpodcastalbumApi()
    {
        final ProgressD progressDialog = ProgressD.show(getActivity(),getResources().getString(R.string.logging_in), true, false, null);
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();
        Gson gson = new GsonBuilder()
                .setLenient()
                .create();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(UrlApi.BASE_URL)
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(client)
                .build();
        ApiInterface service = retrofit.create(ApiInterface.class);

        Call<GetPodcastAllDataResponse> call = service.getpodcastallitems(LoginPreferences.getActiveInstance(getActivity()).getUser_id(),category_id);
        call.enqueue(new Callback<GetPodcastAllDataResponse>()
        {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onResponse(Call<GetPodcastAllDataResponse> call, retrofit2.Response<GetPodcastAllDataResponse> response)
            {
                progressDialog.dismiss();
                try
                {
                    GetPodcastAllDataResponse resultFile = response.body();
                    if(resultFile.getStatus().equals("true"))
                    {
                        List<PopularPodcastsModel1> popularpodcastlist=resultFile.getAllPodcastResult().getPopularPodcasts();
                        List<TopSeriesModel> topserieslist=resultFile.getAllPodcastResult().getTopSeries();
                        List<PodcastFeaturedModel> featuredpodcastlist=resultFile.getAllPodcastResult().getPodcastCategories();

                        populatpodcast_textview.setVisibility(View.VISIBLE);     //
                        view_popular_podcast.setVisibility(View.VISIBLE);

                        top_series.setVisibility(View.VISIBLE);
                        view_topseries.setVisibility(View.VISIBLE);

                        relative_feature_podcast.setVisibility(View.VISIBLE);
                        linear_featured_podcast.setVisibility(View.VISIBLE);

                        recyclerview_popular_podcasts.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));
                        popularPodcastsAdapter = new PopularPodcastsAdapter(getActivity(), popularpodcastlist);
                        recyclerview_popular_podcasts.setAdapter(popularPodcastsAdapter);

                        recyclerview_top_series.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));
                        topseriesadapter = new TopSeriesAdapter(getActivity(), topserieslist);
                        recyclerview_top_series.setAdapter(topseriesadapter);

                        recyclerview_feature_podcast.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));
                        featurepodcastAdapter  = new FeaturedPodcastsAdapter(getActivity(), featuredpodcastlist);
                        recyclerview_feature_podcast.setAdapter(featurepodcastAdapter);
                    }
                    else
                    { }
                }
                catch (Exception e)
                {
                    Log.e("Login Faild", e.toString());
                }
            }
            @Override
            public void onFailure(Call<GetPodcastAllDataResponse> call, Throwable t) { }
        });
    }

}