package com.webmobril.nyimboapp.fragments;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.webmobril.nyimboapp.Activities.MusicListingActivity;
import com.webmobril.nyimboapp.Adapters.GetUserplaylistFirstPageAdapter;
import com.webmobril.nyimboapp.Models.GetPlaylistModel;
import com.webmobril.nyimboapp.R;
import com.webmobril.nyimboapp.Response.CreatePlaylistResponse;
import com.webmobril.nyimboapp.Response.GetPlaylistDownloadListResponse;
import com.webmobril.nyimboapp.Sharedpreference.LoginPreferences;
import com.webmobril.nyimboapp.Utils.ProgressD;
import com.webmobril.nyimboapp.Utils.UrlApi;
import com.webmobril.nyimboapp.network.ApiInterface;

import java.util.List;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;
public class PlayListsFragment extends Fragment implements View.OnClickListener
{
    View view;
    LinearLayout linear_create_playlist;
    Dialog dialog;
    RecyclerView recyclerview_playlist;
    GetUserplaylistFirstPageAdapter userplaylistFirstPageAdapter;
    TextView liked_song_count;
    LinearLayout linear_liked_song;
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        view= inflater.inflate(R.layout.fragment_play_lists, container, false);
        init();
        hituserplaylistApi();
        return view;
    }

    @Override
    public void onAttach(@NonNull Context context)
    {
        super.onAttach(context);
       /* if(context!=null)
            onSignupClick=(OnSignupClick) context;*/
    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    public void init()
    {
        linear_create_playlist=view.findViewById(R.id.linear_create_playlist);
        recyclerview_playlist=view.findViewById(R.id.recyclerview_playlist);
        liked_song_count=view.findViewById(R.id.liked_song_count);
        linear_liked_song=view.findViewById(R.id.linear_liked_song);
        linear_create_playlist.setOnClickListener(this);
        linear_liked_song.setOnClickListener(this);
    }

    @Override
    public void onClick(View v)
    {
        switch (v.getId())
        {
            case R.id.linear_create_playlist:
                openAlertDailog();
                break;


            case R.id.linear_liked_song:
                Intent i=new Intent(getActivity(), MusicListingActivity.class);
                i.putExtra("status_click","liked_song");
                i.putExtra("liked_status","1");
                startActivity(i);
                break;
        }
    }

    private void openAlertDailog()
    {
        dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

        dialog.setContentView(R.layout.item_alert_dailog_create_playlist);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.gravity = Gravity.CENTER;
        dialog.getWindow().setAttributes(lp);

        Button ok = dialog.findViewById(R.id.ok);
        Button cancel = dialog.findViewById(R.id.cancel);
        EditText create_playlist=dialog.findViewById(R.id.create_playlist);


        ok.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                if(!create_playlist.getText().toString().isEmpty())
                {
                    hitCreatPlayListApi(create_playlist.getText().toString().trim());
                }
                else
                {
                    Toast.makeText(getActivity(), "Please enter playlist name", Toast.LENGTH_SHORT).show();
                }
            }
        });

        cancel.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                dialog.dismiss();
            }
        });
        dialog.show();
        dialog.setCanceledOnTouchOutside(false);
    }

    public void hitCreatPlayListApi(String name)
    {
        final ProgressD progressDialog = ProgressD.show(getActivity(),getResources().getString(R.string.logging_in), true, false, null);
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();
        Gson gson = new GsonBuilder()
                .setLenient()
                .create();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(UrlApi.BASE_URL)
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(client)
                .build();
        ApiInterface service = retrofit.create(ApiInterface.class);
        Call<CreatePlaylistResponse> call = service.createPlaylist(LoginPreferences.getActiveInstance(getActivity()).getUser_id(),name);
        call.enqueue(new Callback<CreatePlaylistResponse>()
        {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onResponse(Call<CreatePlaylistResponse> call, retrofit2.Response<CreatePlaylistResponse> response)
            {
                try
                {
                    CreatePlaylistResponse resultFileResponse = response.body();
                    String status = resultFileResponse.getStatus();
                  //  String message = resultFileResponse.getMessage();
                    progressDialog.dismiss();
                    if(status.equals("true"))
                    {
                        Toast.makeText(getActivity(), "Playlist Create Successfully", Toast.LENGTH_SHORT).show();
                        dialog.dismiss();
                    }
                    else
                    { }
                }
                catch (Exception e)
                {
                    Log.e("Login Faild", e.toString());
                    Toast.makeText(getActivity(), "Failed" , Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<CreatePlaylistResponse> call, Throwable t)
            {

                Toast.makeText(getActivity(), "Failed" + t, Toast.LENGTH_SHORT).show();
            }
        });
    }
    public void hituserplaylistApi()
    {
        final ProgressD progressDialog = ProgressD.show(getActivity(),getResources().getString(R.string.logging_in), true, false, null);
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();
        Gson gson = new GsonBuilder()
                .setLenient()
                .create();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(UrlApi.BASE_URL)
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(client)
                .build();
        ApiInterface service = retrofit.create(ApiInterface.class);
        Call<GetPlaylistDownloadListResponse> call = service.getplaylist(LoginPreferences.getActiveInstance(getActivity()).getUser_id());
        call.enqueue(new Callback<GetPlaylistDownloadListResponse>()
        {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onResponse(Call<GetPlaylistDownloadListResponse> call, retrofit2.Response<GetPlaylistDownloadListResponse> response)
            {
                try
                {
                    GetPlaylistDownloadListResponse resultFileResponse = response.body();
                    String status = resultFileResponse.getStatus();
                    progressDialog.dismiss();
                    liked_song_count.setText(String.valueOf(resultFileResponse.getGetPlayListResponselist().getLikedSongsCount())+ " songs");
                    List<GetPlaylistModel> getPlaylistModels  =resultFileResponse.getGetPlayListResponselist().getGetPlaylists();
                    if(status.equals("true"))
                    {
                        LinearLayoutManager mLayoutManager = new LinearLayoutManager(getActivity(), RecyclerView.VERTICAL, false);
                        recyclerview_playlist.setLayoutManager(mLayoutManager);
                        userplaylistFirstPageAdapter = new GetUserplaylistFirstPageAdapter(getActivity(),  getPlaylistModels);
                        recyclerview_playlist.setAdapter(userplaylistFirstPageAdapter);
                    }
                    else
                    { }
                }
                catch (Exception e)
                {
                    Log.e("Login Faild", e.toString());
                    Toast.makeText(getActivity(), "Failed" , Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<GetPlaylistDownloadListResponse> call, Throwable t)
            {

                Toast.makeText(getActivity(), "Failed" + t, Toast.LENGTH_SHORT).show();
            }
        });
    }
}