package com.webmobril.nyimboapp.fragments;

import android.content.Context;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.webmobril.nyimboapp.Adapters.ArtistAdapter;
import com.webmobril.nyimboapp.Adapters.ParticularAdapter;
import com.webmobril.nyimboapp.Adapters.PodcastCategoryAdapter;
import com.webmobril.nyimboapp.Adapters.TopAlbumsAdapter;
import com.webmobril.nyimboapp.Adapters.TrendingSongAdapter;
import com.webmobril.nyimboapp.Models.AlbumsListModel1;
import com.webmobril.nyimboapp.Models.ArtistsListModel1;
import com.webmobril.nyimboapp.Models.GenreListModel1;
import com.webmobril.nyimboapp.Models.PodcastCategoryModel;
import com.webmobril.nyimboapp.Models.TrandingSongsModel;
import com.webmobril.nyimboapp.R;
import com.webmobril.nyimboapp.Response.GetPodcastCategoryResponse;
import com.webmobril.nyimboapp.Response.HomePageResponse;
import com.webmobril.nyimboapp.Sharedpreference.LoginPreferences;
import com.webmobril.nyimboapp.Utils.ProgressD;
import com.webmobril.nyimboapp.Utils.UrlApi;
import com.webmobril.nyimboapp.network.ApiInterface;

import java.util.ArrayList;
import java.util.List;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

public class PodcastsFragment extends Fragment {
    View view;
    RecyclerView recyclerView;
    PodcastCategoryAdapter podcastCategoryAdapter;
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        view= inflater.inflate(R.layout.fragment_podcasts, container, false);
        init();
        hitPodcastCategoryApi();
        return view;
    }

    @Override
    public void onAttach(@NonNull Context context)
    {
        super.onAttach(context);
       /* if(context!=null)
            onSignupClick=(OnSignupClick) context;*/
    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    public void init()
    {
        recyclerView=view.findViewById(R.id.recyclerView);
    }


    private void hitPodcastCategoryApi()
    {
        final ProgressD progressDialog = ProgressD.show(getActivity(),getResources().getString(R.string.logging_in), true, false, null);
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();
        Gson gson = new GsonBuilder()
                .setLenient()
                .create();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(UrlApi.BASE_URL)
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(client)
                .build();
        ApiInterface service = retrofit.create(ApiInterface.class);

        Call<GetPodcastCategoryResponse> call = service.podcastcategory();
        call.enqueue(new Callback<GetPodcastCategoryResponse>()
        {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onResponse(Call<GetPodcastCategoryResponse> call, retrofit2.Response<GetPodcastCategoryResponse> response)
            {
                progressDialog.dismiss();
                try
                {
                    GetPodcastCategoryResponse resultFile = response.body();
                    if(resultFile.getStatus().equals("true"))
                    {

                        List<PodcastCategoryModel> podcastlist=resultFile.getResult();
                        LinearLayoutManager gridLayoutManager = new GridLayoutManager(getActivity(), 2);
                        recyclerView.setLayoutManager(gridLayoutManager); // set LayoutManager to RecyclerView
                        podcastCategoryAdapter = new PodcastCategoryAdapter(getActivity(), (ArrayList) podcastlist);
                        recyclerView.setAdapter(podcastCategoryAdapter);
                    }
                    else
                    { }
                }
                catch (Exception e)
                {
                    Log.e("Login Faild", e.toString());
                }
            }
            @Override
            public void onFailure(Call<GetPodcastCategoryResponse> call, Throwable t) { }
        });
    }

}