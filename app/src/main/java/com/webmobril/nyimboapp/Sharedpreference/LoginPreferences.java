package com.webmobril.nyimboapp.Sharedpreference;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

public class LoginPreferences
{
    private static LoginPreferences preferences = null;
    private static SharedPreferences mPreferences;
    private SharedPreferences.Editor editor;
    private Context context;

    private String token = "token";
    private  String email="email";
    private  String user_id="user_id";
    private  String selected_language="selected_language";
    private  String artist_selected="artist_language";


    public LoginPreferences(Context context) {
        this.context = context;
        setmPreferences(PreferenceManager.getDefaultSharedPreferences(context));
    }

    public SharedPreferences getmPreferences() {
        return mPreferences;
    }

    private void setmPreferences(SharedPreferences mPreferences) {
        this.mPreferences = mPreferences;
    }

    public static LoginPreferences getActiveInstance(Context context) {
        if (preferences == null) {
            preferences = new LoginPreferences(context);
        }
        return preferences;
    }
    public String getToken()
    {
        return mPreferences.getString(this.token, "");
    }

    public void setToken(String token)
    {
        editor = mPreferences.edit();
        editor.putString(this.token, token);
        editor.apply();
    }


    public String getUser_id()
    {
        return mPreferences.getString(this.user_id, "");
    }

    public void setUser_id(int user_id)
    {
        editor = mPreferences.edit();
        editor.putString(this.user_id, String.valueOf(user_id));
        editor.apply();
    }





    public String getSelectedLanguagestatus()
    {
        return mPreferences.getString(this.selected_language, "");
    }

    public void setSelectedLanguagestatus(String selectedlanguage)
    {
        editor = mPreferences.edit();
        editor.putString(this.selected_language, selectedlanguage);
        editor.apply();
    }

    public String getSelectedArtiststatus()
    {
        return mPreferences.getString(this.artist_selected, "");
    }

    public void setSelectedArtiststatus(String selectedartist)
    {
        editor = mPreferences.edit();
        editor.putString(this.artist_selected, artist_selected);
        editor.apply();
    }

   /* public String getEmailResetPassword()
    {
        return mPreferences.getString(this.email, "");
    }

    public void setEmailResetPassword(String email)
    {
        editor = mPreferences.edit();
        editor.putString(this.email, email);
        editor.apply();
    }*/

    public static void deleteAllPreference()
    {
        mPreferences.edit().clear().apply();
    }

}
