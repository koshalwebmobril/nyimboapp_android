package com.webmobril.nyimboapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.Window;

import com.webmobril.nyimboapp.Activities.LoginActivity;
import com.webmobril.nyimboapp.Activities.MainActivity;
import com.webmobril.nyimboapp.Sharedpreference.LoginPreferences;

public class SplashActivity extends AppCompatActivity
{
    private final int SPLASH_DISPLAY_LENGTH = 3000;
    Handler handler;
    private Window mWindow;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        mWindow = getWindow();
        mWindow.getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
        handler = new Handler();

        handler.postDelayed(new Runnable()
        {
            @Override
            public void run()
            {
                if(LoginPreferences.getActiveInstance(SplashActivity.this).getToken().isEmpty() || LoginPreferences.getActiveInstance(SplashActivity.this).getToken().equals(""))
                {
                    Intent intent1 = new Intent(SplashActivity.this, LoginActivity.class);
                    SplashActivity.this.startActivity(intent1);
                    SplashActivity.this.finish();
                }
                else if(LoginPreferences.getActiveInstance(SplashActivity.this).getSelectedLanguagestatus().equals("0"))
                {
                    Intent intent1 = new Intent(SplashActivity.this, LoginActivity.class);
                    SplashActivity.this.startActivity(intent1);
                    SplashActivity.this.finish();
                }
                else if(LoginPreferences.getActiveInstance(SplashActivity.this).getSelectedArtiststatus().equals("0"))
                {
                    Intent intent1 = new Intent(SplashActivity.this, LoginActivity.class);
                    SplashActivity.this.startActivity(intent1);
                    SplashActivity.this.finish();
                }
                else
                {
                    Intent intent2 = new Intent(SplashActivity.this, MainActivity.class);
                    SplashActivity.this.startActivity(intent2);
                    SplashActivity.this.finish();
                }
            }
        }, SPLASH_DISPLAY_LENGTH);

    }
    @Override
    public void onBackPressed()
    {
        super.onBackPressed();
        handler.removeCallbacksAndMessages(null);
        finish();
    }
}