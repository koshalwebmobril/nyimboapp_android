package com.webmobril.nyimboapp.Utils;

import android.app.Dialog;
import android.content.Context;
import android.view.View;
import android.view.Window;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.webmobril.nyimboapp.R;


public class ProgressD extends Dialog {
    public ProgressD(Context context) {
        super(context);
    }

    private ProgressD(Context context, int theme) {
        super(context, theme);
    }

   /* public void onWindowFocusChanged(boolean hasFocus) {
        ProgressBar progressBar = findViewById(R.id.progressBar);
    }*/

    public void setMessage(CharSequence message) {
        if (message != null && message.length() > 0) {
            findViewById(R.id.message).setVisibility(View.VISIBLE);
            TextView txt = findViewById(R.id.message);
            txt.setText(message);
            txt.invalidate();
        }
    }

    public static ProgressD show(Context context, CharSequence message, boolean indeterminate,
                                 boolean cancelable, OnCancelListener cancelListener)
    {
        ProgressD dialog = new ProgressD(context, R.style.ProgressD);
        dialog.setCanceledOnTouchOutside(false);
        dialog.setTitle("");
        dialog.setContentView(R.layout.progress_hud_bg);
        if (message == null || message.length() == 0)
        {
            dialog.findViewById(R.id.message).setVisibility(View.GONE);
        }
        else
            {
           /* TextView txt = dialog.findViewById(R.id.message);
            txt.setText(message);*/
        }
        dialog.setCancelable(cancelable);
        dialog.setOnCancelListener(cancelListener);

        dialog.show();
        Window window = dialog.getWindow();
        assert window != null;
        window.setLayout(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);
        return dialog;
    }
}
