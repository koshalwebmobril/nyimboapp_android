package com.webmobril.nyimboapp.Utils;

public class UrlApi
{
   // public static String BASE_URL = "http://webmobril.org/dev/nyimbo/api/v1/auth/";
    public static String BASE_URL = "http://52.54.1.108/nyimbo/api/v1/auth/";
    public static final String REGISTER="register";
    public static final String LOGIN="login";
    public static final String FORGOTPASSWORD="forget_password";
    public static final String VERIFYOTP="verify_otp";
    public static final String RESETPASSWORD="reset_password";
    public static final String GETLANGUAGE="get_language";
    public static final String GETUSERLANGUAGE="get_user_language";
    public static final String GETARTIST="get_artists";
    public static final String UPDATELANGUAGEAPI="update_language";
    public static final String HomeApi="home_api";
    public static final String SETARTIST="set_user_artist";
    public static final String GETALBUMSONGLIST="get_songs_list";
    public static final String USERLIKEDDISLIKEDSONG="user_liked_disliked_songs";
    public static final String GETPROFILERESPONSE="get_profile";
    public static final String SOCIALLOGIN="social_login";
    public static final String PODCASTCATEGORYAPI="get_podcasts_categories";
    public static final String CREATEPLAYLISTAPI="create_playlist";
    public static final String GETPLAYLISTAPI="get_user_playlist";
    public static final String SETSONGPLAYLISTAPI="set_user_playlist_song";
    public static final String GETUSERARTIST="get_user_artist";
    public static final String GETUSERALBUM="get_user_album";
    public static final String GETPODCASTALLITEMS="get_podcasts_albums";
    public static final String GETPODCASTALBUM="get_podcasts_albums_episodes";
    public static final String GETUSERPLAYLISTSonglist="get_user_playlist_song";
 public static final String MUSICLISTLIKEDSONG="get_user_liked_songs";
}
