package com.webmobril.nyimboapp.Adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.webmobril.nyimboapp.Activities.MusicListingActivity;
import com.webmobril.nyimboapp.Activities.PlayerActivityAllMusicListing;
import com.webmobril.nyimboapp.Models.MusicListModel;
import com.webmobril.nyimboapp.R;

import java.util.ArrayList;


public class AlbumListingAdapter extends RecyclerView.Adapter<AlbumListingAdapter.MyViewHolder>
{
    ArrayList<MusicListModel> artistlist;
    Context context;
    public AlbumListingAdapter(Context context, ArrayList artistlist)
    {
        this.context = context;
        this.artistlist = artistlist;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_album_listing, parent, false);
        MyViewHolder vh = new MyViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position)
    {
        final MusicListModel artistmodel = artistlist.get(position);
        final int artistimage =artistmodel.getImage();
        final String  name= artistmodel.getName();

        holder.artist_imageview.setImageResource(artistimage);
        holder.artist_name.setText(name);

        holder.itemView.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Intent i=new Intent(context, MusicListingActivity.class);
                i.putExtra("selected_id","album_id");
                i.putExtra("artist_image",artistmodel.getImage());
                context.startActivity(i);
            }
        });
    }

    @Override
    public int getItemCount()
    {
        return artistlist.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder
    {
        TextView artist_name;
        ImageView artist_imageview;
        public MyViewHolder(View itemView)
        {
            super(itemView);
            artist_name= itemView.findViewById(R.id.song_name);
            artist_imageview = itemView.findViewById(R.id.artist_imageview);
        }
    }
}