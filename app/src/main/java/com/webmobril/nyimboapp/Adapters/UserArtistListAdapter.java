package com.webmobril.nyimboapp.Adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;
import com.webmobril.nyimboapp.Activities.MusicListingActivity;

import com.webmobril.nyimboapp.Models.GetUserArtistModel;
import com.webmobril.nyimboapp.R;

import java.util.List;
public class UserArtistListAdapter extends RecyclerView.Adapter<UserArtistListAdapter.MyViewHolder>
{
   List<GetUserArtistModel> artistlist;
    Context context;
    public UserArtistListAdapter(Context context, List artistlist)
    {
        this.context = context;
        this.artistlist = artistlist;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_artist_list, parent, false);
        MyViewHolder vh = new MyViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position)
    {
        final GetUserArtistModel artistlistmodel = artistlist.get(position);
        final String  name= artistlistmodel.getName();
        final String songs_number= String.valueOf(artistlistmodel.getSongArtistCount());

        Picasso.get().load(artistlistmodel.getImage()).into(holder.artist_imageview);
        holder.artist_name.setText(name);
        holder.songs_number.setText(songs_number + " songs");

        holder.itemView.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Intent i=new Intent(context, MusicListingActivity.class);
                i.putExtra("album_id",String.valueOf(artistlistmodel.getArtistId()));
                i.putExtra("artist_image",artistlistmodel.getImage());
                i.putExtra("selected_id","artist_id");
                i.putExtra("status_click","home");
                context.startActivity(i);
            }
        });
    }

    @Override
    public int getItemCount()
    {
        return artistlist.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder
    {
        TextView artist_name,songs_number;
        ImageView artist_imageview;
        public MyViewHolder(View itemView)
        {
            super(itemView);
            artist_name= itemView.findViewById(R.id.artist_name);
            songs_number= itemView.findViewById(R.id.song_number);
            artist_imageview = itemView.findViewById(R.id.artist_imageview);
        }
    }
}