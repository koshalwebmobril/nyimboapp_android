package com.webmobril.nyimboapp.Adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;
import com.webmobril.nyimboapp.Activities.AlbumListingActivity;
import com.webmobril.nyimboapp.Activities.MusicListingActivity;
import com.webmobril.nyimboapp.Activities.PlayerActivityAllMusicListing;
import com.webmobril.nyimboapp.Models.GetUserAlbumModel;
import com.webmobril.nyimboapp.R;

import java.util.ArrayList;
import java.util.List;

public class AlbumsListAdapter extends RecyclerView.Adapter<AlbumsListAdapter.MyViewHolder>
{
  List<GetUserAlbumModel> albumslist;
    Context context;
    public AlbumsListAdapter(Context context, List albumslist)
    {
        this.context = context;
        this.albumslist = albumslist;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_albums_list, parent, false);
        MyViewHolder vh = new MyViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position)
    {
        final GetUserAlbumModel albumslistmodel = albumslist.get(position);
        Picasso.get().load(albumslistmodel.getImage()).into(holder.artist_imageview);
        holder.artist_name.setText(albumslistmodel.getAlbumName());
        holder.songs_number.setText(String.valueOf(albumslistmodel.getSongsCount()) + " songs");

        holder.itemView.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Intent i=new Intent(context, MusicListingActivity.class);
                i.putExtra("album_id",String.valueOf(albumslistmodel.getAlbumId()));
                i.putExtra("artist_image",albumslistmodel.getImage());
                i.putExtra("selected_id","album_id");
                i.putExtra("status_click","home");
                context.startActivity(i);
            }
        });
    }

    @Override
    public int getItemCount()
    {
        return albumslist.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder
    {
        TextView artist_name,songs_number;
        ImageView artist_imageview;
        public MyViewHolder(View itemView)
        {
            super(itemView);
            artist_name= itemView.findViewById(R.id.artist_name);
            songs_number= itemView.findViewById(R.id.song_number);
            artist_imageview = itemView.findViewById(R.id.artist_imageview);
        }
    }
}