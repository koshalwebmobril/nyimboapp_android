package com.webmobril.nyimboapp.Adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;
import com.webmobril.nyimboapp.Activities.PodcastAlbumActivity;
import com.webmobril.nyimboapp.Models.PopularPodcastsModel1;
import com.webmobril.nyimboapp.R;

import java.util.ArrayList;
import java.util.List;

public class PopularPodcastsAdapter extends RecyclerView.Adapter<PopularPodcastsAdapter.MyViewHolder>
{
    List<PopularPodcastsModel1> popularpodcastlist;
    Context context;
    public PopularPodcastsAdapter(Context context, List popularpodcastlist)
    {
        this.context = context;
        this.popularpodcastlist = popularpodcastlist;
    }
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_popular_podcast, parent, false);
        MyViewHolder vh = new MyViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position)
    {
        context = holder.itemView.getContext();
        final PopularPodcastsModel1 trendingsongitem = popularpodcastlist.get(position);
        Picasso.get().load(trendingsongitem.getImage()).into(holder.img);

        holder.itemView.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Intent i=new Intent(context, PodcastAlbumActivity.class);
                i.putExtra("album_id",String.valueOf(trendingsongitem.getAlbumId()));
                i.putExtra("category_id",String.valueOf(trendingsongitem.getCategoryId()));
                i.putExtra("image_item",String.valueOf(trendingsongitem.getImage()));
                i.putExtra("description",String.valueOf(trendingsongitem.getDescription()));
                i.putExtra("category_name",String.valueOf(trendingsongitem.getName()));
                i.putExtra("status_click","home");
                context.startActivity(i);
            }
        });
    }

    @Override
    public int getItemCount()
    {
        return popularpodcastlist.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder
    {
        ImageView img;
        public MyViewHolder(View itemView)
        {
            super(itemView);
            img = itemView.findViewById(R.id.singer_image);
        }
    }
}