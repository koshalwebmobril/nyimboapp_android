package com.webmobril.nyimboapp.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;
import com.webmobril.nyimboapp.Models.PodcastFeaturedModel;
import com.webmobril.nyimboapp.Models.TopSeriesItemModel;
import com.webmobril.nyimboapp.R;

import java.util.List;

public class TopSeriesPodcastsAdapter extends RecyclerView.Adapter<TopSeriesPodcastsAdapter.MyViewHolder>
{
    List<TopSeriesItemModel> topseriespodcastlist;
    Context context;

    public TopSeriesPodcastsAdapter(Context context, List topseriespodcastlist)
    {
        this.context = context;
        this.topseriespodcastlist = topseriespodcastlist;
    }
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_featured_podcast, parent, false);
        MyViewHolder vh = new MyViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position)
    {
        context = holder.itemView.getContext();

        final TopSeriesItemModel trendingsongitem = topseriespodcastlist.get(position);

        Picasso.get().load(trendingsongitem.getImage()).into(holder.img);
        holder.artist_name.setText(trendingsongitem.getName());

        holder.itemView.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                /*Intent i=new Intent(context, MainActivity.class);
                context.startActivity(i);*/
            }
        });
    }

    @Override
    public int getItemCount()
    {
        return topseriespodcastlist.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder
    {
        TextView artist_name;
        ImageView img;
        public MyViewHolder(View itemView)
        {
            super(itemView);
            artist_name= itemView.findViewById(R.id.artist_name);
            img = itemView.findViewById(R.id.singer_image);
        }
    }
}