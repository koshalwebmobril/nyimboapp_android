package com.webmobril.nyimboapp.Adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;
import com.webmobril.nyimboapp.Activities.MusicListingActivity;
import com.webmobril.nyimboapp.Models.TrandingSongsModel;
import com.webmobril.nyimboapp.R;

import java.util.ArrayList;
public class TrendingSongAdapter extends RecyclerView.Adapter<TrendingSongAdapter.MyViewHolder>
{
    ArrayList<TrandingSongsModel> trendingSongModelArrayList;
    Context context;
    public TrendingSongAdapter(Context context, ArrayList trendingSongModelArrayList)
    {
        this.context = context;
        this.trendingSongModelArrayList = trendingSongModelArrayList;
    }
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_home_tranding_song, parent, false);
        MyViewHolder vh = new MyViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position)
    {
        context = holder.itemView.getContext();
        final TrandingSongsModel trendingsongitem = trendingSongModelArrayList.get(position);
        Picasso.get().load(trendingsongitem.getImage()).into(holder.img);

         holder.itemView.setOnClickListener(new View.OnClickListener()
         {
            @Override
            public void onClick(View v)
            {
                Intent i=new Intent(context, MusicListingActivity.class);
                i.putExtra("selected_id","album_id");
                i.putExtra("artist_image",trendingsongitem.getImage());
                i.putExtra("album_id",String.valueOf(trendingsongitem.getId()));
                i.putExtra("status_click","home");
                context.startActivity(i);
            }
        });
    }

    @Override
    public int getItemCount()
    {
        return trendingSongModelArrayList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder
    {
        ImageView img;
        public MyViewHolder(View itemView)
        {
            super(itemView);
            img = itemView.findViewById(R.id.singer_image);
        }
    }
}