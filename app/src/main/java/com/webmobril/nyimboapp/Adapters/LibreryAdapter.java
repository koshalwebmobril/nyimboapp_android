package com.webmobril.nyimboapp.Adapters;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import java.util.ArrayList;

public class LibreryAdapter extends FragmentStatePagerAdapter
{
    private ArrayList<Fragment> fragmentArrayList;
    private ArrayList<String> titleList;
    public LibreryAdapter(FragmentManager fm, ArrayList<Fragment> fragmentArrayList, ArrayList<String> titleList)
        {
            super(fm);
            this.fragmentArrayList= fragmentArrayList;
            this.titleList= titleList;
    }

    @Override
    public Fragment getItem(int position)
    {
        return fragmentArrayList!=null?fragmentArrayList.get(position):null;
    }

    @Override
    public int getCount() {
        return fragmentArrayList.size();
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        return titleList.get(position);
    }
}
