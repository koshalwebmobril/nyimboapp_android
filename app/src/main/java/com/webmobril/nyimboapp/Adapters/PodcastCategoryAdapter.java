package com.webmobril.nyimboapp.Adapters;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.webmobril.nyimboapp.Models.PodcastCategoryModel;
import com.webmobril.nyimboapp.R;
import com.webmobril.nyimboapp.fragments.PodcastHomeWithCategoryFragment;

import java.util.ArrayList;
public class PodcastCategoryAdapter extends RecyclerView.Adapter<PodcastCategoryAdapter.MyViewHolder>
{
    ArrayList<PodcastCategoryModel> podcastCategoryModelArrayList;
    Context context;
    public PodcastCategoryAdapter(Context context, ArrayList podcastCategoryModelArrayList)
    {
        this.context = context;
        this.podcastCategoryModelArrayList = podcastCategoryModelArrayList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_selectlanguage, parent, false);
        MyViewHolder vh = new MyViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position)
    {
        context = holder.itemView.getContext();
        final PodcastCategoryModel selectlangaugeItem = podcastCategoryModelArrayList.get(position);
        final String name = selectlangaugeItem.getName();
        final String color_code = selectlangaugeItem.getColor();

        holder.langauge_name.setText(name);
        holder.relative_langauge.setBackgroundResource(R.drawable.round_button_langauge);
        GradientDrawable drawable = (GradientDrawable) holder.relative_langauge.getBackground();
        drawable.setColor(Color.parseColor(color_code));

        holder.itemView.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Bundle args = new Bundle();
                AppCompatActivity activity = (AppCompatActivity) v.getContext();
                PodcastHomeWithCategoryFragment navigation = new PodcastHomeWithCategoryFragment();
                args.putString("category_id",String.valueOf(selectlangaugeItem.getId()));
                navigation.setArguments(args);
                activity.getSupportFragmentManager().beginTransaction().replace(R.id.music_podcasts_framelayout, navigation).addToBackStack(null).commit();
            }
        });
    }
    @Override
    public int getItemCount()
    {
        return podcastCategoryModelArrayList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder
    {
        TextView langauge_name;
       RelativeLayout relative_langauge;

        public MyViewHolder(View itemView)
        {
            super(itemView);
            langauge_name = itemView.findViewById(R.id.langauge_name);
            relative_langauge = itemView.findViewById(R.id.relative_artist);

        }
    }
}