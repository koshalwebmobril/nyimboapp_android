package com.webmobril.nyimboapp.Adapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;
import com.webmobril.nyimboapp.Activities.MainActivity;
import com.webmobril.nyimboapp.Models.GetArtistResponseModel;
import com.webmobril.nyimboapp.Models.SelectArtistModel;
import com.webmobril.nyimboapp.Models.SelectLanguageModel1;
import com.webmobril.nyimboapp.R;

import java.util.ArrayList;
import java.util.List;


public class SelectArtistAdapter extends RecyclerView.Adapter<SelectArtistAdapter.MyViewHolder>
{
    List<GetArtistResponseModel> selectartistlist;
    Context context;
    public ItemCheckListner itemCheckListner;

    public SelectArtistAdapter(Context context, List selectartistlist)
    {
        this.context = context;
        this.selectartistlist = selectartistlist;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_selectartist, parent, false);
        MyViewHolder vh = new MyViewHolder(v);
        return vh;
    }
    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position)
    {
        final GetArtistResponseModel selectlangaugeItem = selectartistlist.get(position);
        holder.artist_name.setText(selectlangaugeItem.getName());
        Picasso.get().load(selectlangaugeItem.getImage()).into(holder.artist_imageview);

        holder.itemView.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Intent i=new Intent(context, MainActivity.class);
                context.startActivity(i);
            }
        });

        holder.itemView.setOnClickListener(view ->
        {
            selectlangaugeItem.setSelectedArtist(!selectlangaugeItem.isSelected());
            itemCheckListner.setCheckedList(String.valueOf(selectlangaugeItem.getArtistId()));
            notifyDataSetChanged();
        });


        if (selectlangaugeItem.isSelected())
        {
            itemCheckListner.remove(selectlangaugeItem);
        }

        if (selectlangaugeItem.isSelected())
        {
            holder.checked.setVisibility(View.VISIBLE);
        }
        else
        {
            holder.checked.setVisibility(View.GONE);
        }
    }

    public interface ItemCheckListner
    {
        void setCheckedList(String list);
        void remove(GetArtistResponseModel selectartistitem);
    }

    public void setCheckedItemListner(SelectArtistAdapter.ItemCheckListner itemCheckListner)
    {
        this.itemCheckListner=itemCheckListner;
    }

    @Override
    public int getItemCount()
    {
        return selectartistlist.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder
    {
        TextView artist_name;
        ImageView artist_imageview;
        ImageView checked;

        public MyViewHolder(View itemView)
        {
            super(itemView);
            artist_name= itemView.findViewById(R.id.artist_name);
            artist_imageview = itemView.findViewById(R.id.artist_imageview);
            checked=itemView.findViewById(R.id.checked);
        }
    }
}