package com.webmobril.nyimboapp.Adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.webmobril.nyimboapp.Activities.MusicListingActivity;
import com.webmobril.nyimboapp.Models.GetPlaylistModel;
import com.webmobril.nyimboapp.R;

import java.util.List;

public class GetUserplaylistFirstPageAdapter extends RecyclerView.Adapter<GetUserplaylistFirstPageAdapter.MyViewHolder>
{
    List<GetPlaylistModel> getPlaylistModelslist;
    Context context;
    public GetUserplaylistFirstPageAdapter(Context context, List getPlaylistModelslist)
    {
        this.context = context;
        this.getPlaylistModelslist= getPlaylistModelslist;
    }
    @Override
    public GetUserplaylistFirstPageAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_firstpage_userplaylist, parent, false);
        GetUserplaylistFirstPageAdapter.MyViewHolder vh = new GetUserplaylistFirstPageAdapter.MyViewHolder(v);
        return vh;
    }
    @Override
    public void onBindViewHolder(final GetUserplaylistFirstPageAdapter.MyViewHolder holder, final int position)
    {
        final GetPlaylistModel getuserplaylist = getPlaylistModelslist.get(position);
        holder.playlist_name.setText(getuserplaylist.getName());
        holder.playlist_count.setText(String.valueOf(getuserplaylist.getSongPlaylistCount()) + " songs");
        holder.itemView.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                  Intent i=new Intent(context, MusicListingActivity.class);
                  i.putExtra("playlist_id",String.valueOf(getuserplaylist.getId()));
                  i.putExtra("status_click","playlist");
                  context.startActivity(i);
            }
        });
    }
    @Override
    public int getItemCount()
    {
        return getPlaylistModelslist.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder
    {
        TextView artist_name_textview,playlist_count,playlist_name;

        public MyViewHolder(View itemView)
        {
            super(itemView);
            playlist_name = itemView.findViewById(R.id.playlist_name);
            playlist_count=itemView.findViewById(R.id.playlist_count1);
        }
    }
}
