package com.webmobril.nyimboapp.Adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;
import com.webmobril.nyimboapp.Activities.PodcastAlbumActivity;
import com.webmobril.nyimboapp.Models.PodcastFeaturedModel;

import com.webmobril.nyimboapp.R;


import java.util.List;

public class FeaturedPodcastsAdapter extends RecyclerView.Adapter<FeaturedPodcastsAdapter.MyViewHolder>
{
    List<PodcastFeaturedModel> featuredpodcastlist;
    Context context;

    public FeaturedPodcastsAdapter(Context context, List featuredpodcastlist)
    {
        this.context = context;
        this.featuredpodcastlist = featuredpodcastlist;
    }
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_featured_podcast, parent, false);
        MyViewHolder vh = new MyViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position)
    {
        context = holder.itemView.getContext();

        final PodcastFeaturedModel trendingsongitem = featuredpodcastlist.get(position);
        Picasso.get().load(trendingsongitem.getImage()).into(holder.img);
        holder.artist_name.setText(trendingsongitem.getName());

        holder.itemView.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Intent i=new Intent(context, PodcastAlbumActivity.class);
                i.putExtra("album_id",String.valueOf(trendingsongitem.getAlbumId()));
                i.putExtra("category_id",String.valueOf(trendingsongitem.getCategoryId()));
                i.putExtra("image_item",String.valueOf(trendingsongitem.getImage()));
                i.putExtra("description",String.valueOf(trendingsongitem.getDescription()));
                i.putExtra("category_name",String.valueOf(trendingsongitem.getName()));
                i.putExtra("status_click","home");
                context.startActivity(i);
            }
        });
    }

    @Override
    public int getItemCount()
    {
        return featuredpodcastlist.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder
    {
        TextView artist_name;
        ImageView img;
        public MyViewHolder(View itemView)
        {
            super(itemView);
            artist_name= itemView.findViewById(R.id.artist_name);
            img = itemView.findViewById(R.id.singer_image);
        }
    }
}