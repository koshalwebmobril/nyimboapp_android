package com.webmobril.nyimboapp.Adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;
import com.webmobril.nyimboapp.Activities.MusicListingActivity;
import com.webmobril.nyimboapp.Models.AlbumsListModel1;
import com.webmobril.nyimboapp.R;

import java.util.ArrayList;
public class TopAlbumsAdapter extends RecyclerView.Adapter<TopAlbumsAdapter.MyViewHolder>
{
    ArrayList<AlbumsListModel1>topalbumsmodellist;
    Context context;

    public TopAlbumsAdapter(Context context, ArrayList topalbumsmodellist)
    {
        this.context = context;
        this.topalbumsmodellist = topalbumsmodellist;
    }
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_top_albums, parent, false);
        MyViewHolder vh = new MyViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position)
    {
        final AlbumsListModel1 albumsListModel1 = topalbumsmodellist.get(position);

        Picasso.get().load(albumsListModel1.getImage()).into(holder.img);
        holder.artist_name.setText(albumsListModel1.getName());

        holder.itemView.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Intent i=new Intent(context, MusicListingActivity.class);
                i.putExtra("album_id",String.valueOf(albumsListModel1.getId()));
                i.putExtra("artist_image",albumsListModel1.getImage());
                i.putExtra("selected_id","album_id");
                i.putExtra("status_click","home");
                context.startActivity(i);
            }
        });
    }

    @Override
    public int getItemCount()
    {
        return topalbumsmodellist.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder
    {
        TextView artist_name;
        ImageView img;
        public MyViewHolder(View itemView)
        {
            super(itemView);
            artist_name= itemView.findViewById(R.id.artist_name);
            img = itemView.findViewById(R.id.singer_image);
        }
    }
}