package com.webmobril.nyimboapp.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.webmobril.nyimboapp.Models.SubcriptionPlanModel;
import com.webmobril.nyimboapp.R;

import java.util.ArrayList;


public class SubcriptionPlanAdapter extends RecyclerView.Adapter<SubcriptionPlanAdapter.MyViewHolder>
{
    ArrayList<SubcriptionPlanModel> subcriptionplanmodellist;
    Context context;
    public SubcriptionPlanAdapter(Context context, ArrayList subcriptionplanmodellist)
    {
        this.context = context;
        this.subcriptionplanmodellist = subcriptionplanmodellist;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_subcription_plan, parent, false);
        MyViewHolder vh = new MyViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position)
    {
        final SubcriptionPlanModel subcriptionplanmodel = subcriptionplanmodellist.get(position);
        final String title1 =subcriptionplanmodel.getTitle_plan1();
        final String  title2= subcriptionplanmodel.getTitle_text1();
        final String title3 =subcriptionplanmodel.getTitle_plan2();
        final String  title4= subcriptionplanmodel.getTitle_text2();

        holder.free_title.setText(title1);
        holder.ad_breaks_title.setText(title2);
        holder.primium_title.setText(title3);
        holder.ad_free_music.setText(title4);

        holder.itemView.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                /*Intent i=new Intent(context, MainActivity.class);
                context.startActivity(i);*/
            }
        });
    }

    @Override
    public int getItemCount()
    {
        return subcriptionplanmodellist.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder
    {
        TextView free_title,ad_breaks_title,primium_title,ad_free_music;
        public MyViewHolder(View itemView)
        {
            super(itemView);
            free_title= itemView.findViewById(R.id.free_title);
            ad_breaks_title = itemView.findViewById(R.id.ad_breaks_title);
            primium_title= itemView.findViewById(R.id.primium_title);
            ad_free_music = itemView.findViewById(R.id.ad_free_music);
        }
    }
}