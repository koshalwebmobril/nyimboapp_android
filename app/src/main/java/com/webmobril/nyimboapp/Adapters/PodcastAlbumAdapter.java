package com.webmobril.nyimboapp.Adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;
import com.webmobril.nyimboapp.Activities.PlayerPodcastActivity;
import com.webmobril.nyimboapp.Models.PodcastEpisodeItemModel;
import com.webmobril.nyimboapp.R;
import com.webmobril.nyimboapp.Utils.Singleton;

import java.util.ArrayList;
import java.util.List;

public class PodcastAlbumAdapter extends RecyclerView.Adapter<PodcastAlbumAdapter.MyViewHolder>
{
    List<PodcastEpisodeItemModel> podcastalubumlist;
    Context context;
    Singleton singleton=Singleton.getInstance();
    public PodcastAlbumAdapter(Context context, List podcastalubumlist)
    {
        this.context = context;
        this.podcastalubumlist = podcastalubumlist;
    }
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_podcast_albums, parent, false);
        MyViewHolder vh = new MyViewHolder(v);
        return vh;
    }
    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position)
    {
        context = holder.itemView.getContext();
        final PodcastEpisodeItemModel podcastitem = podcastalubumlist.get(position);
        Picasso.get().load(podcastitem.getImage()).into(holder.artist_imageview);
        holder.artist_name.setText(podcastitem.getName());

      /*  String artistName = "";
        if (podcastitem.getArtistSongs().size()==1)
        {
            artistName=podcastitem.getArtistSongs().get(0).getArtistName();
        }
        else
        {
            for (int i = 0; i < podcastitem.getArtistSongs().size(); i++)
            {
                if(i==0){
                    artistName=podcastitem.getArtistSongs().get(0).getArtistName();

                }else {
                    artistName=artistName+","+podcastitem.getArtistSongs().get(i).getArtistName();
                }
            }
        }
   //     holder.artist_name_textview.setText(artistName);
        String finalArtistName = artistName;*/
        holder.itemView.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Intent i = new Intent(context, PlayerPodcastActivity.class);
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                i.putExtra("song_url", podcastitem.getUploadSong());
                i.putExtra("song_name", podcastitem.getName());
                i.putExtra("singer_image", podcastitem.getImage());
                i.putExtra("artistName", "Neha");
               /* i.putExtra("position", position);
                i.putExtra("song_id", String.valueOf(podcastitem.getId()));*/
                singleton.podcastEpisodeItemModels = (ArrayList<PodcastEpisodeItemModel>) podcastalubumlist;
               // i.putExtra("like_dislike_songs_count", String.valueOf(podcastitem.getLikeDislikeSongsCount()));
                context.startActivity(i);
            }
        });
    }

    @Override
    public int getItemCount()
    {
        return podcastalubumlist.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder
    {
        TextView artist_name;
        ImageView artist_imageview;
        public MyViewHolder(View itemView)
        {
            super(itemView);
            artist_name= itemView.findViewById(R.id.artist_name);
            artist_imageview = itemView.findViewById(R.id.artist_imageview);
        }
    }
}