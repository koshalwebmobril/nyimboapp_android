package com.webmobril.nyimboapp.Adapters;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;
import com.webmobril.nyimboapp.Interface.PlaylistClick;
import com.webmobril.nyimboapp.Models.GetPlaylistModel;
import com.webmobril.nyimboapp.R;

import java.util.List;
public class userplaylistadapterAdapter extends RecyclerView.Adapter<userplaylistadapterAdapter.MyViewHolder>
{
    List<GetPlaylistModel> getPlaylistModelslist;
    PlaylistClick playlistClick;
    Context context;
    String artist_image;

    public userplaylistadapterAdapter(Context context, List getPlaylistModelslist,PlaylistClick playlistClick,String artist_image)
    {
        this.context = context;
        this.getPlaylistModelslist= getPlaylistModelslist;
        this.playlistClick=playlistClick;
        this.artist_image=artist_image;
    }
    @Override
    public userplaylistadapterAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_user_playlist, parent, false);
        userplaylistadapterAdapter.MyViewHolder vh = new userplaylistadapterAdapter.MyViewHolder(v);
        return vh;
    }
    @Override
    public void onBindViewHolder(final userplaylistadapterAdapter.MyViewHolder holder, final int position)
    {
        final GetPlaylistModel getuserplaylist = getPlaylistModelslist.get(position);

          Picasso.get().load(artist_image).into(holder.artist_imageview);
          holder.playlist_name.setText(getuserplaylist.getName());

        holder.itemView.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                playlistClick.playlistClick(String.valueOf(getuserplaylist.getId()));
            }
        });
    }
    @Override
    public int getItemCount()
    {
        return getPlaylistModelslist.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder
    {
        TextView playlist_name;
        ImageView artist_imageview;
        TextView artist_name_textview;
        public MyViewHolder(View itemView)
        {
            super(itemView);
            playlist_name = itemView.findViewById(R.id.playlist_name);
            artist_imageview=itemView.findViewById(R.id.artist_imageview);
        }
    }
}
