package com.webmobril.nyimboapp.Adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;
import com.webmobril.nyimboapp.Activities.MusicListingActivity;
import com.webmobril.nyimboapp.Activities.PlayerActivityAllMusicListing;
import com.webmobril.nyimboapp.Models.GenreListModel1;
import com.webmobril.nyimboapp.R;

import java.util.ArrayList;


public class ParticularAdapter extends RecyclerView.Adapter<ParticularAdapter.MyViewHolder>
{
    ArrayList<GenreListModel1> particularArrayList;
    Context context;
    public ParticularAdapter(Context context, ArrayList particularArrayList)
    {
        this.context = context;
        this.particularArrayList = particularArrayList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_particular_song, parent, false);
        MyViewHolder vh = new MyViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position)
    {
       GenreListModel1 genrelistmodel = particularArrayList.get(position);
       Picasso.get().load(genrelistmodel.getImage()).into(holder.img);
       holder.itemView.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Intent i=new Intent(context, MusicListingActivity.class);
                i.putExtra("selected_id","category_id");
                i.putExtra("artist_image",genrelistmodel.getImage());
                i.putExtra("album_id",String.valueOf(genrelistmodel.getId()));
                i.putExtra("status_click","home");
                context.startActivity(i);
            }
        });
    }

    @Override
    public int getItemCount()
    {
        return particularArrayList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder
    {
        ImageView img;
        public MyViewHolder(View itemView)
        {
            super(itemView);
            img = itemView.findViewById(R.id.singer_image);
        }
    }
}