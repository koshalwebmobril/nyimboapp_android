package com.webmobril.nyimboapp.Adapters;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;
import com.webmobril.nyimboapp.Activities.PlayerActivityAllMusicListing;
import com.webmobril.nyimboapp.Interface.OnClick;
import com.webmobril.nyimboapp.Interface.OnClickLikeDislike;
import com.webmobril.nyimboapp.Models.GetAllSongListModel;
import com.webmobril.nyimboapp.R;
import com.webmobril.nyimboapp.Utils.Singleton;

import java.util.ArrayList;

public class MusicListingAdapter extends RecyclerView.Adapter<MusicListingAdapter.MyViewHolder> {
    ArrayList<GetAllSongListModel> audiomodellist;
    Context context;
    ImageView menu_list;
    OnClick onclick;
    OnClickLikeDislike onClickLikeDislike;
    Singleton singleton=Singleton.getInstance();
    public MusicListingAdapter(Context context, ArrayList audiomodellist, OnClick onclick,OnClickLikeDislike onClickLikeDislike)
    {
        this.context = context;
        this.audiomodellist = audiomodellist;
        this.onclick = onclick;
        this.onClickLikeDislike=onClickLikeDislike;
    }


    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_music_listing, parent, false);
        MyViewHolder vh = new MyViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position)
    {
        final GetAllSongListModel getalbumsonglistmodel = audiomodellist.get(position);
        Picasso.get().load(getalbumsonglistmodel.getImage()).into(holder.artist_imageview);
        holder.song_name_textview.setText(getalbumsonglistmodel.getName());
        holder.duration.setText(getalbumsonglistmodel.getDuration());

        String artistName = "";
        if (getalbumsonglistmodel.getArtistSongs().size()==1)
        {
            artistName=getalbumsonglistmodel.getArtistSongs().get(0).getArtistName();
        }
        else
            {
            for (int i = 0; i < getalbumsonglistmodel.getArtistSongs().size(); i++)
            {
                if(i==0){
                    artistName=getalbumsonglistmodel.getArtistSongs().get(0).getArtistName();

                }else {
                    artistName=artistName+","+getalbumsonglistmodel.getArtistSongs().get(i).getArtistName();
                }
            }
        }
        holder.artist_name_textview.setText(artistName);
        String finalArtistName = artistName;

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(context, PlayerActivityAllMusicListing.class);
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                i.putExtra("song_url", getalbumsonglistmodel.getUploadSong());
                i.putExtra("song_name", getalbumsonglistmodel.getName());
                i.putExtra("singer_image", getalbumsonglistmodel.getImage());
                i.putExtra("position", position);
                i.putExtra("artistName", finalArtistName);
                i.putExtra("song_id", String.valueOf(getalbumsonglistmodel.getId()));
                singleton.audiomodellist=audiomodellist;
                i.putExtra("like_dislike_songs_count", String.valueOf(getalbumsonglistmodel.getLikeDislikeSongsCount()));
                context.startActivity(i);
            }
        });

        menu_list.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                PopupMenu popup = new PopupMenu(context, v, Gravity.END);
                popup.inflate(R.menu.poupup_menu);
                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener()
                {
                    @Override
                    public boolean onMenuItemClick(MenuItem item)
                    {
                        switch (item.getItemId())
                        {
                            case R.id.like:
                                Log.e("like_dislike","id: "+getalbumsonglistmodel.getId()+" position:" + position);
                                onClickLikeDislike.likedislike(String.valueOf(getalbumsonglistmodel.getId()),position);
                                break;

                            case R.id.add_playlist:
                                onclick.addplaylist(String.valueOf(getalbumsonglistmodel.getId()));
                                break;
                        }
                        return false;
                    }
                });
                //displaying the popup
                popup.show();
            }
        });
    }

    @Override
    public int getItemCount() {
        return audiomodellist.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView song_name_textview, duration, artist_name;
        ImageView artist_imageview;
        TextView artist_name_textview;

        public MyViewHolder(View itemView) {
            super(itemView);
            song_name_textview = itemView.findViewById(R.id.song_name);
            artist_name_textview = itemView.findViewById(R.id.artist_name);
            artist_imageview = itemView.findViewById(R.id.artist_imageview);
            duration = itemView.findViewById(R.id.duration);

            menu_list = itemView.findViewById(R.id.menu_list);
        }
    }
}