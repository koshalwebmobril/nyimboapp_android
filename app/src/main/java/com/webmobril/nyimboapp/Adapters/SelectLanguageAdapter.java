package com.webmobril.nyimboapp.Adapters;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;


import com.webmobril.nyimboapp.Models.SelectLanguageModel1;
import com.webmobril.nyimboapp.R;

import java.util.List;

public class SelectLanguageAdapter extends RecyclerView.Adapter<SelectLanguageAdapter.MyViewHolder>
{
    List<SelectLanguageModel1> selectlanguagelist;
    Context context;
    public ItemCheckListner itemCheckListner;

    public SelectLanguageAdapter(Context context, List<SelectLanguageModel1> selectlanguagelist)
    {
        this.context = context;
        this.selectlanguagelist = selectlanguagelist;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_selectlanguage, parent, false);
        MyViewHolder vh = new MyViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position)
    {
        context = holder.itemView.getContext();
         SelectLanguageModel1 selectlangaugeItem = selectlanguagelist.get(position);
        final String name = selectlangaugeItem.getLanguage();
        final String color_code = selectlangaugeItem.getColor();

        holder.langauge_name.setText(name);
        holder.relative_langauge.setBackgroundResource(R.drawable.round_button_langauge);
        GradientDrawable drawable = (GradientDrawable) holder.relative_langauge.getBackground();
        drawable.setColor(Color.parseColor(color_code));

        holder.itemView.setOnClickListener(view ->
        {
            selectlangaugeItem.setSelected(!selectlangaugeItem.isSelected());
            itemCheckListner.setCheckedList(String.valueOf(selectlangaugeItem.getId()));
            notifyDataSetChanged();
        });


        if(selectlangaugeItem.isSelected())
        {
            itemCheckListner.remove(selectlangaugeItem);
        }
        if (selectlangaugeItem.isSelected())
        {
            holder.checked.setVisibility(View.VISIBLE);
        }
        else
        {
            holder.checked.setVisibility(View.GONE);
        }
    }
    public interface ItemCheckListner
     {
        void setCheckedList(String list);
         void remove(SelectLanguageModel1 selectlangaugeItem);
     }


     public void setCheckedItemListner(ItemCheckListner itemCheckListner)
     {
         this.itemCheckListner=itemCheckListner;
     }

     @Override
    public int getItemCount()
    {
        return selectlanguagelist.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder
    {
        TextView langauge_name;
        RelativeLayout relative_langauge;
        ImageView checked;


        public MyViewHolder(View itemView)
        {
            super(itemView);
            langauge_name = itemView.findViewById(R.id.langauge_name);
            relative_langauge = itemView.findViewById(R.id.relative_artist);
            checked=itemView.findViewById(R.id.checked);
        }
    }
}