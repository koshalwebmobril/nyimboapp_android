package com.webmobril.nyimboapp.Adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;
import com.webmobril.nyimboapp.Activities.MusicListingActivity;
import com.webmobril.nyimboapp.Activities.PlayerActivityAllMusicListing;

import com.webmobril.nyimboapp.Models.ArtistsListModel1;
import com.webmobril.nyimboapp.R;

import java.util.List;

public class ArtistAdapter extends RecyclerView.Adapter<ArtistAdapter.MyViewHolder>
{
    List<ArtistsListModel1> artistlist;
    Context context;

    public ArtistAdapter(Context context, List artistlist)
    {
        this.context = context;
        this.artistlist = artistlist;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_artist, parent, false);
        MyViewHolder vh = new MyViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position)
    {
        final ArtistsListModel1 artistmodel = artistlist.get(position);

        Picasso.get().load(artistmodel.getImage()).into(holder.artist_imageview);
        holder.artist_name.setText(artistmodel.getName());

        holder.itemView.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Intent i=new Intent(context, MusicListingActivity.class);
                i.putExtra("selected_id","artist_id");
                i.putExtra("artist_image",artistmodel.getImage());
                i.putExtra("album_id",String.valueOf(artistmodel.getArtistId()));
                i.putExtra("status_click","home");
                context.startActivity(i);
            }
        });
    }

    @Override
    public int getItemCount()
    {
        return artistlist.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder
    {
        TextView artist_name;
        ImageView artist_imageview;
        public MyViewHolder(View itemView)
        {
            super(itemView);
            artist_name= itemView.findViewById(R.id.artist_name);
            artist_imageview = itemView.findViewById(R.id.artist_imageview);
        }
    }
}