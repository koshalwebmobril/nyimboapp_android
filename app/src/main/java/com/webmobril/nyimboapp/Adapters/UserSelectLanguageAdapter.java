package com.webmobril.nyimboapp.Adapters;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.webmobril.nyimboapp.Activities.UserSelectLanguageActivity;
import com.webmobril.nyimboapp.Models.SelectLanguageModel1;
import com.webmobril.nyimboapp.Models.UserSelectLanguageModel1;
import com.webmobril.nyimboapp.R;
import com.webmobril.nyimboapp.Sharedpreference.LoginPreferences;

import java.util.List;

public class UserSelectLanguageAdapter extends RecyclerView.Adapter<UserSelectLanguageAdapter.MyViewHolder> {
    List<UserSelectLanguageModel1> selectlanguagelist;
    Context context;
    public ItemCheckListner itemCheckListner;
    String user_selected_language_id;

    public UserSelectLanguageAdapter(Context context, List<UserSelectLanguageModel1> selectlanguagelist) {
        this.context = context;
        this.selectlanguagelist = selectlanguagelist;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_selectlanguage, parent, false);
        MyViewHolder vh = new MyViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        context = holder.itemView.getContext();
        UserSelectLanguageModel1 selectlangaugeItem = selectlanguagelist.get(position);

        final String name = selectlangaugeItem.getLanguage();
        final String color_code = selectlangaugeItem.getColor();
        String getid = String.valueOf(selectlangaugeItem.getId());
        String getstatus_selected = String.valueOf(selectlangaugeItem.getStatus());

        holder.langauge_name.setText(name);
        holder.relative_langauge.setBackgroundResource(R.drawable.round_button_langauge);
        GradientDrawable drawable = (GradientDrawable) holder.relative_langauge.getBackground();
        drawable.setColor(Color.parseColor(color_code));

      /*  user_selected_language_id= LoginPreferences.getActiveInstance(context).getSelectedLanguage();
        Log.d("LANGAUGE",user_selected_language_id);*/


        if (getstatus_selected.equals("0")) {
            holder.checked.setVisibility(View.VISIBLE);
        } else {
            holder.checked.setVisibility(View.GONE);
        }

        holder.itemView.setOnClickListener(view ->
        {
           /*selectlangaugeItem.setSelected(!selectlangaugeItem.isSelected());
            itemCheckListner.setCheckedList(String.valueOf(selectlangaugeItem.getId()));*/
           /*  if (selectlangaugeItem.isSelected()) {
                itemCheckListner.remove(selectlangaugeItem);
                holder.checked.setVisibility(View.VISIBLE);
            } else {
                holder.checked.setVisibility(View.GONE);
            }*/

            if (getstatus_selected.equals("0")) {
                holder.checked.setVisibility(View.GONE);
                itemCheckListner.onItemSelect(position,1);

               /* selectlangaugeItem.setStatus(1);
                selectlanguagelist.set(position,selectlangaugeItem);*/

            } else {
                holder.checked.setVisibility(View.VISIBLE);
                itemCheckListner.onItemSelect(position,0);

               /* selectlangaugeItem.setStatus(0);
                selectlanguagelist.set(position,selectlangaugeItem);*/
            }
            //notifyDataSetChanged();
        });


    }

    public interface ItemCheckListner {
        void setCheckedList(String list);

        void remove(UserSelectLanguageModel1 selectlangaugeItem);

        void onItemSelect(int position, int i);
    }


    public void setCheckedItemListner(ItemCheckListner itemCheckListner) {
        this.itemCheckListner = itemCheckListner;
    }

    @Override
    public int getItemCount() {
        return selectlanguagelist.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView langauge_name;
        RelativeLayout relative_langauge;
        ImageView checked, checked_afterselect;
        String user_selected_language_id;

        public MyViewHolder(View itemView) {
            super(itemView);
            langauge_name = itemView.findViewById(R.id.langauge_name);
            relative_langauge = itemView.findViewById(R.id.relative_artist);
            checked = itemView.findViewById(R.id.checked);
            checked_afterselect = itemView.findViewById(R.id.checked_afterselect);
        }
    }
}