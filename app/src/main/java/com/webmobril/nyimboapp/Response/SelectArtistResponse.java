package com.webmobril.nyimboapp.Response;

import java.util.List;
import com.google.gson.annotations.SerializedName;
import com.webmobril.nyimboapp.Models.GetArtistResponseModel;

public class SelectArtistResponse {
	@SerializedName("result")
	private List<GetArtistResponseModel> result;

	@SerializedName("status")
	private String status;

	public List<GetArtistResponseModel> getResult(){
		return result;
	}

	public String getStatus()
	{
		return status;
	}
}