package com.webmobril.nyimboapp.Response;

import com.google.gson.annotations.SerializedName;

public class GetUserArtistResponse{

	@SerializedName("result")
	private getGetUserArtistListResponse getGetUserArtistListResponse;

	@SerializedName("status")
	private String status;

	public getGetUserArtistListResponse getGetGetUserArtistListResponse(){
		return getGetUserArtistListResponse;
	}

	public String getStatus(){
		return status;
	}
}