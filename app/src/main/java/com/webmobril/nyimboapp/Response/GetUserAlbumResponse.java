package com.webmobril.nyimboapp.Response;

import java.util.List;
import com.google.gson.annotations.SerializedName;
import com.webmobril.nyimboapp.Models.GetUserAlbumModel;

public class GetUserAlbumResponse{

	@SerializedName("result")
	private List<GetUserAlbumModel> result;

	@SerializedName("status")
	private String status;

	public List<GetUserAlbumModel> getResult(){
		return result;
	}

	public String getStatus(){
		return status;
	}
}