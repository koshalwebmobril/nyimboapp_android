package com.webmobril.nyimboapp.Response;

import com.google.gson.annotations.SerializedName;
import com.webmobril.nyimboapp.Models.CreatePlaylistModel;

public class CreatePlaylistResponse{

	@SerializedName("result")
	private CreatePlaylistModel createPlaylistModel;

	@SerializedName("status")
	private String status;

	public CreatePlaylistModel getCreatePlaylistModel(){
		return createPlaylistModel;
	}

	public String getStatus(){
		return status;
	}
}