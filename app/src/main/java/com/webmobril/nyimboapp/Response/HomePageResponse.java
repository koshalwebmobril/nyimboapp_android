package com.webmobril.nyimboapp.Response;

import com.google.gson.annotations.SerializedName;

public class HomePageResponse{

	@SerializedName("result")
	private HomePageResultResponse homePageResultResponse;

	@SerializedName("status")
	private String status;

	public HomePageResultResponse getHomePageResultResponse(){
		return homePageResultResponse;
	}

	public String getStatus(){
		return status;
	}
}