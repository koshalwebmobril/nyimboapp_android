package com.webmobril.nyimboapp.Response;

import java.util.List;
import com.google.gson.annotations.SerializedName;
import com.webmobril.nyimboapp.Models.GetAllSongListModel;

public class GetAllSongListResponse {

	@SerializedName("result")
	private List<GetAllSongListModel> result;

	@SerializedName("status")
	private String status;

	public List<GetAllSongListModel> getResult(){
		return result;
	}

	public String getStatus(){
		return status;
	}
}