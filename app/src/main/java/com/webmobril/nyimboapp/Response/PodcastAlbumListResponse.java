package com.webmobril.nyimboapp.Response;

import java.util.List;
import com.google.gson.annotations.SerializedName;
import com.webmobril.nyimboapp.Models.PodcastEpisodeItemModel;
import com.webmobril.nyimboapp.Models.TopSeriesItemModel;

public class PodcastAlbumListResponse {

	@SerializedName("podcastEpisode")
	private List<PodcastEpisodeItemModel> podcastEpisode;

	@SerializedName("topSeries")
	private List<TopSeriesItemModel> topSeries;

	public List<PodcastEpisodeItemModel> getPodcastEpisode(){
		return podcastEpisode;
	}

	public List<TopSeriesItemModel> getTopSeries(){
		return topSeries;
	}
}