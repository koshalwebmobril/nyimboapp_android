package com.webmobril.nyimboapp.Response;

import com.google.gson.annotations.SerializedName;

public class PodcastAlbumResponse {

	@SerializedName("result")
	private PodcastAlbumListResponse podcastAlbumListResponse;

	@SerializedName("status")
	private String status;

	public PodcastAlbumListResponse getPodcastAlbumListResponse(){
		return podcastAlbumListResponse;
	}
	 public String getStatus(){
		return status;
	}
}