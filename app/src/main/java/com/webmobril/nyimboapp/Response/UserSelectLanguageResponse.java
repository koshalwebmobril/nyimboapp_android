package com.webmobril.nyimboapp.Response;

import com.google.gson.annotations.SerializedName;
import com.webmobril.nyimboapp.Models.SelectLanguageModel1;
import com.webmobril.nyimboapp.Models.UserSelectLanguageModel1;

import java.util.List;

public class UserSelectLanguageResponse {
	@SerializedName("result")
	private List<UserSelectLanguageModel1> selectLanguageModel1List;

	@SerializedName("status")
	private String status;

	public List<UserSelectLanguageModel1> getSelectLanguageModel1List() {
		return selectLanguageModel1List;
	}

	public String getStatus() {
		return status;
	}
}