package com.webmobril.nyimboapp.Response;

import java.util.List;
import com.google.gson.annotations.SerializedName;
import com.webmobril.nyimboapp.Models.GetUserArtistModel;

public class getGetUserArtistListResponse {

	@SerializedName("artists")
	private List<GetUserArtistModel> artists;

	public List<GetUserArtistModel> getArtists(){
		return artists;
	}
}