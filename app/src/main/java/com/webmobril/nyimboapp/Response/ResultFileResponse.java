package com.webmobril.nyimboapp.Response;

import com.google.gson.annotations.SerializedName;
import com.webmobril.nyimboapp.Models.AuthenticationModel;
public class ResultFileResponse
{
       @SerializedName("status")
        String  status;

        @SerializedName("message")
        String message;

       @SerializedName("token")
         String token;
       @SerializedName("result")
        AuthenticationModel authenticationModel;



    public String getStatus() {
        return status;
    }
    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() { return message; }
    public void setMessage(String message) {
        this.message = message;
    }

        public void setToken(String token) {
        this.token = token;
    }
        public String getToken() {
        return token;
    }

    public AuthenticationModel getAuthenticationModel() {
        return authenticationModel;
    }
    public void setAuthenticationModel(AuthenticationModel authenticationModel)
    {
        this.authenticationModel = authenticationModel;
    }
}
