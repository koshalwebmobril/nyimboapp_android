package com.webmobril.nyimboapp.Response;

import java.util.List;
import com.google.gson.annotations.SerializedName;
import com.webmobril.nyimboapp.Models.AlbumsListModel1;
import com.webmobril.nyimboapp.Models.ArtistsListModel1;
import com.webmobril.nyimboapp.Models.GenreListModel1;
import com.webmobril.nyimboapp.Models.TrandingSongsModel;

public class HomePageResultResponse
{
	@SerializedName("albums")
	private List<AlbumsListModel1> albums;

	@SerializedName("artists")
	private List<ArtistsListModel1> artists;

	@SerializedName("genre")
	private List<GenreListModel1> genre;

	@SerializedName("trandingSongs")
	private List<TrandingSongsModel> trandingSongs;

	public List<AlbumsListModel1> getAlbums(){
		return albums;
	}

	public List<ArtistsListModel1> getArtists(){
		return artists;
	}

	public List<GenreListModel1> getGenre(){
		return genre;
	}

	public List<TrandingSongsModel> getTrandingSongs(){
		return trandingSongs;
	}
}