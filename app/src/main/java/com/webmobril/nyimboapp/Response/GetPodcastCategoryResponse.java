package com.webmobril.nyimboapp.Response;

import java.util.List;
import com.google.gson.annotations.SerializedName;
import com.webmobril.nyimboapp.Models.PodcastCategoryModel;

public class GetPodcastCategoryResponse {

	@SerializedName("result")
	private List<PodcastCategoryModel> result;

	@SerializedName("status")
	private String status;

	public List<PodcastCategoryModel> getResult(){
		return result;
	}

	public String getStatus(){
		return status;
	}
}