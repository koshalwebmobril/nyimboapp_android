package com.webmobril.nyimboapp.Response;

import com.google.gson.annotations.SerializedName;

public class SetArtistResponse{

	@SerializedName("status")
	private String status;

	public String getStatus(){
		return status;
	}
}