package com.webmobril.nyimboapp.Response;

import java.util.List;
import com.google.gson.annotations.SerializedName;
import com.webmobril.nyimboapp.Models.GetPlaylistModel;

public class getPlayListResponselist {

	@SerializedName("likedSongsCount")
	private int likedSongsCount;

	@SerializedName("getPlaylists")
	private List<GetPlaylistModel> getPlaylists;

	@SerializedName("downloadedSongsCount")
	private List<Object> downloadedSongsCount;

	public int getLikedSongsCount(){
		return likedSongsCount;
	}

	public List<GetPlaylistModel> getGetPlaylists(){
		return getPlaylists;
	}

	public List<Object> getDownloadedSongsCount(){
		return downloadedSongsCount;
	}
}