package com.webmobril.nyimboapp.Response;

import com.google.gson.annotations.SerializedName;

public class GetPlaylistDownloadListResponse
{

	@SerializedName("result")
	private getPlayListResponselist getPlayListResponselist;

	@SerializedName("message")
	private String message;

	@SerializedName("status")
	private String status;

	public getPlayListResponselist getGetPlayListResponselist(){
		return getPlayListResponselist;
	}

	public String getMessage(){
		return message;
	}

	public String getStatus(){
		return status;
	}
}