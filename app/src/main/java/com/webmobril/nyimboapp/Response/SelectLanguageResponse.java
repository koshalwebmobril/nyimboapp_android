package com.webmobril.nyimboapp.Response;

import java.util.List;
import com.google.gson.annotations.SerializedName;
import com.webmobril.nyimboapp.Models.SelectLanguageModel1;

public class SelectLanguageResponse{
	@SerializedName("result")
	private List<SelectLanguageModel1> selectLanguageModel1List;

	@SerializedName("status")
	private String status;

	public List<SelectLanguageModel1> getSelectLanguageModel1List() {
		return selectLanguageModel1List;
	}

	public String getStatus() {
		return status;
	}
}