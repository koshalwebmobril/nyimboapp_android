package com.webmobril.nyimboapp.Response;

import com.google.gson.annotations.SerializedName;
import com.webmobril.nyimboapp.Models.GetProfileModel;

public class GetProfileApiResponse{
	@SerializedName("result")
	private GetProfileModel getProfileModel;

	@SerializedName("message")
	private String message;

	@SerializedName("status")
	private String status;

	public GetProfileModel getGetProfileModel(){
		return getProfileModel;
	}

	public String getMessage(){
		return message;
	}

	public String getStatus(){
		return status;
	}
}