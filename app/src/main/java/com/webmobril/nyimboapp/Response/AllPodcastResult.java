package com.webmobril.nyimboapp.Response;

import java.util.List;
import com.google.gson.annotations.SerializedName;
import com.webmobril.nyimboapp.Models.PodcastFeaturedModel;
import com.webmobril.nyimboapp.Models.PopularPodcastsModel1;
import com.webmobril.nyimboapp.Models.TopSeriesModel;

public class AllPodcastResult {

	@SerializedName("popularPodcasts")
	private List<PopularPodcastsModel1> popularPodcasts;

	@SerializedName("podcastCategories")
	private List<PodcastFeaturedModel> podcastCategories;

	@SerializedName("topSeries")
	private List<TopSeriesModel> topSeries;

	public List<PopularPodcastsModel1> getPopularPodcasts(){
		return popularPodcasts;
	}

	public List<PodcastFeaturedModel> getPodcastCategories(){
		return podcastCategories;
	}

	public List<TopSeriesModel> getTopSeries(){
		return topSeries;
	}
}