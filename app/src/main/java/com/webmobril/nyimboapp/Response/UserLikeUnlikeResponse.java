package com.webmobril.nyimboapp.Response;

import com.google.gson.annotations.SerializedName;
import com.webmobril.nyimboapp.Models.LikeUnlikeModel;

public class UserLikeUnlikeResponse {

	@SerializedName("status")
	private String status;

	public String getStatus(){
		return status;
	}


	@SerializedName("message")
	private String message;

	public String getMessage(){
		return message;
	}
}