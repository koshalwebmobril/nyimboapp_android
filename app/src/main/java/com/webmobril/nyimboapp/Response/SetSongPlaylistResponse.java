package com.webmobril.nyimboapp.Response;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class SetSongPlaylistResponse{

	@SerializedName("result")
	private List<Object> result;

	@SerializedName("message")
	private String message;

	@SerializedName("status")
	private String status;

	public List<Object> getResult(){
		return result;
	}

	public String getMessage(){
		return message;
	}

	public String getStatus(){
		return status;
	}
}