package com.webmobril.nyimboapp.Response;

import com.google.gson.annotations.SerializedName;
import com.webmobril.nyimboapp.Models.GetPlaylistModel;

import java.util.List;

public class GetPlayListlistsResponse {

	@SerializedName("likedSongsCount")
	private int likedSongsCount;

	@SerializedName("getPlaylists")
	private List<GetPlaylistModel> getPlaylists;

	public int getLikedSongsCount(){
		return likedSongsCount;
	}

	public List<GetPlaylistModel> getGetPlaylists(){
		return getPlaylists;
	}
}