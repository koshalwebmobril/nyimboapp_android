package com.webmobril.nyimboapp.Response;

import com.google.gson.annotations.SerializedName;

public class GetPodcastAllDataResponse {

	@SerializedName("result")
	private AllPodcastResult allPodcastResult;

	@SerializedName("status")
	private String status;

	public AllPodcastResult getAllPodcastResult(){
		return allPodcastResult;
	}

	public String getStatus(){
		return status;
	}
}