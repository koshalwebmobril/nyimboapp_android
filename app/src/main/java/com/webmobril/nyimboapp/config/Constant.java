package com.webmobril.nyimboapp.config;

public interface Constant {
    String PREFS_NAME = "music_data";
    String TAG_TOKEN = "firebase_token";
    String FRAGMENT_TYPE = "fragment_type";
}
