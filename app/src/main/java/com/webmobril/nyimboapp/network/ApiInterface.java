package com.webmobril.nyimboapp.network;



import com.webmobril.nyimboapp.Models.LanguageUpdateResponse;
import com.webmobril.nyimboapp.Response.CreatePlaylistResponse;
import com.webmobril.nyimboapp.Response.GetAllSongListResponse;
import com.webmobril.nyimboapp.Response.GetPlaylistDownloadListResponse;
import com.webmobril.nyimboapp.Response.GetPodcastAllDataResponse;
import com.webmobril.nyimboapp.Response.GetPodcastCategoryResponse;
import com.webmobril.nyimboapp.Response.GetProfileApiResponse;
import com.webmobril.nyimboapp.Response.GetUserAlbumResponse;
import com.webmobril.nyimboapp.Response.GetUserArtistResponse;
import com.webmobril.nyimboapp.Response.HomePageResponse;

import com.webmobril.nyimboapp.Response.PodcastAlbumResponse;
import com.webmobril.nyimboapp.Response.SelectArtistResponse;
import com.webmobril.nyimboapp.Response.SelectLanguageResponse;
import com.webmobril.nyimboapp.Response.ResultFileResponse;
import com.webmobril.nyimboapp.Response.SetArtistResponse;
import com.webmobril.nyimboapp.Response.SetSongPlaylistResponse;
import com.webmobril.nyimboapp.Response.UserSelectLanguageResponse;
import com.webmobril.nyimboapp.Response.UserLikeUnlikeResponse;
import com.webmobril.nyimboapp.Utils.UrlApi;

import java.util.Map;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;

public interface ApiInterface
{
    @FormUrlEncoded
    @POST(UrlApi.LOGIN)
    Call<ResultFileResponse> loginUser(@Field("username") String username, @Field("password") String password,
                                       @Field("device_type") String deviceType, @Field("device_token") String deviceToken);


    @FormUrlEncoded
    @POST(UrlApi.SOCIALLOGIN)
    Call<ResultFileResponse> loginUserSocial(@Field("fb_id") String fbid, @Field("username") String username, @Field("email") String email,
                                             @Field("social_type") String socialtype, @Field("device_type") String deviceType,
                                             @Field("device_token") String deviceToken);

    @FormUrlEncoded
    @POST(UrlApi.REGISTER)
    Call<ResultFileResponse> RegisterUser(@Field("username") String username,
                                          @Field("email") String email,
                                          @Field("password") String password,
                                          @Field("device_type") String deviceType,
                                          @Field("device_token") String deviceToken,
                                          @Field("dob") String dob,
                                          @Field("gender") String gender);


    @FormUrlEncoded
    @POST(UrlApi.FORGOTPASSWORD)
    Call<ResultFileResponse> forgotpassword(@Field("mobile_or_email") String email);

    @FormUrlEncoded
    @POST(UrlApi.VERIFYOTP)
    Call<ResultFileResponse> Verifiyotp(@Field("email") String email, @Field("otp") String otp, @Field("from_forgotpassword_screen") String from_forgotpassword_screen);

    @FormUrlEncoded
    @POST(UrlApi.RESETPASSWORD)
    Call<ResultFileResponse> ResetPassword(@Field("new_password") String new_password, @Field("confrim_new_password") String confirm_password, @Field("email") String email);



    @GET(UrlApi.GETLANGUAGE)
    Call<SelectLanguageResponse> GetLanguage();

    @GET(UrlApi.PODCASTCATEGORYAPI)
    Call<GetPodcastCategoryResponse> podcastcategory();


    @FormUrlEncoded
    @POST(UrlApi.GETUSERLANGUAGE)
    Call<UserSelectLanguageResponse> GetUserLanguage(@Field("user_id") String user_id);


    @FormUrlEncoded
    @POST(UrlApi.GETARTIST)
    Call<SelectArtistResponse> GetArtist(@Field("language_id") String language_id,@Field("user_id") String user_id);


    @FormUrlEncoded
    @POST(UrlApi.SETARTIST)
    Call<SetArtistResponse> SetArtist(@Field("artist_id") String artist_id, @Field("user_id") String user_id);


    @FormUrlEncoded
    @POST(UrlApi.UPDATELANGUAGEAPI)
    Call<LanguageUpdateResponse> updateLanguage(@Field("language_id") String language_id, @Field("user_id") String user_id);


    @FormUrlEncoded
    @POST(UrlApi.HomeApi)
    Call<HomePageResponse> homePage(@Field("user_id") String user_id);

    @FormUrlEncoded
    @POST(UrlApi.GETPROFILERESPONSE)
    Call<GetProfileApiResponse> getProfile(@Field("user_id") String user_id);

    @FormUrlEncoded
    @POST(UrlApi.GETALBUMSONGLIST)
    Call<GetAllSongListResponse> getAlbumSonglist(@FieldMap Map<String, String> params);

    @FormUrlEncoded
    @POST(UrlApi.GETUSERPLAYLISTSonglist)
    Call<GetAllSongListResponse> getuserplaylistsonglist(@FieldMap Map<String, String> params);

    @FormUrlEncoded
    @POST(UrlApi.MUSICLISTLIKEDSONG)
    Call<GetAllSongListResponse> likedsonglist(@FieldMap Map<String, String> params);

    @FormUrlEncoded
    @POST(UrlApi.GETUSERARTIST)
    Call<GetUserArtistResponse> getuserartist(@FieldMap Map<String, String> params);

    @FormUrlEncoded
    @POST(UrlApi.GETUSERALBUM)
    Call<GetUserAlbumResponse> getuseralbum(@Field("user_id") String user_id);

    @FormUrlEncoded
    @POST(UrlApi.USERLIKEDDISLIKEDSONG)
    Call<UserLikeUnlikeResponse> likeunlike(@Field("user_id") String user_id, @Field("song_id") String song_id, @Field("status") String statuslikeunlike);

    @FormUrlEncoded
    @POST(UrlApi.CREATEPLAYLISTAPI)
    Call<CreatePlaylistResponse> createPlaylist(@Field("user_id") String user_id, @Field("name") String name);


    @FormUrlEncoded
    @POST(UrlApi.GETPLAYLISTAPI)
    Call<GetPlaylistDownloadListResponse> getplaylist(@Field("user_id") String user_id);


    @FormUrlEncoded
    @POST(UrlApi.SETSONGPLAYLISTAPI)
    Call<SetSongPlaylistResponse> setplaylistsong(@Field("user_id") String user_id,@Field("playlist_id") String playlist_id,@Field("song_id") String song_id,@Field("status") String status);


    @FormUrlEncoded
    @POST(UrlApi.GETPODCASTALLITEMS)
    Call<GetPodcastAllDataResponse> getpodcastallitems(@Field("user_id") String user_id, @Field("category_id") String category_id);

    @FormUrlEncoded
    @POST(UrlApi.GETPODCASTALBUM)
    Call<PodcastAlbumResponse> podcastsubcategory(@Field("user_id") String user_id, @Field("category_id") String category_id, @Field("album_id") String album_id);
}
