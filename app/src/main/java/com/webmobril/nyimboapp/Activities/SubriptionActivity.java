package com.webmobril.nyimboapp.Activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.RelativeLayout;

import com.kingfisher.easyviewindicator.RecyclerViewIndicator;
import com.webmobril.nyimboapp.Adapters.SubcriptionPlanAdapter;
import com.webmobril.nyimboapp.Models.SubcriptionPlanModel;
import com.webmobril.nyimboapp.R;

import java.util.ArrayList;

public class SubriptionActivity extends AppCompatActivity {
    // private ViewPager viewPager;
    private SubcriptionPlanAdapter subcriptionplanadapter;
    RecyclerView recyclerview_plan;
    ArrayList<SubcriptionPlanModel> subcriptionplanlist;
    RelativeLayout trypremium;

    RecyclerViewIndicator recyclerViewIndicator;
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_subription);
        init();
        GetData();
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(this,RecyclerView.HORIZONTAL,false);
        recyclerview_plan.setLayoutManager(mLayoutManager);
        subcriptionplanadapter = new SubcriptionPlanAdapter(this,subcriptionplanlist);
        recyclerview_plan.setAdapter(subcriptionplanadapter);
        recyclerViewIndicator.setRecyclerView(recyclerview_plan);
        recyclerViewIndicator.forceUpdateItemCount();
    }

    public void init()
    {
        recyclerview_plan=findViewById(R.id.recycler_plan);
        recyclerViewIndicator=findViewById(R.id.recyclerViewIndicator);
        trypremium=findViewById(R.id.trypremium);
        subcriptionplanlist = new ArrayList<SubcriptionPlanModel>();

        trypremium.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                Intent i=new Intent(SubriptionActivity.this,CardPaymentActivity.class);
                startActivity(i);
            }
        });

    }

    public void GetData()
    {
        SubcriptionPlanModel restaurentModel1 = new SubcriptionPlanModel("Free","Ad Breaks","Premium","Ad free music");
        subcriptionplanlist.add(restaurentModel1);

        SubcriptionPlanModel restaurentModel2 = new SubcriptionPlanModel("Free","Ad Breaks","Premium","Ad free music");
        subcriptionplanlist.add(restaurentModel2);

       SubcriptionPlanModel restaurentModel3= new SubcriptionPlanModel("Free","Ad Breaks","Premium","Ad free music");
       subcriptionplanlist.add(restaurentModel3);
    }



}