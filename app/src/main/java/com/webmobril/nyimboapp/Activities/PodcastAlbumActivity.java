package com.webmobril.nyimboapp.Activities;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.squareup.picasso.Picasso;
import com.webmobril.nyimboapp.Adapters.FeaturedPodcastsAdapter;
import com.webmobril.nyimboapp.Adapters.PodcastAlbumAdapter;
import com.webmobril.nyimboapp.Adapters.TopSeriesPodcastsAdapter;
import com.webmobril.nyimboapp.Models.PodcastEpisodeItemModel;
import com.webmobril.nyimboapp.Models.PodcastFeaturedModel;
import com.webmobril.nyimboapp.Models.PopularPodcastsModel1;
import com.webmobril.nyimboapp.Models.TopSeriesItemModel;
import com.webmobril.nyimboapp.Models.TopSeriesModel;
import com.webmobril.nyimboapp.R;
import com.webmobril.nyimboapp.Response.PodcastAlbumResponse;
import com.webmobril.nyimboapp.Sharedpreference.LoginPreferences;
import com.webmobril.nyimboapp.Utils.ProgressD;
import com.webmobril.nyimboapp.Utils.UrlApi;
import com.webmobril.nyimboapp.network.ApiInterface;

import java.util.List;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

public class PodcastAlbumActivity extends AppCompatActivity
{
    RecyclerView recyclerview;
    private PodcastAlbumAdapter podcastalbumadapter;
    List<PodcastEpisodeItemModel> podcastepisodeitemlist;

    RecyclerView recyclerview1;
    private TopSeriesPodcastsAdapter featuredPodcastsAdapter;
    List<TopSeriesItemModel> topseriesmodellist;
    ImageView imgback,singer_image;
    String album_id,category_id;
    String image_item;
    TextView podcast_name,podcast_description;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_podcast_album);
        init();
        hitgetpodcastApi();


        imgback.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            { finish(); }
        });
    }

    public void init()
    {
        recyclerview=findViewById(R.id.recyclerview);
      //  recyclerview1=findViewById(R.id.recyclerview1);
        imgback=findViewById(R.id.imgback);
        singer_image=findViewById(R.id.singer_image);
        podcast_name=findViewById(R.id.podcast_name);
        podcast_description=findViewById(R.id.podcast_description);


        album_id=getIntent().getStringExtra("album_id");
        category_id=getIntent().getStringExtra("category_id");
        image_item=getIntent().getStringExtra("image_item");
    }

    private void hitgetpodcastApi()
    {
        final ProgressD progressDialog = ProgressD.show(PodcastAlbumActivity.this,getResources().getString(R.string.logging_in), true, false, null);
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();
        Gson gson = new GsonBuilder()
                .setLenient()
                .create();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(UrlApi.BASE_URL)
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(client)
                .build();
        ApiInterface service = retrofit.create(ApiInterface.class);

        Call<PodcastAlbumResponse> call = service.podcastsubcategory(LoginPreferences.getActiveInstance(PodcastAlbumActivity.this).getUser_id(),category_id,album_id);
        call.enqueue(new Callback<PodcastAlbumResponse>()
        {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onResponse(Call<PodcastAlbumResponse> call, retrofit2.Response<PodcastAlbumResponse> response)
            {
                progressDialog.dismiss();
                try
                {
                    PodcastAlbumResponse resultFile = response.body();
                    if(resultFile.getStatus().equals("true"))
                    {
                        List<PodcastEpisodeItemModel> podcastalubumlist=resultFile.getPodcastAlbumListResponse().getPodcastEpisode();
                     //   List<TopSeriesItemModel> topserieslist=resultFile.getPodcastAlbumListResponse().getTopSeries();


                        podcast_name.setText(getIntent().getStringExtra("category_name"));
                        podcast_description.setText(getIntent().getStringExtra("description"));
                        Picasso.get().load(getIntent().getStringExtra("image_item")).into(singer_image);


                     //   LinearLayoutManager mLayoutManager = new LinearLayoutManager(PodcastAlbumActivity.this,RecyclerView.HORIZONTAL,false);
                      //  recyclerview.setLayoutManager(mLayoutManager);
                        recyclerview.setLayoutManager(new GridLayoutManager(PodcastAlbumActivity.this, 2));
                        podcastalbumadapter = new PodcastAlbumAdapter(PodcastAlbumActivity.this,podcastalubumlist);
                        recyclerview.setAdapter(podcastalbumadapter);

                       /* LinearLayoutManager mLayoutManager1 = new LinearLayoutManager(PodcastAlbumActivity.this,RecyclerView.HORIZONTAL,false);
                        recyclerview1.setLayoutManager(mLayoutManager1);
                        featuredPodcastsAdapter = new TopSeriesPodcastsAdapter(PodcastAlbumActivity.this,topserieslist);
                        recyclerview1.setAdapter(featuredPodcastsAdapter);*/
                    }
                    else
                    { }
                }
                catch (Exception e)
                {
                    Log.e("Login Faild", e.toString());
                }
            }
            @Override
            public void onFailure(Call<PodcastAlbumResponse> call, Throwable t) { }
        });
    }
}