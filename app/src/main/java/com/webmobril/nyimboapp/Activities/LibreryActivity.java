package com.webmobril.nyimboapp.Activities;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.tabs.TabLayout;
import com.google.android.material.tabs.TabLayout.TabLayoutOnPageChangeListener;
import com.webmobril.nyimboapp.Adapters.LibreryAdapter;
import com.webmobril.nyimboapp.Interface.OnSignupClick;
import com.webmobril.nyimboapp.R;
import com.webmobril.nyimboapp.fragments.MusicFragment;
import com.webmobril.nyimboapp.fragments.PodcastsFragment;


import java.util.ArrayList;

public class LibreryActivity extends AppCompatActivity implements OnSignupClick
{
    ViewPager simpleViewPager;
    TabLayout tabLayout;
    private ArrayList<Fragment> fragmentArrayList;
    private ArrayList<String> titleList;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_librery);

        simpleViewPager = (ViewPager) findViewById(R.id.simpleViewPager);
        tabLayout = (TabLayout) findViewById(R.id.simpleTabLayout);
        fragmentArrayList=new ArrayList<>();
        titleList=new ArrayList<>();

        fragmentArrayList.add(new MusicFragment());
        titleList.add("Music");

        fragmentArrayList.add(new PodcastsFragment());
        titleList.add("Podcasts");

        tabLayout.setupWithViewPager(simpleViewPager);
        simpleViewPager.setOffscreenPageLimit(1);

        LibreryAdapter adapter = new LibreryAdapter(getSupportFragmentManager(), fragmentArrayList,titleList);
        simpleViewPager.setAdapter(adapter);
        // addOnPageChangeListener event change the tab on slide
        simpleViewPager.addOnPageChangeListener(new TabLayoutOnPageChangeListener(tabLayout));
    }

    @Override
    public void onSignUp(boolean isSignUp)
    {
        if(isSignUp)
        {
            simpleViewPager.setCurrentItem(1);
        }
        else
        {
            simpleViewPager.setCurrentItem(0);
        }
    }
}

