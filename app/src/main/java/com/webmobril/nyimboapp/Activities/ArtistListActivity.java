package com.webmobril.nyimboapp.Activities;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.webmobril.nyimboapp.Adapters.SelectArtistAdapter;
import com.webmobril.nyimboapp.Models.GetArtistResponseModel;
import com.webmobril.nyimboapp.Models.SelectArtistModel;
import com.webmobril.nyimboapp.Response.SelectArtistResponse;
import com.webmobril.nyimboapp.R;
import com.webmobril.nyimboapp.Response.SetArtistResponse;
import com.webmobril.nyimboapp.Sharedpreference.LoginPreferences;
import com.webmobril.nyimboapp.Utils.ProgressD;
import com.webmobril.nyimboapp.Utils.UrlApi;
import com.webmobril.nyimboapp.network.ApiInterface;

import java.util.ArrayList;
import java.util.List;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

public class ArtistListActivity extends AppCompatActivity
{
    RecyclerView recyclerView;
    SelectArtistAdapter selectartistAdapter;
    List<SelectArtistModel> selectartistlist;
    String language_list;
    private String strCheckedItem="";
    RelativeLayout relative_artist;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_artist_list);
        init();
        SelectArtistApi();

        relative_artist.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                if(!TextUtils.isEmpty(strCheckedItem))
                {
                    hitsetArtistApi();

                }
                else
                {
                    Toast.makeText(ArtistListActivity.this, "Please select Artist", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
    public void init()
    {
        selectartistlist = new ArrayList<SelectArtistModel>();
        recyclerView=findViewById(R.id.recyclerView);
        language_list=getIntent().getStringExtra("language_list");
        relative_artist =findViewById(R.id.relative_artist);
    }

    private void SelectArtistApi()
    {
        final ProgressD progressDialog = ProgressD.show(ArtistListActivity.this,getResources().getString(R.string.logging_in), true, false, null);
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();
        Gson gson = new GsonBuilder()
                .setLenient()
                .create();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(UrlApi.BASE_URL)
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(client)
                .build();
        ApiInterface service = retrofit.create(ApiInterface.class);
        Call<SelectArtistResponse> call = service.GetArtist(language_list,LoginPreferences.getActiveInstance(ArtistListActivity.this).getUser_id());
        call.enqueue(new Callback<SelectArtistResponse>()
        {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onResponse(Call<SelectArtistResponse> call, retrofit2.Response<SelectArtistResponse> response)
            {
                progressDialog.dismiss();
                try
                {
                    SelectArtistResponse resultFile = response.body();
                    List<GetArtistResponseModel> getartistmodellist=resultFile.getResult();

                    GridLayoutManager gridLayoutManager = new GridLayoutManager(ArtistListActivity.this, 3);
                    recyclerView.setLayoutManager(gridLayoutManager);
                    selectartistAdapter = new SelectArtistAdapter(ArtistListActivity.this,getartistmodellist);
                    recyclerView.setAdapter(selectartistAdapter);
                    String status=resultFile.getStatus();

                    if(resultFile.getStatus()!=null && status.equals("true"))
                    {
                        relative_artist.setVisibility(View.VISIBLE);
                    }
                    else
                    {
                        relative_artist.setVisibility(View.GONE);
                    }

                    selectartistAdapter.setCheckedItemListner(new SelectArtistAdapter.ItemCheckListner()
                    {
                        @Override
                        public void setCheckedList(String list)
                        {
                            strCheckedItem="";
                            Log.d("checked_list",list);
                        }

                        @Override
                        public void remove(GetArtistResponseModel selectartistitem) {
                            if(selectartistitem.isSelected())
                            {
                                if(strCheckedItem.equals(""))
                                {
                                    strCheckedItem=""+ selectartistitem.getArtistId();
                                }
                                else
                                {
                                    strCheckedItem=strCheckedItem+","+selectartistitem.getArtistId();
                                }
                                Log.e("LoginFaild",strCheckedItem.toString());
                            }
                        }
                    });
                }

                catch (Exception e)
                {
                    Log.e("Login Faild", e.toString());
                    progressDialog.dismiss();
                }
            }

            @Override
            public void onFailure(Call<SelectArtistResponse> call, Throwable t)
            {
                progressDialog.dismiss();
            }
        });
    }


    private void hitsetArtistApi()
    {
        final ProgressD progressDialog = ProgressD.show(ArtistListActivity.this,getResources().getString(R.string.logging_in), true, false, null);
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();
        Gson gson = new GsonBuilder()
                .setLenient()
                .create();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(UrlApi.BASE_URL)
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(client)
                .build();
        ApiInterface service = retrofit.create(ApiInterface.class);
        Call<SetArtistResponse> call = service.SetArtist(strCheckedItem,LoginPreferences.getActiveInstance(ArtistListActivity.this).getUser_id());
        call.enqueue(new Callback<SetArtistResponse>()
        {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onResponse(Call<SetArtistResponse> call, retrofit2.Response<SetArtistResponse> response)
            {
                progressDialog.dismiss();
                try
                {
                    SetArtistResponse resultFile = response.body();
                    if(resultFile.getStatus().equals("true"))
                    {
                        Intent i=new Intent(ArtistListActivity.this,MainActivity.class);
                        startActivity(i);
                        LoginPreferences.getActiveInstance(ArtistListActivity.this).setSelectedArtiststatus(strCheckedItem);
                    }
                    else
                    { }
                }
                catch (Exception e)
                {
                    Log.e("Login Faild", e.toString());
                    progressDialog.dismiss();
                }
            }

            @Override
            public void onFailure(Call<SetArtistResponse> call, Throwable t)
            {
                progressDialog.dismiss();
            }
        });
    }
}