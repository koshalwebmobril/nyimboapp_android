package com.webmobril.nyimboapp.Activities;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.DatePicker;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.webmobril.nyimboapp.R;
import com.webmobril.nyimboapp.Response.ResultFileResponse;
import com.webmobril.nyimboapp.Sharedpreference.LoginPreferences;
import com.webmobril.nyimboapp.Utils.CommonMethod;

import com.webmobril.nyimboapp.Utils.ProgressD;
import com.webmobril.nyimboapp.Utils.UrlApi;
import com.webmobril.nyimboapp.network.ApiInterface;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

import static com.webmobril.nyimboapp.Utils.CommonMethod.isValidEmaillId;

public class RegisterActivity extends AppCompatActivity implements View.OnClickListener,DatePickerDialog.OnDateSetListener {
    RelativeLayout relative_login;
    TextView username, email_id, dateofbirth, password;
    LinearLayout linear_register, linear_data_of_birth;
    private DatePickerDialog datePickerDialog;
    Calendar dateSelected;
    private RadioGroup radioGroup;
    private RadioButton radioButton;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        init();
    }
    public void init()
    {
        relative_login = findViewById(R.id.relative_login);
        username = findViewById(R.id.username);
        email_id = findViewById(R.id.email_id);
        dateofbirth = findViewById(R.id.dateofbirth);
        password = findViewById(R.id.password);
        linear_register = findViewById(R.id.linear_register);
        linear_data_of_birth = findViewById(R.id.linear_data_of_birth);
        radioGroup = (RadioGroup) findViewById(R.id.radioGrp);




        linear_register.setOnClickListener(this);
        relative_login.setOnClickListener(this);
        linear_data_of_birth.setOnClickListener(this);
        dateofbirth.setOnClickListener(this);

        dateSelected = Calendar.getInstance();     //calander show
    }

    @Override
    public void onClick(View v)
    {
        switch (v.getId())
        {
            case R.id.linear_register:
                if (CommonMethod.isOnline(this))
                {
                    hideKeyboard((LinearLayout) v);
                    if (validation())
                    {
                        int selectedId = radioGroup.getCheckedRadioButtonId();
                        radioButton = (RadioButton) findViewById(selectedId);
                        RegisterUser(username.getText().toString().trim(),email_id.getText().toString().trim(),
                                password.getText().toString().trim(),"1","wrfff",
                                dateofbirth.getText().toString().trim()
                                ,radioButton.getText().toString());
                    }
                }
                else
                    {
                       CommonMethod.showAlert(getString(R.string.check_internet), this);
                    }
                break;


            case R.id.relative_login:
                Intent i = new Intent(this, LoginActivity.class);
                startActivity(i);
                break;


            case R.id.dateofbirth:
                hideKeyboard((TextView) v);

                final Calendar newCalendar = Calendar.getInstance();
                int year = newCalendar.get(Calendar.YEAR);
                int month = newCalendar.get(Calendar.MONTH);
                int day = newCalendar.get(Calendar.DAY_OF_MONTH);
                datePickerDialog = new DatePickerDialog(this, R.style.datepicker, this, year, month, day);
                datePickerDialog.getDatePicker().setMaxDate(new Date().getTime());
                datePickerDialog.show();
                break;
        }
    }

    public void hideKeyboard(View view)
    {
        try {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        } catch (Exception ignored) {
        }
    }

    private boolean validation()
    {
        if(TextUtils.isEmpty(username.getText().toString().trim()))
        {
            Toast.makeText(this, "Please enter username", Toast.LENGTH_SHORT).show();
            return false;
        }
        else if (username.getText().toString().matches("[0-9]+"))
        {
            Toast.makeText(this, "Enter valid username", Toast.LENGTH_SHORT).show();
            return false;
        }
         else if (String.valueOf(username.getText().toString().charAt(0)).matches("[0-9]+"))
         {
            Toast.makeText(this, "Enter valid username", Toast.LENGTH_SHORT).show();
            return false;
         }

        else if (String.valueOf(username.getText().toString().charAt(0)).matches("[$&+,:;=\\\\\\\\?@#|/'<>.^*()%!-]"))
        {
            Toast.makeText(this, "Enter valid username", Toast.LENGTH_SHORT).show();
            return false;
        }
        else if (TextUtils.isEmpty(email_id.getText().toString().trim())) {
            Toast.makeText(this, "Please enter Email Id", Toast.LENGTH_SHORT).show();
            return false;
        } else if (!isValidEmaillId(email_id.getText().toString().trim())) {
            Toast.makeText(this, "Please enter a valid Email Id", Toast.LENGTH_SHORT).show();
            return false;
        } else if (TextUtils.isEmpty(dateofbirth.getText().toString().trim())) {
            Toast.makeText(this, "Please enter date of birth", Toast.LENGTH_SHORT).show();
            return false;
        }
        else if (TextUtils.isEmpty(password.getText().toString().trim()))
        {
            Toast.makeText(this, "Please enter password", Toast.LENGTH_SHORT).show();
            return false;
        } else if (password.getText().toString().trim().length() < 8 || password.getText().toString().trim().length() > 16) {
            Toast.makeText(this, "Password must be 8 to 16 characters", Toast.LENGTH_LONG).show();
            return false;
        }
        return true;
    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int day)
    {
        Calendar newDate = Calendar.getInstance();
        newDate.set(year, month, day);
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
        String birthDate = simpleDateFormat.format(newDate.getTime());
        dateofbirth.setText(birthDate);
    }

    private void RegisterUser(String username,String email,String password,String device_type,String device_token,String dob,
                              String gender) {
        final ProgressD progressDialog = ProgressD.show(RegisterActivity.this,getResources().getString(R.string.logging_in), true, false, null);
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();
        Gson gson = new GsonBuilder()
                .setLenient()
                .create();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(UrlApi.BASE_URL)
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(client)
                .build();
        ApiInterface service = retrofit.create(ApiInterface.class);
        Call<ResultFileResponse> call = service.RegisterUser(username,email,password,device_type,device_token,dob,gender);


        call.enqueue(new Callback<ResultFileResponse>()
        {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onResponse(Call<ResultFileResponse> call, retrofit2.Response<ResultFileResponse> response)
            {
                try
                {
                    ResultFileResponse resultFileResponse = response.body();
                  //  String status = resultFile.getStatus();
                    String message = resultFileResponse.getMessage();

                    progressDialog.dismiss();
                    Toast.makeText(RegisterActivity.this, message, Toast.LENGTH_SHORT).show();

                    if (resultFileResponse.getStatus()!=null&& resultFileResponse.getStatus().equals("true"))
                    {
                        String token = resultFileResponse.getAuthenticationModel().getToken();
                        LoginPreferences.getActiveInstance(RegisterActivity.this).setToken(token);
                        LoginPreferences.getActiveInstance(RegisterActivity.this).setUser_id(resultFileResponse.getAuthenticationModel().getId());

                        Intent i=new Intent(RegisterActivity.this,SelectLanguageActivity.class);
                        startActivity(i);
                        finish();
                    }
                    else
                    { }
                }
                catch (Exception e)
                {
                    Log.e("Register Faild", e.toString());
                    progressDialog.dismiss();
                }
            }

            @Override
            public void onFailure(Call<ResultFileResponse> call, Throwable t)
            {
                Toast.makeText(RegisterActivity.this, "Failed" + t, Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    protected void onResume()
    {
        super.onResume();
        username.setText("");
        email_id.setText("");
        dateofbirth.setText("");
        password.setText("");
    }
}