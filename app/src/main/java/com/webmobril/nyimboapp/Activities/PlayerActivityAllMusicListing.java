package com.webmobril.nyimboapp.Activities;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import android.media.MediaPlayer;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.squareup.picasso.Picasso;
import com.webmobril.nyimboapp.Models.GetAllSongListModel;
import com.webmobril.nyimboapp.R;
import com.webmobril.nyimboapp.Response.UserLikeUnlikeResponse;
import com.webmobril.nyimboapp.Sharedpreference.LoginPreferences;
import com.webmobril.nyimboapp.Utils.Singleton;
import com.webmobril.nyimboapp.Utils.UrlApi;
import com.webmobril.nyimboapp.network.ApiInterface;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

import static android.view.View.GONE;

public class PlayerActivityAllMusicListing extends AppCompatActivity implements View.OnClickListener {
    ImageView imgBack, next_music, pause_icon, previous_music;
    String song_url, song_name,singer_img;
    ImageView singer_image,unlike_icon,like_icon;
    ProgressBar progressBar;
    private SeekBar playerseekbar;
    int position;

    private Handler handler = new Handler();
    MediaPlayer mediaplayer;
    private TextView textcurrentTime, texttotalDuration;
    private  TextView song_name_textview,artist_name_textview;
    List<GetAllSongListModel> audiomodellist;
    String song_id,like_dislike_songs_count;
    String artistName;
    Singleton singleton=Singleton.getInstance();

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_player);
        init();
        FirstTimeMusicPlay();
    }
    private void init()
    {
        imgBack = findViewById(R.id.imgBack);
        pause_icon = findViewById(R.id.pause_icon);
        next_music = findViewById(R.id.next_music);
        previous_music = findViewById(R.id.previous_music);
        singer_image=findViewById(R.id.singer_image);
        song_name_textview=findViewById(R.id.song_name);

        unlike_icon=findViewById(R.id.unlike_icon);
        like_icon=findViewById(R.id.like_icon);

        playerseekbar = findViewById(R.id.seekBar);
        progressBar = findViewById(R.id.progress_circular);
        pause_icon = findViewById(R.id.pause_icon);
        textcurrentTime = findViewById(R.id.textcurrentTime);
        texttotalDuration = findViewById(R.id.texttotalDuration);
        artist_name_textview= findViewById(R.id.artist_name_textview);

        getIntentData();                          //getintent

        imgBack.setOnClickListener(this);                         //  OnClickListner
        pause_icon.setOnClickListener(this);
        next_music.setOnClickListener(this);
        previous_music.setOnClickListener(this);
        like_icon.setOnClickListener(this);
        unlike_icon.setOnClickListener(this);

        Picasso.get().load(singer_img).into(singer_image);        //load singer image
        song_name_textview.setText(song_name);
        artist_name_textview.setText(artistName);


        if(like_dislike_songs_count.equals("0"))
        {
            like_icon.setVisibility(GONE);
            unlike_icon.setVisibility(View.VISIBLE);
        }
        else
        {
            like_icon.setVisibility(View.VISIBLE);
            unlike_icon.setVisibility(GONE);
        }


        mediaplayer = new MediaPlayer();
        playerseekbar.setMax(100);

        playerseekbar.setOnTouchListener(new View.OnTouchListener()
        {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                SeekBar seekBar = (SeekBar) v;
                int playposition = (mediaplayer.getDuration() / 100) * seekBar.getProgress();
                mediaplayer.seekTo(playposition);
                textcurrentTime.setText(millisecondstoTimer(mediaplayer.getCurrentPosition()));
                return false;
            }
        });


        mediaplayer.setOnBufferingUpdateListener(new MediaPlayer.OnBufferingUpdateListener() {
            @Override
            public void onBufferingUpdate(MediaPlayer mp, int percent) {
                playerseekbar.setSecondaryProgress(percent);
            }
        });

        mediaplayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                playerseekbar.setProgress(0);
                pause_icon.setImageResource(R.drawable.pause);
                textcurrentTime.setText("0.00");
                texttotalDuration.setText("0.00");
                mediaplayer.reset();
                preparemediaplayer();
                if(position==1)
                {

                    next_music.setAlpha(0.50f);
                    next_music.setClickable(false);
                }
                else if(position==audiomodellist.size()-1)
                {
                    next_music.setAlpha(0.50f);
                    next_music.setClickable(false);
                }
            }
        });
    }

    public void getIntentData()
    {
        song_name = getIntent().getStringExtra("song_name");
        song_url = getIntent().getStringExtra("song_url");
        singer_img=getIntent().getStringExtra("singer_image");
        artistName=getIntent().getStringExtra("artistName");


        audiomodellist = new ArrayList<>();
        audiomodellist=singleton.audiomodellist;
        //music list
       // audiomodellist = getIntent().getParcelableArrayListExtra("playlist");
        song_id=getIntent().getStringExtra("song_id");
        like_dislike_songs_count=getIntent().getStringExtra("like_dislike_songs_count");
        Bundle extras = getIntent().getExtras();
        if(extras != null)
        {
            position = extras.getInt("position");
            Log.d("position","position"+ position);
        }
        if (audiomodellist!=null&&audiomodellist.size()<=1)
        {
            previous_music.setAlpha(0.50f);
            next_music.setAlpha(0.50f);
            previous_music.setClickable(false);
            next_music.setClickable(false);

        }
        if (position==0&&audiomodellist.size()>1)
        {
            previous_music.setAlpha(0.50f);
            previous_music.setClickable(false);
            next_music.setAlpha(1f);
            next_music.setClickable(true);
        }
        if(position==audiomodellist.size()-1)
        {
            previous_music.setAlpha(1f);
            previous_music.setClickable(true);
            next_music.setAlpha(0.50f);
            next_music.setClickable(false);
        }
        if(audiomodellist.size()==1)
        {
            previous_music.setAlpha(0.50f);
            previous_music.setClickable(false);
            next_music.setAlpha(0.50f);
            next_music.setClickable(false);
        }
    }
    public void FirstTimeMusicPlay()
    {
        try {
            progressBar.setVisibility(View.VISIBLE);
            pause_icon.setVisibility(GONE);
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    try {
                        progressBar.setVisibility(GONE);
                        pause_icon.setVisibility(View.VISIBLE);
                        preparemediaplayer();
                        mediaplayer.start();
                        pause_icon.setImageResource(R.drawable.play);
                        updateseekbar();
                    } catch (Exception e) {
                    }
                }
            }, 1000);
        } catch (Exception e) {
        }
    }

    public void NextTimeMusicPlay(final String nextsongurl)
    {
        try {
            mediaplayer=new MediaPlayer();
            playerseekbar.setMax(100);
            progressBar.setVisibility(View.VISIBLE);
            pause_icon.setVisibility(GONE);
            new Handler().postDelayed(new Runnable()
            {
                @Override
                public void run()
                {
                    progressBar.setVisibility(GONE);
                    pause_icon.setVisibility(View.VISIBLE);
                    try
                    {
                        mediaplayer.setDataSource(nextsongurl);
                        mediaplayer.prepare();
                        texttotalDuration.setText(millisecondstoTimer(mediaplayer.getDuration()));
                    }
                    catch (Exception exception)
                    {
                        // Toast.makeText(PlayerNewActivity.this,exception.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                    mediaplayer.start();
                    pause_icon.setImageResource(R.drawable.play);
                    updateseekbar();
                }
            }, 1000);
        }
        catch (Exception e)
        {

        }

    }

    private String millisecondstoTimer(long millisecond) {
        String timerstring = "";
        String secondsstring;
        int hours = (int) (millisecond / (1000 * 60 * 60));
        int minutes = (int) (millisecond % (1000 * 60 * 60)) / (1000 * 60);
        int seconds = (int) ((millisecond % (1000 * 60 * 60)) % (1000 * 60) / 1000);

        if (hours > 0) {
            timerstring = hours + ":";

        }

        if (seconds < 10) {
            secondsstring = "0" + seconds;

        } else {
            secondsstring = "" + seconds;
        }

        timerstring = timerstring + minutes + ':' + secondsstring;
        return timerstring;
    }

    private void preparemediaplayer() {
        try {
            mediaplayer.setDataSource(song_url);
            mediaplayer.prepare();
            texttotalDuration.setText(millisecondstoTimer(mediaplayer.getDuration()));
        } catch (IOException exception) {
            //Toast.makeText(this,exception.toString(), Toast.LENGTH_SHORT).show();
            Log.e("error", exception.toString());
        }
    }

    private void updateseekbar() {

        try {
            if (mediaplayer.isPlaying()) {
                playerseekbar.setProgress((int) (((float) mediaplayer.getCurrentPosition() / mediaplayer.getDuration()) * 100));
                handler.postDelayed(updater, 1000);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private Runnable updater = new Runnable() {
        @Override
        public void run() {
            try {
                updateseekbar();
                long currentDuration = mediaplayer.getCurrentPosition();
                textcurrentTime.setText(millisecondstoTimer(currentDuration));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    };

    @Override
    public void onBackPressed() {
        if (mediaplayer.isPlaying()) {
            handler.removeCallbacks(updater);
            mediaplayer.stop();
            pause_icon.setImageResource(R.drawable.pause);
        }
        finish();
    }


    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.imgBack:
                if (mediaplayer != null && mediaplayer.isPlaying()) {
                    mediaplayer.stop();
                    handler.removeCallbacks(updater);
                    pause_icon.setImageResource(R.drawable.pause);
                    finish();
                }
                else
                    {
                    try {
                        mediaplayer = null;
                        handler.removeCallbacks(updater);
                        finish();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }
                break;

            case R.id.pause_icon:
                if (mediaplayer.isPlaying()) {
                    handler.removeCallbacks(updater);
                    mediaplayer.pause();
                    pause_icon.setImageResource(R.drawable.pause);
                } else {
                    mediaplayer.start();
                    pause_icon.setImageResource(R.drawable.play);
                    updateseekbar();
                }
                break;


            case R.id.next_music:
                if(position<audiomodellist.size()-1)
                {
                    previous_music.setClickable(true);
                    previous_music.setAlpha(1f);
                    position++;
                    if(position==audiomodellist.size()-1)
                    {
                        next_music.setAlpha(0.50f);
                        next_music.setClickable(false);
                        previous_music.setClickable(true);
                        previous_music.setAlpha(1f);
                    }
                    else if(position==0)
                    {
                        position=0;
                        next_music.setAlpha(1f);
                        next_music.setClickable(true);
                        previous_music.setClickable(false);
                        previous_music.setAlpha(0.50f);
                    }

                    GetAllSongListModel next_song=audiomodellist.get(position);
                    final String nextsongurl = next_song.getUploadSong();
                    final String song_name = next_song.getName();

                    String artistName = "";
                    if(next_song.getArtistSongs().size()==1)
                    {
                        artistName=next_song.getArtistSongs().get(0).getArtistName();
                    }
                    else
                    {
                        for (int i = 0; i < next_song.getArtistSongs().size(); i++)
                        {
                            if(i==0){
                                artistName=next_song.getArtistSongs().get(0).getArtistName();

                            }else {
                                artistName=artistName+","+next_song.getArtistSongs().get(i).getArtistName();
                            }
                        }
                    }


                    int likeunlikestatus = next_song.getLikeDislikeSongsCount();
                    song_id= String.valueOf(next_song.getId());
                    Picasso.get().load(next_song.getImage()).into(singer_image);
                    if(likeunlikestatus == 0)
                    {
                        like_icon.setVisibility(GONE);
                        unlike_icon.setVisibility(View.VISIBLE);
                    }
                    else
                    {
                        like_icon.setVisibility(View.VISIBLE);
                        unlike_icon.setVisibility(GONE);
                    }
                    try
                    {
                        handler.removeCallbacks(updater);
                        mediaplayer.stop();
                        NextTimeMusicPlay(nextsongurl);
                        song_name_textview.setText(song_name);
                        artist_name_textview.setText(artistName);
                    }
                    catch (Exception e)
                    {
                        e.printStackTrace();
                    }
                }
                break;

            case R.id.previous_music:
                if(position==0)
                {
                    position=0;
                    // Toast.makeText(this, "Arrived at the beginning", Toast.LENGTH_SHORT).show();
                    previous_music.setClickable(false);
                    previous_music.setAlpha(0.50f);
                    next_music.setClickable(true);
                    next_music.setAlpha(1f);
                }
                else
                {
                        previous_music.setClickable(false);
                        previous_music.setAlpha(0.50f);

                    previous_music.setClickable(true);
                    previous_music.setAlpha(1f);
                    next_music.setClickable(true);
                    next_music.setAlpha(1f);                                       //new changes
                    position--;
                    if(position==0)
                    {
                        position=0;
                        previous_music.setClickable(false);
                        previous_music.setAlpha(0.50f);
                        next_music.setClickable(true);
                        next_music.setAlpha(1f);
                    }
                }
                GetAllSongListModel privious_song=audiomodellist.get(position);
                final String privsongurl = privious_song.getUploadSong();
                final String privsong_name = privious_song.getName();

                String artistName = "";
                if (privious_song.getArtistSongs().size()==1)
                {
                    artistName=privious_song.getArtistSongs().get(0).getArtistName();
                }
                else
                {
                    for (int i = 0; i < privious_song.getArtistSongs().size(); i++)
                    {
                        if(i==0){
                            artistName=privious_song.getArtistSongs().get(0).getArtistName();

                        }else {
                            artistName=artistName+","+privious_song.getArtistSongs().get(i).getArtistName();
                        }
                    }
                }

                int likeunlikestatus = privious_song.getLikeDislikeSongsCount();
                song_id= String.valueOf(privious_song.getId());
                Picasso.get().load(privious_song.getImage()).into(singer_image);

                if(likeunlikestatus==0)
                {
                    like_icon.setVisibility(GONE);
                    unlike_icon.setVisibility(View.VISIBLE);
                }
                else
                {
                    like_icon.setVisibility(View.VISIBLE);
                    unlike_icon.setVisibility(GONE);
                }
                try
                {
                 handler.removeCallbacks(updater);
                 mediaplayer.stop();
                 NextTimeMusicPlay(privsongurl);
                 song_name_textview.setText(privsong_name);
                 artist_name_textview.setText(artistName);
             }
             catch (Exception e)
             { }
             break;

            case R.id.like_icon:
                hitlikeunlikeApi("0");
                break;

            case R.id.unlike_icon:
                hitlikeunlikeApi("1");
                break;
        }
        }


    private void hitlikeunlikeApi(String likeunlike)
    {
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();
        Gson gson = new GsonBuilder()
                .setLenient()
                .create();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(UrlApi.BASE_URL)
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(client)
                .build();
        ApiInterface service = retrofit.create(ApiInterface.class);

        Call<UserLikeUnlikeResponse> call = service.likeunlike(LoginPreferences.getActiveInstance(this).getUser_id(),song_id,likeunlike);
        call.enqueue(new Callback<UserLikeUnlikeResponse>()
        {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onResponse(Call<UserLikeUnlikeResponse> call, retrofit2.Response<UserLikeUnlikeResponse> response)
            {
                try
                {
                    UserLikeUnlikeResponse resultFile = response.body();
                    Toast.makeText(PlayerActivityAllMusicListing.this,resultFile.getMessage() , Toast.LENGTH_SHORT).show();
                    if(resultFile.getStatus().equals("true"))
                    {
                        if(likeunlike.equals("1"))
                        {
                            like_icon.setVisibility(View.VISIBLE);
                            unlike_icon.setVisibility(GONE);
                            GetAllSongListModel songlist=audiomodellist.get(position);
                            songlist.setLikeDislikeSongsCount(1);
                            audiomodellist.set(position,songlist);
                        }
                        else
                        {
                            unlike_icon.setVisibility(View.VISIBLE);
                            like_icon.setVisibility(GONE);
                            GetAllSongListModel songlist=audiomodellist.get(position);
                            songlist.setLikeDislikeSongsCount(0);
                            audiomodellist.set(position,songlist);
                           // Toast.makeText(PlayerActivityAllMusicListing.this, "Dislike song", Toast.LENGTH_SHORT).show();
                        }
                    }
                    else
                    { }
                }
                catch (Exception e)
                {
                    Log.e("Login Faild", e.toString());
                }
            }
            @Override
            public void onFailure(Call<UserLikeUnlikeResponse> call, Throwable t) { }
        });
    }
    }


