package com.webmobril.nyimboapp.Activities;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.webmobril.nyimboapp.Adapters.SelectLanguageAdapter;
import com.webmobril.nyimboapp.Adapters.UserSelectLanguageAdapter;
import com.webmobril.nyimboapp.Models.LanguageUpdateResponse;
import com.webmobril.nyimboapp.Models.SelectLanguageModel1;

import com.webmobril.nyimboapp.Models.UserSelectLanguageModel1;
import com.webmobril.nyimboapp.R;

import com.webmobril.nyimboapp.Response.SelectLanguageResponse;
import com.webmobril.nyimboapp.Response.UserSelectLanguageResponse;
import com.webmobril.nyimboapp.Sharedpreference.LoginPreferences;
import com.webmobril.nyimboapp.Utils.ProgressD;
import com.webmobril.nyimboapp.Utils.UrlApi;
import com.webmobril.nyimboapp.network.ApiInterface;

import java.util.ArrayList;
import java.util.List;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

public class UserSelectLanguageActivity extends AppCompatActivity {
    RecyclerView recyclerView;
    UserSelectLanguageAdapter userSelectLanguageAdapter;
    ArrayList<UserSelectLanguageModel1> selectlanguagelist;
    private String strCheckedItem = "";
    private RelativeLayout relative_langauge;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_language);
        init();
        SelectLanguageApi();
    }

    public void init()
    {
        selectlanguagelist = new ArrayList<UserSelectLanguageModel1>();
        recyclerView = findViewById(R.id.recyclerView);
        relative_langauge = findViewById(R.id.relative_artist);

        relative_langauge.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (selectlanguagelist.size() == 1) {
                    if (selectlanguagelist.get(0).getStatus() == 0) {
                        strCheckedItem = String.valueOf(selectlanguagelist.get(0).getId());
                    } else {
                        strCheckedItem = "";
                    }
                } else if (selectlanguagelist.size() > 1) {
                    strCheckedItem = "";
                    for (UserSelectLanguageModel1 userSelectLanguageModel1 : selectlanguagelist) {
                        if (userSelectLanguageModel1.getStatus() == 0) {
                            if(TextUtils.isEmpty(strCheckedItem)){
                                strCheckedItem = String.valueOf(userSelectLanguageModel1.getId());

                            }else {
                                strCheckedItem = strCheckedItem + "," + userSelectLanguageModel1.getId();

                            }
                        }

                    }


                }
                Log.i("TAG", "onClick1111: " + strCheckedItem);

                if (!TextUtils.isEmpty(strCheckedItem))
                {
                    hitUpdateLanguageApi();
                }
                else {
                    Toast.makeText(UserSelectLanguageActivity.this, "Please select language", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void SelectLanguageApi() {
        final ProgressD progressDialog = ProgressD.show(UserSelectLanguageActivity.this, getResources().getString(R.string.logging_in), true, false, null);
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();
        Gson gson = new GsonBuilder()
                .setLenient()
                .create();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(UrlApi.BASE_URL)
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(client)
                .build();
        ApiInterface service = retrofit.create(ApiInterface.class);
        Call<UserSelectLanguageResponse> call = service.GetUserLanguage(LoginPreferences.getActiveInstance(UserSelectLanguageActivity.this).getUser_id());
        call.enqueue(new Callback<UserSelectLanguageResponse>() {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onResponse(Call<UserSelectLanguageResponse> call, retrofit2.Response<UserSelectLanguageResponse> response) {
                progressDialog.dismiss();
                try {
                    UserSelectLanguageResponse resultFile = response.body();
                    List<UserSelectLanguageModel1> selectLanguageModel1 = resultFile.getSelectLanguageModel1List();
                    selectlanguagelist = (ArrayList<UserSelectLanguageModel1>) response.body().getSelectLanguageModel1List();
                    LinearLayoutManager gridLayoutManager = new GridLayoutManager(UserSelectLanguageActivity.this, 2);
                    recyclerView.setLayoutManager(gridLayoutManager);

                    userSelectLanguageAdapter = new UserSelectLanguageAdapter(UserSelectLanguageActivity.this, selectLanguageModel1);
                    recyclerView.setAdapter(userSelectLanguageAdapter);
                    String status = resultFile.getStatus();

                    if (status.equals("true")) {
                        relative_langauge.setVisibility(View.VISIBLE);
                    } else {
                        relative_langauge.setVisibility(View.GONE);
                    }

                    userSelectLanguageAdapter.setCheckedItemListner(new UserSelectLanguageAdapter.ItemCheckListner() {
                        @Override
                        public void setCheckedList(String list) {
                            strCheckedItem = "";
                            Log.d("checked_list", strCheckedItem);
                        }

                        @Override
                        public void remove(UserSelectLanguageModel1 selectlangaugeItem) {
                            if (selectlangaugeItem.isSelected()) {
                                if (strCheckedItem.equals("")) {
                                    strCheckedItem = "" + selectlangaugeItem.getId();
                                } else {
                                    strCheckedItem = strCheckedItem + "," + selectlangaugeItem.getId();
                                }
                            }
                        }

                        @Override
                        public void onItemSelect(int position, int status) {
                            if (status == 1)
                            {
                                UserSelectLanguageModel1 selectlangaugeItem = selectlanguagelist.get(position);
                                selectlangaugeItem.setStatus(1);
                                selectlanguagelist.set(position, selectlangaugeItem);
                            } else {
                                UserSelectLanguageModel1 selectlangaugeItem = selectlanguagelist.get(position);
                                selectlangaugeItem.setStatus(0);
                                selectlanguagelist.set(position, selectlangaugeItem);
                            }
                            userSelectLanguageAdapter.notifyDataSetChanged();
                        }
                    });
                } catch (Exception e) {
                    Log.e("Login Faild", e.toString());
                }
            }

            @Override
            public void onFailure(Call<UserSelectLanguageResponse> call, Throwable t) {
            }
        });
    }


    private void hitUpdateLanguageApi() {
        final ProgressD progressDialog = ProgressD.show(UserSelectLanguageActivity.this, getResources().getString(R.string.logging_in), true, false, null);
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();
        Gson gson = new GsonBuilder()
                .setLenient()
                .create();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(UrlApi.BASE_URL)
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(client)
                .build();
        ApiInterface service = retrofit.create(ApiInterface.class);
        Call<LanguageUpdateResponse> call = service.updateLanguage(strCheckedItem, LoginPreferences.getActiveInstance(UserSelectLanguageActivity.this).getUser_id());
        call.enqueue(new Callback<LanguageUpdateResponse>() {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onResponse(Call<LanguageUpdateResponse> call, retrofit2.Response<LanguageUpdateResponse> response) {
                progressDialog.dismiss();
                try {
                    LanguageUpdateResponse resultFile = response.body();
                    //  List<LanguageUpdateResponse> selectLanguageModel1=resultFile.getSelectLanguageModel1List();
                    String status = resultFile.getStatus();
                    if (status.equals("true")) {
                        Intent i = new Intent(UserSelectLanguageActivity.this, MainActivity.class);
                        startActivity(i);
                        Log.d("userselectedlang", strCheckedItem);
                    } else {
                    }
                    userSelectLanguageAdapter.setCheckedItemListner(new UserSelectLanguageAdapter.ItemCheckListner() {
                        @Override
                        public void setCheckedList(String list) {
                            strCheckedItem = "";
                            Log.d("checked_list", list);
                        }

                        @Override
                        public void remove(UserSelectLanguageModel1 selectlangaugeItem) {
                            if (selectlangaugeItem.isSelected()) {
                                if (strCheckedItem.equals("")) {
                                    strCheckedItem = "" + selectlangaugeItem.getId();
                                } else {
                                    strCheckedItem = strCheckedItem + "," + selectlangaugeItem.getId();
                                }
                            }
                        }

                        @Override
                        public void onItemSelect(int position, int status) {
                            if (status == 1) {
                                UserSelectLanguageModel1 selectlangaugeItem = selectlanguagelist.get(position);
                                selectlangaugeItem.setStatus(1);
                                selectlanguagelist.set(position, selectlangaugeItem);
                            } else {
                                UserSelectLanguageModel1 selectlangaugeItem = selectlanguagelist.get(position);
                                selectlangaugeItem.setStatus(0);
                                selectlanguagelist.set(position, selectlangaugeItem);
                            }
                            userSelectLanguageAdapter.notifyDataSetChanged();
                        }
                    });
                } catch (Exception e) {
                    Log.e("Login Faild", e.toString());
                }
            }

            @Override
            public void onFailure(Call<LanguageUpdateResponse> call, Throwable t) {
            }
        });
    }
}