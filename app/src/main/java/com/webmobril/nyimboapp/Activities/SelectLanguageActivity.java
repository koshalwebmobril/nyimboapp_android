package com.webmobril.nyimboapp.Activities;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.webmobril.nyimboapp.Adapters.SelectLanguageAdapter;
import com.webmobril.nyimboapp.Models.SelectLanguageModel1;
import com.webmobril.nyimboapp.Response.SelectLanguageResponse;
import com.webmobril.nyimboapp.R;
import com.webmobril.nyimboapp.Sharedpreference.LoginPreferences;
import com.webmobril.nyimboapp.Utils.ProgressD;
import com.webmobril.nyimboapp.Utils.UrlApi;
import com.webmobril.nyimboapp.network.ApiInterface;

import java.util.ArrayList;
import java.util.List;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

public class SelectLanguageActivity extends AppCompatActivity
{
    RecyclerView recyclerView;
    SelectLanguageAdapter selectLanguageAdapter;
    ArrayList<SelectLanguageModel1> selectlanguagelist;
    private String strCheckedItem="";
    private RelativeLayout relative_langauge;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_language);
        init();
        SelectLanguageApi();
    }
    public void init()
    {
        selectlanguagelist = new ArrayList<SelectLanguageModel1>();
        recyclerView=findViewById(R.id.recyclerView);
        relative_langauge=findViewById(R.id.relative_artist);

        relative_langauge.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                if(!TextUtils.isEmpty(strCheckedItem))
                {
                      Intent i=new Intent(SelectLanguageActivity.this,ArtistListActivity.class);
                      i.putExtra("language_list",strCheckedItem);
                      LoginPreferences.getActiveInstance(SelectLanguageActivity.this).setSelectedLanguagestatus(strCheckedItem);
                      startActivity(i);
                }
                else
                {
                    Toast.makeText(SelectLanguageActivity.this, "Please select language", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void SelectLanguageApi()
    {
        final ProgressD progressDialog = ProgressD.show(SelectLanguageActivity.this,getResources().getString(R.string.logging_in), true, false, null);
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();
        Gson gson = new GsonBuilder()
                .setLenient()
                .create();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(UrlApi.BASE_URL)
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(client)
                .build();
        ApiInterface service = retrofit.create(ApiInterface.class);
        Call<SelectLanguageResponse> call = service.GetLanguage();
        call.enqueue(new Callback<SelectLanguageResponse>()
        {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onResponse(Call<SelectLanguageResponse> call, retrofit2.Response<SelectLanguageResponse> response)
            {
                progressDialog.dismiss();
                try
                {
                    SelectLanguageResponse resultFile = response.body();
                    List<SelectLanguageModel1> selectLanguageModel1=resultFile.getSelectLanguageModel1List();
                    LinearLayoutManager gridLayoutManager = new GridLayoutManager( SelectLanguageActivity.this, 2);
                    recyclerView.setLayoutManager(gridLayoutManager);
                    selectLanguageAdapter = new SelectLanguageAdapter(SelectLanguageActivity.this, selectLanguageModel1);
                    recyclerView.setAdapter(selectLanguageAdapter);
                    String status=resultFile.getStatus();

                    if(status.equals("true"))
                    {
                        relative_langauge.setVisibility(View.VISIBLE);
                    }
                    else
                    {
                        relative_langauge.setVisibility(View.GONE);
                    }

                    selectLanguageAdapter.setCheckedItemListner(new SelectLanguageAdapter.ItemCheckListner()
                    {
                        @Override
                        public void setCheckedList(String list)
                        {
                             strCheckedItem="";
                             Log.d("checked_list",list);
                        }

                        @Override
                        public void remove(SelectLanguageModel1 selectlangaugeItem)
                        {
                            if(selectlangaugeItem.isSelected())
                            {
                                if(strCheckedItem.equals(""))
                                {
                                    strCheckedItem=""+selectlangaugeItem.getId();
                                }
                                else
                                {
                                    strCheckedItem=strCheckedItem+","+selectlangaugeItem.getId();
                                    Log.d("checked_list1", strCheckedItem);
                                }
                            }
                        }
                    });
                }
                catch (Exception e)
                {
                    Log.e("Login Faild", e.toString());
                }
            }

            @Override
            public void onFailure(Call<SelectLanguageResponse> call, Throwable t) { }
        });
    }
}