package com.webmobril.nyimboapp.Activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import com.webmobril.nyimboapp.Adapters.AlbumListingAdapter;
import com.webmobril.nyimboapp.Models.MusicListModel;
import com.webmobril.nyimboapp.R;

import java.util.ArrayList;

public class AlbumListingActivity extends AppCompatActivity implements View.OnClickListener
{
    RecyclerView recyclerview_album_list;
    AlbumListingAdapter albumListAdapter;
    ArrayList<MusicListModel> musiclist;
    ImageView imgback;
    ImageView abcd;
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_album_listing);
        init();
        GetData();
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(this,RecyclerView.VERTICAL,false);
        recyclerview_album_list.setLayoutManager(mLayoutManager);
        albumListAdapter = new AlbumListingAdapter(AlbumListingActivity.this,musiclist);
        recyclerview_album_list.setAdapter(albumListAdapter);
    }

    private void init()
    {
        musiclist = new ArrayList<MusicListModel>();
        recyclerview_album_list=findViewById(R.id.recyclerview_album_list);
        imgback=findViewById(R.id.imgback);
        imgback.setOnClickListener(this);
    }

    public void GetData()
    {
        MusicListModel restaurentModel1 = new MusicListModel(R.drawable.arjit_singh,"Perfect");
        musiclist.add(restaurentModel1);

        MusicListModel restaurentModel2 = new MusicListModel(R.drawable.arjit_singh,"Shap of You");
        musiclist.add(restaurentModel2);

        MusicListModel restaurentModel3 = new MusicListModel(R.drawable.arjit_singh,"Perfect");
        musiclist.add(restaurentModel3);
    }

    @Override
    public void onClick(View v)
    {
        switch (v.getId())
        {
            case R.id.imgback:
                finish();
                break;
        }
    }
}