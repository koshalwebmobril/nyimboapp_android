package com.webmobril.nyimboapp.Activities;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.view.GravityCompat;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.facebook.login.LoginManager;
import com.webmobril.nyimboapp.R;
import com.webmobril.nyimboapp.Sharedpreference.LoginPreferences;
import com.webmobril.nyimboapp.SplashActivity;
import com.webmobril.nyimboapp.config.Constant;


import com.webmobril.nyimboapp.databinding.ActivityMainBinding;
import com.webmobril.nyimboapp.fragments.HomeFragment;
import com.webmobril.nyimboapp.fragments.ProfileFragment;

public class MainActivity extends AppCompatActivity implements View.OnClickListener
{
    ActivityMainBinding binding;
    private FragmentTransaction ft;
    private Fragment currentFragment;
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main);
        setSupportActionBar(binding.layoutContent.toolbar.toolbarMain);
        init();
        loadHome("0");
    }

    private void init()
    {
        binding.layoutContent.toolbar.imgMenu.setOnClickListener(this);
        binding.layoutContent.toolbar.imgBack.setOnClickListener(this);
       // binding.layoutContent.toolbar.imgbackOther

        binding.drawerMenuItems.linearProfile.setOnClickListener(this);
        binding.drawerMenuItems.linearLibrary.setOnClickListener(this);
        binding.drawerMenuItems.linearLanguage.setOnClickListener(this);
        binding.drawerMenuItems.linearSubcription.setOnClickListener(this);
        binding.drawerMenuItems.linearLogout.setOnClickListener(this);
    }

    @Override
    public void onClick(View v)
    {
        if (v.getId() == R.id.imgMenu)
        {
            if (!binding.drawerLayout.isDrawerOpen(GravityCompat.START))
                binding.drawerLayout.openDrawer(GravityCompat.START);
            else binding.drawerLayout.closeDrawer(GravityCompat.START);
        }
        if (v.getId() == R.id.imgBack)
        {
            onBackPressed();
        }
        if (v.getId() == R.id.linear_profile)
        {
            binding.drawerLayout.closeDrawer(GravityCompat.START);
            loadFragmentOther(new ProfileFragment(), "");
        }

        if (v.getId() == R.id.linear_library)
        {
            binding.drawerLayout.closeDrawer(GravityCompat.START);
            //loadFragmentOther(new LibreryMainFragment(), "");
            Intent i=new Intent(MainActivity.this, LibraryMainActivity.class);
            startActivity(i);
        }

        if (v.getId() == R.id.linear_language)
        {
            binding.drawerLayout.closeDrawer(GravityCompat.START);
           Intent i=new Intent(this,UserSelectLanguageActivity.class);
           startActivity(i);
        }

        if (v.getId() == R.id.linear_subcription)
        {
            binding.drawerLayout.closeDrawer(GravityCompat.START);
            Intent i=new Intent(this,SubriptionActivity.class);
            startActivity(i);
        }

        if(v.getId() == R.id.linear_logout)
        {
            new AlertDialog.Builder(this)
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .setTitle(getString(R.string.really_exit))
                    .setMessage(getString(R.string.are_sure_logout))
                    .setPositiveButton(getString(R.string.yes), new DialogInterface.OnClickListener()
                    {
                        @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
                        @Override
                        public void onClick(DialogInterface dialog, int which)
                        {
                            Intent i=new Intent (MainActivity.this,LoginActivity.class);
                            startActivity(i);
                            LoginPreferences.deleteAllPreference();
                            LoginManager.getInstance().logOut();
                        }
                    })
                    .setNegativeButton(getString(R.string.no), null).show();
        }

    }

    private void loadFragmentOther(Fragment fragment, String type)
    {
        Bundle bundle = new Bundle();
        bundle.putString(Constant.FRAGMENT_TYPE, type);
        ft = getSupportFragmentManager().beginTransaction();
        ft.setCustomAnimations(R.anim.right_in, R.anim.left_out, R.anim.left_in, R.anim.right_out);
        ft.replace(R.id.frameMain, fragment);
        fragment.setArguments(bundle);
        ft.addToBackStack(null);
        ft.commit();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        getMenuInflater().inflate(R.menu.option_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        int id = item.getItemId();
        switch (id) {

            /*case R.id.actionBell:
                loadFragmentOther(new FilterFragment(), "no");
                Toast.makeText(getApplicationContext(), "Item 1 Selected", Toast.LENGTH_LONG).show();
                return true;*/

            default:
                return super.onOptionsItemSelected(item);
        }
    }


    @Override
    public void onBackPressed()
    {
        if (binding.drawerLayout.isDrawerOpen(GravityCompat.START))
        {
            binding.drawerLayout.closeDrawer(GravityCompat.START);
        }
        else if (getSupportFragmentManager().getBackStackEntryCount() > 0)
        {
            getSupportFragmentManager().popBackStack();
        }
        else if (getSupportFragmentManager().getBackStackEntryCount() == 0)
        {
            new AlertDialog.Builder(this)
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .setTitle(getString(R.string.really_exit))
                    .setMessage(getString(R.string.are_sure_exit))
                    .setPositiveButton(getString(R.string.yes), new DialogInterface.OnClickListener()
                    {
                        @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
                        @Override
                        public void onClick(DialogInterface dialog, int which)
                        {
                            MainActivity.this.finishAffinity();
                        }
                    })
                    .setNegativeButton(getString(R.string.no), null).show();
        }
    }

    public void toolbarOther(String title)
    {
        binding.layoutContent.toolbar.lnMain.setVisibility(View.GONE);
        binding.layoutContent.toolbar.lnOtherTool.setVisibility(View.VISIBLE);
        binding.layoutContent.toolbar.txtTitle.setText(title);
    }

    public void toolbarHome()
    {
        binding.layoutContent.toolbar.lnMain.setVisibility(View.VISIBLE);
        binding.layoutContent.toolbar.lnOtherTool.setVisibility(View.GONE);
    }


    private void loadHome(String anim)
    {
        ft = getSupportFragmentManager().beginTransaction();
        currentFragment = new HomeFragment();
        if (anim.equals("1"))
        {
            ft.setCustomAnimations(R.anim.left_in, R.anim.right_out);
        }
        ft.replace(R.id.frameMain, currentFragment);
        ft.commit();
    }
}