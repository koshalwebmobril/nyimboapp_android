package com.webmobril.nyimboapp.Activities;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.stripe.android.ApiResultCallback;
import com.stripe.android.Stripe;
import com.stripe.android.model.Card;
import com.stripe.android.model.Token;
import com.webmobril.nyimboapp.R;
import com.webmobril.nyimboapp.Response.ResultFileResponse;
import com.webmobril.nyimboapp.Utils.CommonMethod;
import com.webmobril.nyimboapp.Utils.ProgressD;
import com.webmobril.nyimboapp.Utils.UrlApi;
import com.webmobril.nyimboapp.network.ApiInterface;

import org.jetbrains.annotations.NotNull;

import java.util.StringTokenizer;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

import static com.webmobril.nyimboapp.Utils.CommonMethod.isValidEmaillId;

public class CardPaymentActivity extends AppCompatActivity implements View.OnClickListener {
    Button saveBtn;
    EditText cardHolderNameET, cardNumberET, expiryDateET, cvvET;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_card_payment);
        init();
        expiryDateET.addTextChangedListener(new com.webmobril.buzzbutler.view.utils.ExpiryDateTextWatcher());
    }

    public void init()
    {
        saveBtn = findViewById(R.id.saveBtn);
        cardHolderNameET = findViewById(R.id.cardHolderNameET);
        cardNumberET = findViewById(R.id.cardNumberET);
        expiryDateET = findViewById(R.id.expiryDateET);
        cvvET = findViewById(R.id.cvvET);
        saveBtn.setOnClickListener(this);
    }

    private boolean isValid(boolean validation, Card card)
    {
        if (validation) {
            return true;
        } else if (!card.validateNumber()) {
            Toast.makeText(this, "The card number that you entered is invalid.", Toast.LENGTH_SHORT).show();
        } else if (!card.validateExpiryDate()) {
            Toast.makeText(this, "The expiration date that you entered is invalid.", Toast.LENGTH_SHORT).show();
        } else if (!card.validateCVC()) {
            Toast.makeText(this, "The CVV code that you entered is invalid.", Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(this, "The card details that you entered are invalid.", Toast.LENGTH_SHORT).show();
        }
        return false;
    }



    private boolean valid(String cardName, String cardNumber, String expiryDate, String cvv) {
        if (TextUtils.isEmpty(cardName)) {
            cardHolderNameET.requestFocus();
            Toast.makeText(this, "err_card_holder_name_empty", Toast.LENGTH_SHORT).show();
            return false;
        } else if (TextUtils.isEmpty(cardNumber)) {
            cardNumberET.requestFocus();
            Toast.makeText(this, "err_card_number_empty", Toast.LENGTH_SHORT).show();
            return false;
        } else if (cardNumber.length() < 16) {
            cardNumberET.requestFocus();
            Toast.makeText(this, "err_cardnumber_invalid", Toast.LENGTH_SHORT).show();
            return false;
        } else if (TextUtils.isEmpty(expiryDate)) {
            expiryDateET.requestFocus();
            Toast.makeText(this, "err_expiry_date_empty", Toast.LENGTH_SHORT).show();
            return false;
        } else if (expiryDate.length() < 4) {
            expiryDateET.requestFocus();
            Toast.makeText(this, "err_expiry_date_invalid", Toast.LENGTH_SHORT).show();
            return false;
        } else if (Integer.parseInt(expiryDate.substring(0, 2)) > 12) {
            expiryDateET.requestFocus();
            Toast.makeText(this, "err_expiry_date_invalid", Toast.LENGTH_SHORT).show();
            return false;
        } else if (TextUtils.isEmpty(cvv)) {
            cvvET.requestFocus();
            Toast.makeText(this, "err_cvv_empty", Toast.LENGTH_SHORT).show();
            return false;
        } else if (cvv.length() < 3) {
            cvvET.requestFocus();
            Toast.makeText(this, "err_cvv_invalid", Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }

    @Override
    public void onClick(View v)
    {
        String cardName = cardHolderNameET.getText().toString().trim();
        String cardNumber = cardNumberET.getText().toString().trim().replace(" ", "");
        String expiryDate = expiryDateET.getText().toString().trim();
        String cvv = cvvET.getText().toString().trim();
        if (valid(cardName, cardNumber, expiryDate, cvv))
        {
            if (CommonMethod.isOnline(this))
            {
                String expiryMonth = expiryDate.substring(0, 2);
                String expiryYear = expiryDate.substring(2);
                StringTokenizer tokens = new StringTokenizer(expiryMonth + expiryYear, "/");
                Integer cardExpMonth = Integer.parseInt(tokens.nextToken());

                Card card = new Card.Builder(cardNumber, Integer.valueOf(expiryMonth), Integer.valueOf(expiryYear), cvv)
                        .name(cardName)
                        .build();
                boolean validation = card.validateCard();
                if(isValid(validation, card))
                {
                   // final ProgressD progressDialog = ProgressD.show(CardPaymentActivity.this, getResources().getString(R.string.logging_in), true, false, null);
                    //  hitsavecardapi();
                       Toast.makeText(this, "success", Toast.LENGTH_SHORT).show();
                       hideKeyboard(v);

                    /*Stripe stripe = new Stripe(CardPaymentActivity.this, STRIPEAPIKEY);
                    stripe.createCardToken(card, new ApiResultCallback<Token>()
                    {
                        @Override
                        public void onSuccess(Token token)
                        {
                           // saveCardDetails(token);
                            Log.d("carddetails", String.valueOf(token));
                            progressDialog.dismiss();
                        }
                        @Override
                        public void onError(@NotNull Exception e)
                        {
                            progressDialog.dismiss();
                        }
                    });*/
                }
                else
                    {
                        CommonMethod.showAlert(getString(R.string.check_internet), this);
                   }
            }
        }
    }


    /*private void hitsavecardapi()
    {
        final ProgressD progressDialog = ProgressD.show(CardPaymentActivity.this,getResources().getString(R.string.logging_in), true, false, null);

        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();
        Gson gson = new GsonBuilder()
                .setLenient()
                .create();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(UrlApi.BASE_URL)
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(client)
                .build();
        ApiInterface service = retrofit.create(ApiInterface.class);
        Call<ResultFileResponse> call = service.forgotpassword(email);
        call.enqueue(new Callback<ResultFileResponse>()
        {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onResponse(Call<ResultFileResponse> call, retrofit2.Response<ResultFileResponse> response)
            {
                try
                {
                    ResultFileResponse resultFileResponse = response.body();
                    String status = resultFileResponse.getStatus();
                    String message = resultFileResponse.getMessage();
                    progressDialog.dismiss();

                    Toast.makeText(ForgotPasswordActivity.this, message, Toast.LENGTH_SHORT).show();

                    if(status.equals("true"))
                    {
                        Intent i=new Intent(ForgotPasswordActivity.this,VerificationCodeActivity.class);
                        i.putExtra("email",email);
                        startActivity(i);
                    }
                    else
                    { }
                }
                catch (Exception e)
                {
                    Log.e("Login Faild", e.toString());
                    Toast.makeText(ForgotPasswordActivity.this, "Failed" , Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ResultFileResponse> call, Throwable t)
            {

                Toast.makeText(ForgotPasswordActivity.this, "Failed" + t, Toast.LENGTH_SHORT).show();
            }
        });
    }*/
    public void hideKeyboard(View view)
    {
        try {
            InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        } catch(Exception ignored) {
        }
    }

}