package com.webmobril.nyimboapp.Activities;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.webmobril.nyimboapp.R;
import com.webmobril.nyimboapp.Response.ResultFileResponse;
import com.webmobril.nyimboapp.Sharedpreference.LoginPreferences;
import com.webmobril.nyimboapp.Utils.CommonMethod;
import com.webmobril.nyimboapp.Utils.ProgressD;
import com.webmobril.nyimboapp.Utils.UrlApi;
import com.webmobril.nyimboapp.network.ApiInterface;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener
{
    LinearLayout linear_register,linear_login;
    RelativeLayout relative_facebook;
    TextView forgot_textview;
    TextView username,password;
    private CallbackManager callbackManager;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        callbackManager = CallbackManager.Factory.create();
        init();
    }
    public void init()
    {
        relative_facebook = findViewById(R.id.relative_facebook);
        linear_register = findViewById(R.id.linear_register);
        forgot_textview = findViewById(R.id.forgot_textview);
        linear_login=findViewById(R.id.linear_login);
        username =findViewById(R.id.username);
        password=findViewById(R.id.password);

        linear_register.setOnClickListener(this);
        linear_login.setOnClickListener(this);
        forgot_textview.setOnClickListener(this);
        relative_facebook.setOnClickListener(this);
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        callbackManager.onActivityResult(requestCode, resultCode, data);
        super.onActivityResult(requestCode, resultCode, data);
    }
    @Override
    public void onClick(View v)
    {
        switch (v.getId())
        {
            case R.id.linear_login:
                if (CommonMethod.isOnline(this))
                {
                    hideKeyboard((LinearLayout)v);
                    if(validation())
                    {
                        LoginUser(username.getText().toString().trim(),password.getText().toString().trim(),"1","fdfdf");
                    }
                }
                else
                    {
                      CommonMethod.showAlert(getString(R.string.check_internet), this);
                    }
                break;



            case R.id.linear_register:
                Intent i=new Intent(this,RegisterActivity.class);
                startActivity(i);
                break;

            case R.id.forgot_textview:
                Intent forgot=new Intent(this,ForgotPasswordActivity.class);
                startActivity(forgot);
                break;
            case R.id.relative_facebook:
                LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList("email","public_profile"));

                LoginManager.getInstance().registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(LoginResult loginResult)
                    {
                        Log.d("MainActivity", "Facebook token: " + loginResult.getAccessToken().getToken());
                        GraphRequest data_request = GraphRequest.newMeRequest(
                                loginResult.getAccessToken(), (json_object, response) -> {
                                    try
                                    {
                                        Log.e("response","=="+response);
                                        JSONObject profile_pic_data, profile_pic_url;
                                        String strFbId=json_object.getString("id");
                                        String strFbName=json_object.getString("name");

                                        //  String strFbEmail=json_object.getString("email");
                                        profile_pic_data = new JSONObject(json_object.get("picture").toString());
                                        profile_pic_url = new JSONObject(profile_pic_data.getString("data"));
                                        String strFbProfilePic= profile_pic_url.getString("url");
                                        LoginManager.getInstance().logOut();

                                        hitfbLoginApi(strFbId,strFbName,"","1","1","dsdsf");
                                        //String email = user.getProperty("email").toString();
                                    }
                                    catch (JSONException e)
                                    {
                                        e.printStackTrace();
                                    }

                                });
                        Bundle permission_param = new Bundle();
                        permission_param.putString("fields", "id,name,email,picture.width(120).height(120)");
                        data_request.setParameters(permission_param);
                        data_request.executeAsync();

                    }

                    @Override
                    public void onCancel() {
                        Log.d("MainActivity", "Facebook onCancel.");

                    }

                    @Override
                    public void onError(FacebookException exception) {
                        Log.d("MainActivity", "Facebook onError.");

                    }
                });
                break;
        }
    }
    public void hideKeyboard(View view)
    {
        try {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        } catch(Exception ignored)
        {
        }
    }

    private boolean validation()
    {
        if(TextUtils.isEmpty(username.getText().toString().trim()))
        {
            Toast.makeText(this, "Please Enter User Name", Toast.LENGTH_SHORT).show();
            return false;
        }
        else if (username.getText().toString().matches("[0-9]+"))
        {
            Toast.makeText(this, "Enter Valid User name", Toast.LENGTH_SHORT).show();
            return false;
        }
        else if (String.valueOf(username.getText().toString().charAt(0)).matches("[0-9]+"))
        {
            Toast.makeText(this, "Enter Valid User name", Toast.LENGTH_SHORT).show();
            return false;
        }
        else if (String.valueOf(username.getText().toString().charAt(0)).matches("[$&+,:;=\\\\\\\\?@#|/'<>.^*()%!-]"))
        {
            Toast.makeText(this, "Enter Valid User name", Toast.LENGTH_SHORT).show();
            return false;
        }
        else if(TextUtils.isEmpty(password.getText().toString().trim()))
        {
            Toast.makeText(this, "Please Enter Password", Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }

    private void LoginUser(String username,String password,String device_type,String device_token)
    {
        final ProgressD progressDialog = ProgressD.show(LoginActivity.this,getResources().getString(R.string.logging_in), true, false, null);

        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();
        Gson gson = new GsonBuilder()
                .setLenient()
                .create();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(UrlApi.BASE_URL)
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(client)
                .build();
        ApiInterface service = retrofit.create(ApiInterface.class);

        Call<ResultFileResponse> call = service.loginUser(username,password,device_type,device_token);
        call.enqueue(new Callback<ResultFileResponse>()
        {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onResponse(Call<ResultFileResponse> call, retrofit2.Response<ResultFileResponse> response)
            {
                progressDialog.dismiss();
                try
                {
                    ResultFileResponse resultFileResponse = response.body();
                    String status = resultFileResponse.getStatus();
                    String message = resultFileResponse.getMessage();

                    Toast.makeText(LoginActivity.this, message, Toast.LENGTH_SHORT).show();
                    LoginPreferences.getActiveInstance(LoginActivity.this).setToken(resultFileResponse.getAuthenticationModel().getToken());
                    LoginPreferences.getActiveInstance(LoginActivity.this).setUser_id(resultFileResponse.getAuthenticationModel().getId());
                    LoginPreferences.getActiveInstance(LoginActivity.this).setSelectedLanguagestatus(String.valueOf(resultFileResponse.getAuthenticationModel().getLanguage_status()));
                    LoginPreferences.getActiveInstance(LoginActivity.this).setSelectedLanguagestatus(String.valueOf(resultFileResponse.getAuthenticationModel().getArtist_status()));
                    if(status.equals("true"))
                    {
                        if(resultFileResponse.getAuthenticationModel().getLanguage_status()== 0  ||  resultFileResponse.getAuthenticationModel().getArtist_status() == 0 )
                        {
                            Intent i=new Intent(LoginActivity.this,SelectLanguageActivity.class);
                            startActivity(i);
                        }
                        else
                        {
                            Intent i=new Intent(LoginActivity.this,MainActivity.class);
                            startActivity(i);
                        }
                    }
                    else
                        { }
                }
                catch (Exception e)
                {
                    Log.e("Login Faild", e.toString());
                }
            }

            @Override
            public void onFailure(Call<ResultFileResponse> call, Throwable t)
            {

                Toast.makeText(LoginActivity.this, "Failed" + t, Toast.LENGTH_SHORT).show();
            }
        });
    }


    private void hitfbLoginApi(String strFbId,String username,String strFbEmail,String social_id,String device_type,String device_token)
    {
        final ProgressD progressDialog = ProgressD.show(LoginActivity.this,getResources().getString(R.string.logging_in), true, false, null);
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();
        Gson gson = new GsonBuilder()
                .setLenient()
                .create();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(UrlApi.BASE_URL)
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(client)
                .build();
        ApiInterface service = retrofit.create(ApiInterface.class);

        Call<ResultFileResponse> call = service.loginUserSocial(strFbId,username,strFbEmail,social_id,device_type,device_token);
        call.enqueue(new Callback<ResultFileResponse>()
        {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onResponse(Call<ResultFileResponse> call, retrofit2.Response<ResultFileResponse> response)
            {
                progressDialog.dismiss();
                try
                {
                    ResultFileResponse resultFileResponse = response.body();
                    String status = resultFileResponse.getStatus();
                    String message = resultFileResponse.getMessage();

                    Toast.makeText(LoginActivity.this, message, Toast.LENGTH_SHORT).show();
                    LoginPreferences.getActiveInstance(LoginActivity.this).setToken(resultFileResponse.getAuthenticationModel().getToken());
                    LoginPreferences.getActiveInstance(LoginActivity.this).setUser_id(resultFileResponse.getAuthenticationModel().getId());

                    if(status.equals("true"))
                    {
                        if(resultFileResponse.getAuthenticationModel().getLanguage_status() == 0  ||  resultFileResponse.getAuthenticationModel().getArtist_status() == 0 )
                        {
                            Intent i=new Intent(LoginActivity.this,SelectLanguageActivity.class);
                            startActivity(i);
                        }
                        else
                        {
                            Intent i=new Intent(LoginActivity.this,MainActivity.class);
                            startActivity(i);
                        }
                    }
                    else
                    {
                       // LoginManager.getInstance().logOut();
                    }
                }
                catch (Exception e)
                {
                    Log.e("Login Faild", e.toString());
                  //  LoginManager.getInstance().logOut();
                }
            }

            @Override
            public void onFailure(Call<ResultFileResponse> call, Throwable t)
            {
                Toast.makeText(LoginActivity.this, "Failed" + t, Toast.LENGTH_SHORT).show();
               // LoginManager.getInstance().logOut();
            }
        });
    }
    @Override
    public void onBackPressed()
    {
        finishAffinity();
        finish();
    }
    @Override
    protected void onResume() {
        super.onResume();
        username.setText("");
        password.setText("");
    }
}
