package com.webmobril.nyimboapp.Activities;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Rect;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.squareup.picasso.Picasso;
import com.webmobril.nyimboapp.Adapters.MusicListingAdapter;
import com.webmobril.nyimboapp.Adapters.userplaylistadapterAdapter;
import com.webmobril.nyimboapp.Interface.OnClick;

import com.webmobril.nyimboapp.Interface.OnClickLikeDislike;
import com.webmobril.nyimboapp.Interface.PlaylistClick;

import com.webmobril.nyimboapp.Models.GetAllSongListModel;
import com.webmobril.nyimboapp.Models.GetPlaylistModel;
import com.webmobril.nyimboapp.R;
import com.webmobril.nyimboapp.Response.CreatePlaylistResponse;
import com.webmobril.nyimboapp.Response.GetAllSongListResponse;
import com.webmobril.nyimboapp.Response.GetPlayListlistsResponse;
import com.webmobril.nyimboapp.Response.GetPlaylistDownloadListResponse;
import com.webmobril.nyimboapp.Response.SetSongPlaylistResponse;
import com.webmobril.nyimboapp.Response.UserLikeUnlikeResponse;
import com.webmobril.nyimboapp.Sharedpreference.LoginPreferences;
import com.webmobril.nyimboapp.Utils.ProgressD;
import com.webmobril.nyimboapp.Utils.UrlApi;
import com.webmobril.nyimboapp.network.ApiInterface;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

public class MusicListingActivity extends AppCompatActivity implements View.OnClickListener,OnClick,OnClickLikeDislike ,PlaylistClick{
    RecyclerView recyclerview_music_list,recyclerview_playlist;
    MusicListingAdapter musicListAdapter;
    userplaylistadapterAdapter userplaylistadpter;
    ImageView imgback;
    String album_id, Artist_image;
    ImageView img;
    String userselectedId;
    TextView no_music;
    ImageView cross_icon;
    String songId;
    OnClick onClick;
    OnClickLikeDislike onclicklikedislike;
    PlaylistClick playlistClick;
    BottomSheetDialog dialog;
    LinearLayout linear_create_new_playlist;
    Dialog dialog_alert;
    List<GetAllSongListModel> getalbumsonglist;
    String status_click;
    String playlist_id;
    String liked_status;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_music_listing);
        onClick=this;
        playlistClick=this;
        onclicklikedislike=this;
        init();
        status_click=getIntent().getStringExtra("status_click");
        if(status_click.equals("playlist"))
        {
            playlist_id = getIntent().getStringExtra("playlist_id");
            hitPlaylistMusicListingApi();
        }
        else if(status_click.equals("liked_song"))
        {
            hitMusicListLikedSong();
            liked_status=getIntent().getStringExtra("liked_status");
        }
        else
        {
            album_id = getIntent().getStringExtra("album_id");
            Artist_image = getIntent().getStringExtra("artist_image");
            userselectedId = getIntent().getStringExtra("selected_id");

            hitMusicListingApi();
        }
    }
    private void init()
    {
        recyclerview_music_list = findViewById(R.id.recyclerview_music_list);
        imgback = findViewById(R.id.imgback);
        imgback.setOnClickListener(this);

        img = findViewById(R.id.singer_image);
        no_music = findViewById(R.id.no_music);
    }

    @Override
    public void onClick(View v)
    {
        switch (v.getId())
        {
            case R.id.imgback:
                finish();
                break;
        }
    }

    private void hitPlaylistMusicListingApi()
    {
        final ProgressD progressDialog = ProgressD.show(this, getResources().getString(R.string.logging_in), true, false, null);
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();
        Gson gson = new GsonBuilder()
                .setLenient()
                .create();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(UrlApi.BASE_URL)
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(client)
                .build();
        ApiInterface service = retrofit.create(ApiInterface.class);
        Map<String, String> params = new HashMap<>();
        params.put("user_id",LoginPreferences.getActiveInstance(this).getUser_id());
        params.put("playlist_id", playlist_id);

        Call<GetAllSongListResponse> call = service.getuserplaylistsonglist(params);
        call.enqueue(new Callback<GetAllSongListResponse>() {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onResponse(Call<GetAllSongListResponse> call, retrofit2.Response<GetAllSongListResponse> response) {
                progressDialog.dismiss();
                try {
                    GetAllSongListResponse resultFile = response.body();
                    if (resultFile.getStatus().equals("true"))
                    {
                        getalbumsonglist = resultFile.getResult();
                        Picasso.get().load(R.drawable.logo).into(img);

                        if(getalbumsonglist.isEmpty())
                        {
                            no_music.setVisibility(View.VISIBLE);
                            recyclerview_music_list.setVisibility(View.GONE);
                        }
                        else
                        {
                            no_music.setVisibility(View.GONE);
                            LinearLayoutManager mLayoutManager = new LinearLayoutManager(MusicListingActivity.this, RecyclerView.VERTICAL, false);
                            recyclerview_music_list.setLayoutManager(mLayoutManager);
                            musicListAdapter = new MusicListingAdapter(MusicListingActivity.this, (ArrayList) getalbumsonglist, onClick,onclicklikedislike);
                            recyclerview_music_list.setAdapter(musicListAdapter);
                        }
                    }
                    else
                    {
                    }
                } catch (Exception e) {
                    Log.e("Login Faild", e.toString());
                }
            }

            @Override
            public void onFailure(Call<GetAllSongListResponse> call, Throwable t) {
            }
        });
    }

    private void  hitMusicListLikedSong()
    {
        final ProgressD progressDialog = ProgressD.show(this, getResources().getString(R.string.logging_in), true, false, null);
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();
        Gson gson = new GsonBuilder()
                .setLenient()
                .create();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(UrlApi.BASE_URL)
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(client)
                .build();
        ApiInterface service = retrofit.create(ApiInterface.class);
        Map<String, String> params = new HashMap<>();
        params.put("user_id",LoginPreferences.getActiveInstance(this).getUser_id());
        Call<GetAllSongListResponse> call = service.likedsonglist(params);
        call.enqueue(new Callback<GetAllSongListResponse>() {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onResponse(Call<GetAllSongListResponse> call, retrofit2.Response<GetAllSongListResponse> response) {
                progressDialog.dismiss();
                try {
                    GetAllSongListResponse resultFile = response.body();
                    if (resultFile.getStatus().equals("true"))
                    {
                        getalbumsonglist = resultFile.getResult();
                        Picasso.get().load(R.drawable.logo).into(img);

                        if(getalbumsonglist.isEmpty())
                        {
                            no_music.setVisibility(View.VISIBLE);
                            recyclerview_music_list.setVisibility(View.GONE);
                        }
                        else
                        {
                            no_music.setVisibility(View.GONE);
                            LinearLayoutManager mLayoutManager = new LinearLayoutManager(MusicListingActivity.this, RecyclerView.VERTICAL, false);
                            recyclerview_music_list.setLayoutManager(mLayoutManager);
                            musicListAdapter = new MusicListingAdapter(MusicListingActivity.this, (ArrayList) getalbumsonglist, onClick,onclicklikedislike);
                            recyclerview_music_list.setAdapter(musicListAdapter);
                        }
                    }
                    else
                    {
                    }
                } catch (Exception e) {
                    Log.e("Login Faild", e.toString());
                }
            }

            @Override
            public void onFailure(Call<GetAllSongListResponse> call, Throwable t) {
            }
        });
    }

    private void hitMusicListingApi()
    {
        final ProgressD progressDialog = ProgressD.show(this, getResources().getString(R.string.logging_in), true, false, null);
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();
        Gson gson = new GsonBuilder()
                .setLenient()
                .create();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(UrlApi.BASE_URL)
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(client)
                .build();
        ApiInterface service = retrofit.create(ApiInterface.class);
        Map<String, String> params = new HashMap<>();
        params.put("user_id",LoginPreferences.getActiveInstance(this).getUser_id());
        params.put(userselectedId, album_id);

        Call<GetAllSongListResponse> call = service.getAlbumSonglist(params);
        call.enqueue(new Callback<GetAllSongListResponse>() {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onResponse(Call<GetAllSongListResponse> call, retrofit2.Response<GetAllSongListResponse> response) {
                progressDialog.dismiss();
                try {
                    GetAllSongListResponse resultFile = response.body();
                    if (resultFile.getStatus().equals("true"))
                    {
                        getalbumsonglist = resultFile.getResult();
                        Picasso.get().load(Artist_image).into(img);
                        if(getalbumsonglist.isEmpty())
                        {
                            no_music.setVisibility(View.VISIBLE);
                            recyclerview_music_list.setVisibility(View.GONE);
                        }
                        else
                        {
                            no_music.setVisibility(View.GONE);
                            LinearLayoutManager mLayoutManager = new LinearLayoutManager(MusicListingActivity.this, RecyclerView.VERTICAL, false);
                            recyclerview_music_list.setLayoutManager(mLayoutManager);
                            musicListAdapter = new MusicListingAdapter(MusicListingActivity.this, (ArrayList) getalbumsonglist, onClick,onclicklikedislike);
                            recyclerview_music_list.setAdapter(musicListAdapter);
                        }
                    }
                    else {
                    }
                } catch (Exception e) {
                    Log.e("Login Faild", e.toString());
                }
            }

            @Override
            public void onFailure(Call<GetAllSongListResponse> call, Throwable t) {
            }
        });
    }


    private void  hitgetuserPlaylistApi()
    {
        final ProgressD progressDialog = ProgressD.show(this, getResources().getString(R.string.logging_in), true, false, null);
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();
        Gson gson = new GsonBuilder()
                .setLenient()
                .create();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(UrlApi.BASE_URL)
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(client)
                .build();
        ApiInterface service = retrofit.create(ApiInterface.class);
        Call<GetPlaylistDownloadListResponse> call = service.getplaylist(LoginPreferences.getActiveInstance(this).getUser_id());
        call.enqueue(new Callback<GetPlaylistDownloadListResponse>()
        {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onResponse(Call<GetPlaylistDownloadListResponse> call, retrofit2.Response<GetPlaylistDownloadListResponse> response) {
                progressDialog.dismiss();
                try {
                    GetPlaylistDownloadListResponse resultFile = response.body();

                    if (resultFile.getStatus().equals("true"))
                    {
                        List<GetPlaylistModel> getPlaylistModels  =resultFile.getGetPlayListResponselist().getGetPlaylists();
                        openplaylistbottombar(getPlaylistModels);
                    }
                    else
                        { }
                } catch (Exception e) {
                    Log.e("Login Faild", e.toString());
                }
            }

            @Override
            public void onFailure(Call<GetPlaylistDownloadListResponse> call, Throwable t) {
            }
        });
    }

    private void openplaylistbottombar(List<GetPlaylistModel> getplaylistmodellist)
    {
        Rect displayRectangle = new Rect();
        Window window = this.getWindow();
        window.getDecorView().getWindowVisibleDisplayFrame(displayRectangle);
        dialog = new BottomSheetDialog(this);
        LayoutInflater inflater = (LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View layout = inflater.inflate(R.layout.item_playlist_bottom_bar, null);
        //layout.setMinimumWidth((int) (displayRectangle.width() * 0.9f));
        layout.setMinimumHeight((int) (displayRectangle.height() * 0.50f));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE); //before
        dialog.setContentView(layout);

        recyclerview_playlist=dialog.findViewById(R.id.recyclerview_playlist);
        cross_icon=dialog.findViewById(R.id.cross_icon);
        linear_create_new_playlist=dialog.findViewById(R.id.linear_create_new_playlist);

        LinearLayoutManager mLayoutManager = new LinearLayoutManager(MusicListingActivity.this, RecyclerView.VERTICAL, false);
        recyclerview_playlist.setLayoutManager(mLayoutManager);
        userplaylistadpter = new userplaylistadapterAdapter(MusicListingActivity.this, getplaylistmodellist,playlistClick,Artist_image);
        recyclerview_playlist.setAdapter(userplaylistadpter);

        cross_icon.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                 dialog.dismiss();
            }
        });

        linear_create_new_playlist.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                dialog.dismiss();
                openAlertDailog();
            }
        });
        dialog.show();
    }
    @Override
    public void addplaylist(String songId1)
    {
        songId=songId1;
        hitgetuserPlaylistApi();
    }


    @Override
    public void playlistClick(String playlist_id)
    {
        hitsetuserSongPlaylistApi(playlist_id,songId,"1");
    }


    private void hitsetuserSongPlaylistApi(String playlistid, String songId, String status_static)
    {
        ProgressD progressDialog = ProgressD.show(this, getResources().getString(R.string.logging_in), true, false, null);
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();
        Gson gson = new GsonBuilder()
                .setLenient()
                .create();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(UrlApi.BASE_URL)
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(client)
                .build();
        ApiInterface service = retrofit.create(ApiInterface.class);
        Call<SetSongPlaylistResponse> call = service.setplaylistsong(LoginPreferences.getActiveInstance(this).getUser_id(),playlistid,songId,status_static);
        call.enqueue(new Callback<SetSongPlaylistResponse>()
        {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onResponse(Call<SetSongPlaylistResponse> call, retrofit2.Response<SetSongPlaylistResponse> response)
            {
                progressDialog.dismiss();
                try {
                    SetSongPlaylistResponse resultFile = response.body();
                    Toast.makeText(MusicListingActivity.this, resultFile.getMessage(), Toast.LENGTH_SHORT).show();
                    if (resultFile.getStatus().equals("true"))
                    {
                             dialog.dismiss();
                    }
                    else
                    { }
                } catch (Exception e) {
                    Log.e("Login Faild", e.toString());
                }
            }
            @Override
            public void onFailure(Call<SetSongPlaylistResponse> call, Throwable t)
            {
                progressDialog.dismiss();
            }
        });
    }


    private void openAlertDailog()
    {
        dialog_alert = new Dialog(MusicListingActivity.this);
        dialog_alert.requestWindowFeature(Window.FEATURE_NO_TITLE);

        dialog_alert.setContentView(R.layout.item_alert_dailog_create_playlist);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog_alert.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.gravity = Gravity.CENTER;
        dialog_alert.getWindow().setAttributes(lp);

        Button ok = dialog_alert.findViewById(R.id.ok);
        Button cancel = dialog_alert.findViewById(R.id.cancel);
        EditText create_playlist=dialog_alert.findViewById(R.id.create_playlist);


        ok.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                if(!create_playlist.getText().toString().isEmpty())
                {
                    hitCreatPlayListApi(create_playlist.getText().toString().trim());
                }
                else
                {
                    Toast.makeText(MusicListingActivity.this, "Please enter playlist name", Toast.LENGTH_SHORT).show();
                }
            }
        });

        cancel.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                dialog_alert.dismiss();
            }
        });
        dialog_alert.show();
        dialog_alert.setCanceledOnTouchOutside(false);
    }

    public void hitCreatPlayListApi(String name)
    {
        final ProgressD progressDialog = ProgressD.show(this,getResources().getString(R.string.logging_in), true, false, null);
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();
        Gson gson = new GsonBuilder()
                .setLenient()
                .create();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(UrlApi.BASE_URL)
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(client)
                .build();
        ApiInterface service = retrofit.create(ApiInterface.class);
        Call<CreatePlaylistResponse> call = service.createPlaylist(LoginPreferences.getActiveInstance(this).getUser_id(),name);
        call.enqueue(new Callback<CreatePlaylistResponse>()
        {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onResponse(Call<CreatePlaylistResponse> call, retrofit2.Response<CreatePlaylistResponse> response)
            {
                try
                {
                    CreatePlaylistResponse resultFileResponse = response.body();
                    String status = resultFileResponse.getStatus();
                    //  String message = resultFileResponse.getMessage();
                    progressDialog.dismiss();
                    if(status.equals("true"))
                    {
                        Toast.makeText(MusicListingActivity.this, "Playlist Create Successfully", Toast.LENGTH_SHORT).show();
                        dialog_alert.dismiss();
                    }
                    else
                    { }
                }
                catch (Exception e)
                {
                    Log.e("Login Faild", e.toString());
                    Toast.makeText(MusicListingActivity.this, "Failed" , Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<CreatePlaylistResponse> call, Throwable t)
            {
                Toast.makeText(MusicListingActivity.this, "Failed" + t, Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void likedislike(String song_id,int position)
    {
        GetAllSongListModel songlist=getalbumsonglist.get(position);
        String likeunlikestatus =String.valueOf(songlist.getLikeDislikeSongsCount());
        if(likeunlikestatus.equals("1"))
        {
            Toast.makeText(this, "already added in liked list!", Toast.LENGTH_SHORT).show();
        }
        else
        {
            likedislikeapihit(song_id,position,likeunlikestatus);
        }
    }
    public void likedislikeapihit(String song_id,int position,String likeunlikestatus)
    {
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();
        Gson gson = new GsonBuilder()
                .setLenient()
                .create();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(UrlApi.BASE_URL)
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(client)
                .build();
        ApiInterface service = retrofit.create(ApiInterface.class);

        Call<UserLikeUnlikeResponse> call = service.likeunlike(LoginPreferences.getActiveInstance(this).getUser_id(),song_id,likeunlikestatus);
        call.enqueue(new Callback<UserLikeUnlikeResponse>()
        {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onResponse(Call<UserLikeUnlikeResponse> call, retrofit2.Response<UserLikeUnlikeResponse> response)
            {
                try
                {
                    UserLikeUnlikeResponse resultFile = response.body();
                    Toast.makeText(MusicListingActivity.this, resultFile.getMessage(), Toast.LENGTH_SHORT).show();

                    if(resultFile.getStatus().equals("true"))
                    {
                        if(likeunlikestatus.equals("1"))
                        {
                            GetAllSongListModel songlist=getalbumsonglist.get(position);
                            songlist.setLikeDislikeSongsCount(0);
                            getalbumsonglist.set(position,songlist);
                        }
                        else
                        {
                            GetAllSongListModel songlist=getalbumsonglist.get(position);
                            songlist.setLikeDislikeSongsCount(1);
                            getalbumsonglist.set(position,songlist);
                            //  Toast.makeText(PlayerActivityAllMusicListing.this, "Dislike song", Toast.LENGTH_SHORT).show();
                        }
                    }
                    else
                    { }
                }
                catch (Exception e)
                {
                    Log.e("Login Faild", e.toString());
                }
            }
            @Override
            public void onFailure(Call<UserLikeUnlikeResponse> call, Throwable t) { }
        });
    }
}
