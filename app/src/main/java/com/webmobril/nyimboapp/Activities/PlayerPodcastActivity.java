package com.webmobril.nyimboapp.Activities;

import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.SeekBar;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.squareup.picasso.Picasso;
import com.webmobril.nyimboapp.Models.PodcastAlbumModel;
import com.webmobril.nyimboapp.Models.PodcastEpisodeItemModel;
import com.webmobril.nyimboapp.Models.TrandingSongsModel;
import com.webmobril.nyimboapp.R;
import com.webmobril.nyimboapp.Utils.Singleton;

import java.io.IOException;
import java.util.ArrayList;

public class PlayerPodcastActivity extends AppCompatActivity implements View.OnClickListener {
    ImageView imgBack, next_music, pause_icon, previous_music;
    String song_url, song_name,singer_img,artistName;
    ImageView singer_image;
    ProgressBar progressBar;
    private SeekBar playerseekbar;
    int position;

    private Handler handler = new Handler();
    MediaPlayer mediaplayer;
    private TextView textcurrentTime, texttotalDuration;
    private  TextView song_name_textview,artist_name_textview;
    ArrayList<PodcastEpisodeItemModel> audiomodellist;
    Singleton singleton=Singleton.getInstance();
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_player_podcast);
        init();
        FirstTimeMusicPlay();
    }
    private void init()
    {
        imgBack = findViewById(R.id.imgBack);
        pause_icon = findViewById(R.id.pause_icon);
        next_music = findViewById(R.id.next_music);
        previous_music = findViewById(R.id.previous_music);
        singer_image=findViewById(R.id.singer_image);
        song_name_textview=findViewById(R.id.song_name);
        artist_name_textview=findViewById(R.id.artist_name_textview);

        playerseekbar = findViewById(R.id.seekBar);
        progressBar = findViewById(R.id.progress_circular);
        pause_icon = findViewById(R.id.pause_icon);
        textcurrentTime = findViewById(R.id.textcurrentTime);
        texttotalDuration = findViewById(R.id.texttotalDuration);
        getIntentData();                          //getintent
        imgBack.setOnClickListener(this);                         //  OnClickListner
        pause_icon.setOnClickListener(this);
      //  next_music.setOnClickListener(this);
       // previous_music.setOnClickListener(this);

        Picasso.get().load(singer_img).into(singer_image);        //load singer image
        song_name_textview.setText(song_name);
        artist_name_textview.setText(artistName);


        mediaplayer = new MediaPlayer();
        playerseekbar.setMax(100);

        playerseekbar.setOnTouchListener(new View.OnTouchListener()
        {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                SeekBar seekBar = (SeekBar) v;
                int playposition = (mediaplayer.getDuration() / 100) * seekBar.getProgress();
                mediaplayer.seekTo(playposition);
                textcurrentTime.setText(millisecondstoTimer(mediaplayer.getCurrentPosition()));
                return false;
            }
        });
        mediaplayer.setOnBufferingUpdateListener(new MediaPlayer.OnBufferingUpdateListener() {
            @Override
            public void onBufferingUpdate(MediaPlayer mp, int percent) {
                playerseekbar.setSecondaryProgress(percent);
            }
        });

        mediaplayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                playerseekbar.setProgress(0);
                pause_icon.setImageResource(R.drawable.pause);
                textcurrentTime.setText("0.00");
                texttotalDuration.setText("0.00");
                mediaplayer.reset();
                preparemediaplayer();
                if(position==1)
                {

                    next_music.setAlpha(0.50f);
                    next_music.setClickable(false);
                }
                else if(position==audiomodellist.size()-1)
                {
                    next_music.setAlpha(0.50f);
                    next_music.setClickable(false);
                }
            }
        });
    }

    public void getIntentData()
    {
        song_name = getIntent().getStringExtra("song_name");
        song_url = getIntent().getStringExtra("song_url");
        singer_img=getIntent().getStringExtra("singer_image"); //music list\
        artistName=getIntent().getStringExtra("artistName");

        audiomodellist = new ArrayList<>();
      //  audiomodellist=singleton.audiomodellist;

        Bundle extras = getIntent().getExtras();
        if(extras != null)
        {
            position = extras.getInt("position");
            Log.d("position","position"+ position);
        }
        if (audiomodellist!=null&&audiomodellist.size()<=1)
        {
            previous_music.setAlpha(0.50f);
            next_music.setAlpha(0.50f);
            previous_music.setClickable(false);
            next_music.setClickable(false);

        }
        if (position==0&&audiomodellist.size()>1)
        {
            previous_music.setAlpha(0.50f);
            previous_music.setClickable(false);
            next_music.setAlpha(1f);
            next_music.setClickable(true);
        }
        if(position==audiomodellist.size()-1)
        {
            previous_music.setAlpha(1f);
            previous_music.setClickable(true);
            next_music.setAlpha(0.50f);
            next_music.setClickable(false);
        }
        if(audiomodellist.size()==1)
        {
            previous_music.setAlpha(0.50f);
            previous_music.setClickable(false);
            next_music.setAlpha(0.50f);
            next_music.setClickable(false);
        }
    }
    public void FirstTimeMusicPlay()
    {
        try {
            progressBar.setVisibility(View.VISIBLE);
            pause_icon.setVisibility(View.GONE);
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    try {
                        progressBar.setVisibility(View.GONE);
                        pause_icon.setVisibility(View.VISIBLE);
                        preparemediaplayer();
                        mediaplayer.start();
                        pause_icon.setImageResource(R.drawable.play);
                        updateseekbar();
                    } catch (Exception e) {
                    }
                }
            }, 1000);
        } catch (Exception e) {
        }
    }

    public void NextTimeMusicPlay(final String nextsongurl)
    {
        try {
            mediaplayer=new MediaPlayer();
            playerseekbar.setMax(100);
            progressBar.setVisibility(View.VISIBLE);
            pause_icon.setVisibility(View.GONE);
            new Handler().postDelayed(new Runnable()
            {
                @Override
                public void run()
                {
                    progressBar.setVisibility(View.GONE);
                    pause_icon.setVisibility(View.VISIBLE);
                    try
                    {
                        mediaplayer.setDataSource(nextsongurl);
                        mediaplayer.prepare();
                        texttotalDuration.setText(millisecondstoTimer(mediaplayer.getDuration()));
                    }
                    catch (Exception exception)
                    {
                        // Toast.makeText(PlayerNewActivity.this,exception.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                    mediaplayer.start();
                    pause_icon.setImageResource(R.drawable.play);
                    updateseekbar();
                }
            }, 1000);
        }
        catch (Exception e)
        {

        }

    }

    private String millisecondstoTimer(long millisecond) {
        String timerstring = "";
        String secondsstring;
        int hours = (int) (millisecond / (1000 * 60 * 60));
        int minutes = (int) (millisecond % (1000 * 60 * 60)) / (1000 * 60);
        int seconds = (int) ((millisecond % (1000 * 60 * 60)) % (1000 * 60) / 1000);

        if (hours > 0) {
            timerstring = hours + ":";

        }

        if (seconds < 10) {
            secondsstring = "0" + seconds;

        } else {
            secondsstring = "" + seconds;
        }

        timerstring = timerstring + minutes + ':' + secondsstring;
        return timerstring;
    }

    private void preparemediaplayer() {
        try {
            mediaplayer.setDataSource(song_url);
            mediaplayer.prepare();
            texttotalDuration.setText(millisecondstoTimer(mediaplayer.getDuration()));
        } catch (IOException exception) {
            //Toast.makeText(this,exception.toString(), Toast.LENGTH_SHORT).show();
            Log.e("error", exception.toString());
        }
    }

    private void updateseekbar() {
        try {
            if (mediaplayer.isPlaying()) {
                playerseekbar.setProgress((int) (((float) mediaplayer.getCurrentPosition() / mediaplayer.getDuration()) * 100));
                handler.postDelayed(updater, 1000);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private Runnable updater = new Runnable() {
        @Override
        public void run() {
            try {
                updateseekbar();
                long currentDuration = mediaplayer.getCurrentPosition();
                textcurrentTime.setText(millisecondstoTimer(currentDuration));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    };

    @Override
    public void onBackPressed() {
        if (mediaplayer.isPlaying()) {
            handler.removeCallbacks(updater);
            mediaplayer.stop();
            pause_icon.setImageResource(R.drawable.pause);
        }
        finish();
    }


    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.imgBack:
                if (mediaplayer != null && mediaplayer.isPlaying()) {
                    mediaplayer.stop();
                    handler.removeCallbacks(updater);
                    pause_icon.setImageResource(R.drawable.pause);
                    finish();
                }
                else
                    {
                    try {
                        mediaplayer = null;
                        handler.removeCallbacks(updater);
                        finish();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }
                break;

            case R.id.pause_icon:
                if (mediaplayer.isPlaying()) {
                    handler.removeCallbacks(updater);
                    mediaplayer.pause();
                    pause_icon.setImageResource(R.drawable.pause);
                } else {
                    mediaplayer.start();
                    pause_icon.setImageResource(R.drawable.play);
                    updateseekbar();
                }
                break;


           /* case R.id.next_music:
                if(position<audiomodellist.size()-1)
                {
                    previous_music.setClickable(true);
                    previous_music.setAlpha(1f);
                    position++;
                    if(position==audiomodellist.size()-1)
                    {
                        next_music.setAlpha(0.50f);
                        next_music.setClickable(false);
                        previous_music.setClickable(true);
                        previous_music.setAlpha(1f);
                    }
                    else if(position==0)
                    {
                        position=0;
                        next_music.setAlpha(1f);
                        next_music.setClickable(true);
                        previous_music.setClickable(false);
                        previous_music.setAlpha(0.50f);
                    }

                    TrandingSongsModel next_song=audiomodellist.get(position);
                    final String nextsongurl = next_song.getUploadSong();
                    final String song_name = next_song.getName();
                    Picasso.get().load(next_song.getImage()).into(singer_image);
                    try
                    {
                        handler.removeCallbacks(updater);
                        mediaplayer.stop();
                        NextTimeMusicPlay(nextsongurl);
                        song_name_textview.setText(song_name);
                        //Musicname.setText(StringUtils.capitalize(song_name.toLowerCase().trim()));
                    }
                    catch (Exception e)
                    {
                        e.printStackTrace();
                    }
                }
                break;

            case R.id.previous_music:
                if(position==0)
                {
                    position=0;
                    // Toast.makeText(this, "Arrived at the beginning", Toast.LENGTH_SHORT).show();
                    previous_music.setClickable(false);
                    previous_music.setAlpha(0.50f);
                    next_music.setClickable(true);
                    next_music.setAlpha(1f);
                }
                else
                {
                        previous_music.setClickable(false);
                        previous_music.setAlpha(0.50f);

                    previous_music.setClickable(true);
                    previous_music.setAlpha(1f);
                    next_music.setClickable(true);
                    next_music.setAlpha(1f);                                       //new changes
                    position--;
                    if(position==0)
                    {
                        position=0;
                        // Toast.makeText(this, "Arrived at the beginning", Toast.LENGTH_SHORT).show();
                        previous_music.setClickable(false);
                        previous_music.setAlpha(0.50f);
                        next_music.setClickable(true);
                        next_music.setAlpha(1f);
                    }
                }
                TrandingSongsModel privious_song=audiomodellist.get(position);
                final String privsongurl = privious_song.getUploadSong();
                final String privsong_name = privious_song.getName();
                Picasso.get().load(privious_song.getImage()).into(singer_image);

                handler.removeCallbacks(updater);
                mediaplayer.stop();
                NextTimeMusicPlay(privsongurl);
                song_name_textview.setText(privsong_name);
                break;*/

        }
        }
    }


