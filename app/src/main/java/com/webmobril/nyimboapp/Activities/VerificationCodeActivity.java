package com.webmobril.nyimboapp.Activities;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.webmobril.nyimboapp.R;
import com.webmobril.nyimboapp.Response.ResultFileResponse;
import com.webmobril.nyimboapp.Utils.CommonMethod;
import com.webmobril.nyimboapp.Utils.ProgressD;
import com.webmobril.nyimboapp.Utils.UrlApi;
import com.webmobril.nyimboapp.network.ApiInterface;

import in.aabhasjindal.otptextview.OTPListener;
import in.aabhasjindal.otptextview.OtpTextView;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

public class VerificationCodeActivity extends AppCompatActivity implements View.OnClickListener {
    LinearLayout linear_done;
    private OtpTextView otpTextView;
    String otp;
    String email;
    String from_forgotpassword_screen="1";
    TextView resend_textview;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_verification_code);
        init();

        otpTextView.setOtpListener(new OTPListener()
        {
            @Override
            public void onInteractionListener()
            {
                // fired when user types something in the Otpbox
            }
            @Override
            public void onOTPComplete(String otp1)
            {
                otp=otp1.toString().trim();
            }
        });
    }

    @Override
    public void onClick(View v)
    {
        switch (v.getId())
        {
            case R.id.linear_done:
                if (CommonMethod.isOnline(this))
                {
                    hideKeyboard((LinearLayout)v);
                    if(validation())
                    {
                        verifyotpApi(otp,email,from_forgotpassword_screen);
                    }
                }
                else
                {
                    CommonMethod.showAlert(getString(R.string.check_internet), this);
                }
                break;

            case R.id.resend_textview:
                hideKeyboard((TextView)v);
                // verifyotpApi(otp,email,from_forgotpassword_screen);
                    resendOtpApi(email);

                break;
        }
    }

    public void init()
    {
        email=getIntent().getStringExtra("email");

        linear_done=findViewById(R.id.linear_done);
        otpTextView = findViewById(R.id.otp_view);
        resend_textview=findViewById(R.id.resend_textview);

        linear_done.setOnClickListener(this);
        resend_textview.setOnClickListener(this);
    }


    public void hideKeyboard(View view) {
        try {
            InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        } catch(Exception ignored) {
        }
    }



    private boolean validation()
    {
        /*if(otpTextView.getOTP().equals(""))
        {
            Toast.makeText(this, "Please Enter Otp", Toast.LENGTH_SHORT).show();
            return false;
        }*/
        if(otpTextView.getOTP().length()<4)
        {
            Toast.makeText(this, "Please Enter Valid Otp", Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }

    private void verifyotpApi(String otp, String email, String from_forgotpassword_screen)
    {
        final ProgressD progressDialog = ProgressD.show(VerificationCodeActivity.this,getResources().getString(R.string.logging_in), true, false, null);

        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();
        Gson gson = new GsonBuilder()
                .setLenient()
                .create();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(UrlApi.BASE_URL)
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(client)
                .build();
        ApiInterface service = retrofit.create(ApiInterface.class);
        Call<ResultFileResponse> call = service.Verifiyotp(email,otp,from_forgotpassword_screen);
        call.enqueue(new Callback<ResultFileResponse>()
        {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onResponse(Call<ResultFileResponse> call, retrofit2.Response<ResultFileResponse> response)
            {
                try
                {
                    ResultFileResponse resultFileResponse = response.body();
                    String status = resultFileResponse.getStatus();
                    String message = resultFileResponse.getMessage();

                    progressDialog.dismiss();
                    Toast.makeText(VerificationCodeActivity.this, message, Toast.LENGTH_SHORT).show();

                    if(status.equals("true"))
                    {
                        Intent i=new Intent(VerificationCodeActivity.this,ResetPasswordActivity.class);
                        i.putExtra("email",email);
                        startActivity(i);
                        finish();
                        Log.d("email",email);
                    }
                    else
                    { }
                }
                catch (Exception e)
                {
                    Log.e("Login Faild", e.toString());
                }
            }

            @Override
            public void onFailure(Call<ResultFileResponse> call, Throwable t)
            {

                Toast.makeText(VerificationCodeActivity.this, "Failed" + t, Toast.LENGTH_SHORT).show();
            }
        });
    }


    private void resendOtpApi(String email)
    {
        final ProgressD progressDialog = ProgressD.show(VerificationCodeActivity.this,getResources().getString(R.string.logging_in), true, false, null);

        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();
        Gson gson = new GsonBuilder()
                .setLenient()
                .create();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(UrlApi.BASE_URL)
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(client)
                .build();
        ApiInterface service = retrofit.create(ApiInterface.class);
        Call<ResultFileResponse> call = service.forgotpassword(email);
        call.enqueue(new Callback<ResultFileResponse>()
        {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onResponse(Call<ResultFileResponse> call, retrofit2.Response<ResultFileResponse> response)
            {
                try
                {
                    ResultFileResponse resultFileResponse = response.body();
                    String status = resultFileResponse.getStatus();
                    String message = resultFileResponse.getMessage();
                    progressDialog.dismiss();

                    Toast.makeText(VerificationCodeActivity.this, message, Toast.LENGTH_SHORT).show();

                    if(status.equals("true"))
                    {
                        otpTextView.setOTP("");
                    }
                    else
                    { }
                }
                catch (Exception e)
                {
                    Log.e("Login Faild", e.toString());
                    Toast.makeText(VerificationCodeActivity.this, "Failed" , Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ResultFileResponse> call, Throwable t)
            {
                Toast.makeText(VerificationCodeActivity.this, "Failed" + t, Toast.LENGTH_SHORT).show();
            }
        });
    }


    @Override
    protected void onResume()
    {
        super.onResume();
        otpTextView.setOTP("");
    }
}