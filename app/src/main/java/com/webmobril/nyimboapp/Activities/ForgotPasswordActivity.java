package com.webmobril.nyimboapp.Activities;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.webmobril.nyimboapp.R;
import com.webmobril.nyimboapp.Response.ResultFileResponse;
import com.webmobril.nyimboapp.Utils.CommonMethod;
import com.webmobril.nyimboapp.Utils.ProgressD;
import com.webmobril.nyimboapp.Utils.UrlApi;
import com.webmobril.nyimboapp.network.ApiInterface;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

import static com.webmobril.nyimboapp.Utils.CommonMethod.isValidEmaillId;

public class ForgotPasswordActivity extends AppCompatActivity implements View.OnClickListener
{
    LinearLayout linear_next;
    EditText email_id;
    ImageView back_button;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);
        init();
    }
    public void init()
    {
        email_id=findViewById(R.id.email_id);
        linear_next=findViewById(R.id.linear_next);
        linear_next.setOnClickListener(this);
    }
    private boolean validation()
    {
        if (TextUtils.isEmpty(email_id.getText().toString().trim())) {
            Toast.makeText(this, "Please enter Email Id", Toast.LENGTH_SHORT).show();
            return false;
        }
        else if (!isValidEmaillId(email_id.getText().toString().trim())) {
            Toast.makeText(this, "Please enter a valid Email Id", Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }

    @Override
    public void onClick(View v)
    {
        switch (v.getId())
        {
            case R.id.linear_next:
                if (CommonMethod.isOnline(this))
                {
                    hideKeyboard((LinearLayout)v);
                    if(validation())
                    {
                        ForgotPasswordApi(email_id.getText().toString().trim());
                    }
                }
                else
                {
                    CommonMethod.showAlert(getString(R.string.check_internet), this);
                }
                break;

            /*case R.id.back_button:
                finish();
                break;*/
        }
    }

    public void hideKeyboard(View view) {
        try {
            InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        } catch(Exception ignored) {
        }
    }

    private void ForgotPasswordApi(String email)
    {
        final ProgressD progressDialog = ProgressD.show(ForgotPasswordActivity.this,getResources().getString(R.string.logging_in), true, false, null);

        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();
        Gson gson = new GsonBuilder()
                .setLenient()
                .create();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(UrlApi.BASE_URL)
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(client)
                .build();
        ApiInterface service = retrofit.create(ApiInterface.class);
        Call<ResultFileResponse> call = service.forgotpassword(email);
        call.enqueue(new Callback<ResultFileResponse>()
        {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onResponse(Call<ResultFileResponse> call, retrofit2.Response<ResultFileResponse> response)
            {
                try
                {
                    ResultFileResponse resultFileResponse = response.body();
                    String status = resultFileResponse.getStatus();
                    String message = resultFileResponse.getMessage();
                    progressDialog.dismiss();

                    Toast.makeText(ForgotPasswordActivity.this, message, Toast.LENGTH_SHORT).show();

                    if(status.equals("true"))
                    {
                        Intent i=new Intent(ForgotPasswordActivity.this,VerificationCodeActivity.class);
                        i.putExtra("email",email);
                        startActivity(i);
                    }
                    else
                    { }
                }
                catch (Exception e)
                {
                    Log.e("Login Faild", e.toString());
                    Toast.makeText(ForgotPasswordActivity.this, "Failed" , Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ResultFileResponse> call, Throwable t)
            {

                Toast.makeText(ForgotPasswordActivity.this, "Failed" + t, Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
       email_id.setText("");
    }
}