package com.webmobril.nyimboapp.Activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.annotation.SuppressLint;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.webmobril.nyimboapp.R;

import com.webmobril.nyimboapp.fragments.MusicFragment;
import com.webmobril.nyimboapp.fragments.PodcastsFragment;

public class LibraryMainActivity extends AppCompatActivity implements View.OnClickListener
{
    View view;
    TextView music_heading,Podcasts_heading;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_librery_main);
        init();
    }

    @Override
    public void onBackPressed()
    {
        finish();
    }
    private void init()
    {
        music_heading=findViewById(R.id.music_heading);
        Podcasts_heading=findViewById(R.id.Podcasts_heading);
         music_heading.setOnClickListener(this);
        Podcasts_heading.setOnClickListener(this);

        FragmentManager fragmentManager = getSupportFragmentManager();
        Fragment fragmentMusic = fragmentManager.findFragmentByTag("music");
        fragmentMusic = new MusicFragment();
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.replace(R.id.music_podcasts_framelayout, fragmentMusic, "music");
        transaction.commit();
    }


    @SuppressLint("ResourceAsColor")
    @Override
    public void onClick(View v)
    {
        if (v.getId() == R.id.music_heading)
        {
            FragmentManager fragmentManager = getSupportFragmentManager();
            Fragment fragmentMusic = fragmentManager.findFragmentByTag("music");
            if (fragmentMusic == null)
            {
                fragmentMusic = new MusicFragment();
            }
            FragmentTransaction transaction = fragmentManager.beginTransaction();
            transaction.replace(R.id.music_podcasts_framelayout, fragmentMusic, "music");
            transaction.commit();


            Podcasts_heading.setTextColor(Color.parseColor("#5D726F"));            //first time color
            music_heading.setTextColor(Color.parseColor("#FFFFFF"));
        }

        if(v.getId() == R.id.Podcasts_heading)
        {
            FragmentManager fragmentManager = getSupportFragmentManager();
            Fragment fragmentPodcasts = fragmentManager.findFragmentByTag("podcasts");
            if ( fragmentPodcasts == null)
            {
                fragmentPodcasts = new PodcastsFragment();
            }
            FragmentTransaction transaction = fragmentManager.beginTransaction();
            transaction.replace(R.id.music_podcasts_framelayout,  fragmentPodcasts, "podcasts");
            transaction.commit();

            music_heading.setTextColor(Color.parseColor("#5D726F"));
            Podcasts_heading.setTextColor(Color.parseColor("#FFFFFF"));
        }
    }
}