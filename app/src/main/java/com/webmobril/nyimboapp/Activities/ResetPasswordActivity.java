package com.webmobril.nyimboapp.Activities;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.webmobril.nyimboapp.R;
import com.webmobril.nyimboapp.Response.ResultFileResponse;
import com.webmobril.nyimboapp.Utils.CommonMethod;
import com.webmobril.nyimboapp.Utils.ProgressD;
import com.webmobril.nyimboapp.Utils.UrlApi;
import com.webmobril.nyimboapp.network.ApiInterface;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

import static com.webmobril.nyimboapp.Utils.CommonMethod.isValidEmaillId;
public class ResetPasswordActivity extends AppCompatActivity implements View.OnClickListener
{
    EditText email,password,confirm_password;
    LinearLayout linear_confirm;
    String email_id;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reset_password);
        init();
    }

    public void init()
    {
      //  email_id= LoginPreferences.getActiveInstance(ResetPasswordActivity.this).getEmailResetPassword();
        email_id=getIntent().getStringExtra("email");
        email=findViewById(R.id.email);
        password=findViewById(R.id.password);
        confirm_password=findViewById(R.id.confirm_password);
        linear_confirm=findViewById(R.id.linear_confirm);
        linear_confirm.setOnClickListener(this);

        Log.d("email",email_id+"dffdf");
        email.setText(email_id);
        email.setEnabled(false);
    }

    @Override
    public void onClick(View v)
    {
        switch (v.getId())
        {
            case R.id.linear_confirm:
                if (CommonMethod.isOnline(this))
                {
                    hideKeyboard((LinearLayout)v);
                    if(validation())
                    {
                        ResetPasswordApi(password.getText().toString().trim(),confirm_password.getText().toString().trim(),
                                email.getText().toString().trim());
                    }
                }
                else
                {
                    CommonMethod.showAlert(getString(R.string.check_internet), this);
                }
                break;
        }
    }

    public void hideKeyboard(View view) {
        try {
            InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        } catch(Exception ignored) {
        }
    }

    private boolean validation()
    {
        if (TextUtils.isEmpty(email.getText().toString().trim())) {
            Toast.makeText(this, "Please enter Email", Toast.LENGTH_SHORT).show();
            return false;
        }
        else if (!isValidEmaillId(email.getText().toString().trim())) {
            Toast.makeText(this, "Please enter a valid Email", Toast.LENGTH_SHORT).show();
            return false;
        }

        else if (TextUtils.isEmpty(password.getText().toString().trim())) {
            Toast.makeText(this, "Please enter password", Toast.LENGTH_SHORT).show();
            return false;
        }

        else if (password.getText().toString().trim().length() < 8 || password.getText().toString().trim().length() > 16) {
            Toast.makeText(this, "Password must be 8 to 16 characters", Toast.LENGTH_LONG).show();
            return false;
        }

        else if(TextUtils.isEmpty(confirm_password.getText().toString().trim())) {
            Toast.makeText(this, "Please enter confirm password", Toast.LENGTH_SHORT).show();
            return false;
        }

        else if (confirm_password.getText().toString().trim().length() < 8 || confirm_password.getText().toString().trim().length() > 16) {
            Toast.makeText(this, "Confirm Password must be 8 to 16 characters", Toast.LENGTH_LONG).show();
            return false;
        }

        else if (!confirm_password.getText().toString().trim().equals(password.getText().toString().trim()))
        {
            Toast.makeText(this, "Password and confirm password does not match", Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }


    private void ResetPasswordApi(String new_password,String confirm_password,String email)
    {
        final ProgressD progressDialog = ProgressD.show(ResetPasswordActivity.this,getResources().getString(R.string.logging_in), true, false, null);

        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();
        Gson gson = new GsonBuilder()
                .setLenient()
                .create();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(UrlApi.BASE_URL)
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(client)
                .build();
        ApiInterface service = retrofit.create(ApiInterface.class);
        Call<ResultFileResponse> call = service.ResetPassword(new_password,confirm_password,email);
        call.enqueue(new Callback<ResultFileResponse>()
        {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onResponse(Call<ResultFileResponse> call, retrofit2.Response<ResultFileResponse> response)
            {
                try
                {
                    ResultFileResponse resultFileResponse = response.body();
                    Log.d("response_reset",response.toString());
                    String status = resultFileResponse.getStatus();
                    String message = resultFileResponse.getMessage();
                    progressDialog.dismiss();
                    Toast.makeText(ResetPasswordActivity.this, message, Toast.LENGTH_SHORT).show();
                    if(status.equals("true"))
                    {
                        Intent i=new Intent(ResetPasswordActivity.this,LoginActivity.class);
                        startActivity(i);
                    }
                    else
                    { }
                }
                catch (Exception e)
                {
                    Log.e("Login Faild", e.toString());
                }
            }

            @Override
            public void onFailure(Call<ResultFileResponse> call, Throwable t)
            {

                Toast.makeText(ResetPasswordActivity.this, "Failed" + t, Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void onBackPressed()
    {
        super.onBackPressed();
        finish();
    }

    @Override
    protected void onResume() {
        super.onResume();
        /*email.setText("");
        password.setText("");
        confirm_password.setText("");*/
    }
}